import configparser

from RdReportsReader.Src.AppLogic.FileOps import absolute_file_location


class ProvideInternalTokens:

    config = None

    def __init__(self):
        if not ProvideInternalTokens.config:
            self._initialize_configparser()

    # Internal links

    def one_of_reports_path_prefix(self):
        return ProvideInternalTokens.config['Internal_Links']['one_of_reports_path_prefix']

    # Internal grammar

    def link_start(self):
        return ProvideInternalTokens.config['Internal_Grammar']['link_start']

    def link_end(self):
        return ProvideInternalTokens.config['Internal_Grammar']['link_end']

    def link_display_separator(self):
        return ProvideInternalTokens.config['Internal_Grammar']['link_display_separator']

    def line(self):
        return ProvideInternalTokens.config['Internal_Grammar']['line']

    # Headers

    def masterlist_campaign_header(self):
        return ProvideInternalTokens.config['Headers']['masterlist_campaign_header']

    def top_level_header(self):
        return ProvideInternalTokens.config['Headers']['top_level_header']

    def location_header(self):
        return ProvideInternalTokens.config['Headers']['location_header']

    def consequences_header(self):
        return ProvideInternalTokens.config['Headers']['consequences_header']

    def dramatis_personae_header(self):
        return ProvideInternalTokens.config['Headers']['dramatis_personae_header']

    # Magic numbers

    def location_city_depth(self):
        return int(ProvideInternalTokens.config['Magic_Numbers']['location_city_depth'])

    # Private

    def _initialize_configparser(self):
        ProvideInternalTokens.config = configparser.ConfigParser()
        path = absolute_file_location(__file__, "../rd_internal_tokens.properties")
        ProvideInternalTokens.config.read(path)
