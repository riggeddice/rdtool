from ..Tools.ReadConfigJson import ReadConfigJson


class ProvideZipSourceConfig:

    def __init__(self):
        read_config = ReadConfigJson()
        root = read_config.general_files_area()['root_dir']

        self.rd_dir_path = root
        self.backup_filename_pattern = read_config.zip_source_area()['backup_filename_pattern']
        self.temp_folder = read_config.zip_source_area()['temp_folder']
        self.source = read_config.zip_source_area()['rd_backup_source_folder']
        self.report_masterlist_file = read_config.zip_source_area()['report_masterlist_file_to_read']
        self.profile_masterlist_file = read_config.zip_source_area()['profile_masterlist_file_to_read']

        self.zip_filepath = self.rd_dir_path + "/" + self.backup_filename_pattern
        self.temp_dir_path = self.rd_dir_path + "/" + self.temp_folder

        self.all_text_files_folder = self.temp_dir_path + "/" + self.source + "/"

    def temp_dir_to_create_patch(self):
        return self.temp_dir_path

    def folder_with_zip_backup_file(self):
        return self.rd_dir_path

    def zip_backup_file_pattern(self):
        return self.backup_filename_pattern

    def unpacked_folder_with_all_text_files(self):
        return self.all_text_files_folder

    def profile_masterlist_file_to_read(self):
        return self.profile_masterlist_file

    def report_masterlist_file_to_read(self):
        return self.report_masterlist_file
