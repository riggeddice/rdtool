from ..Tools.ReadConfigJson import ReadConfigJson


class ProvideGeneralFilepathConfig:

    rd_dir_path = None
    output_report_masterlist_file_to = None
    output_location_masterlist_file_to = None
    output_complete_profile_files_to_folder = None
    output_campaign_summaries_to_folder = None
    output_location_descriptions_to_folder = None

    def __init__(self):
        read_config = ReadConfigJson()

        root = read_config.general_files_area()['root_dir']

        self.rd_dir_path = root
        self.output_report_masterlist_file_to = read_config.general_files_area()['output_report_masterlist_file_to']
        self.output_location_masterlist_file_to = read_config.general_files_area()['output_location_masterlist_file_to']
        self.output_complete_profile_files_to_folder = read_config.general_files_area()['output_complete_profile_files_to_folder']
        self.output_campaign_summaries_to_folder = read_config.general_files_area()['output_campaign_summaries_to_folder']
        self.output_location_descriptions_to_folder = read_config.general_files_area()['output_location_descriptions_to_folder']
