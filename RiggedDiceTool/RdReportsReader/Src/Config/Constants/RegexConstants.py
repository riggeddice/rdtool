class RegexConstants:

    def first_line_is_word(self):
        return '^.+'

    def starts_with_h1(self):
        return "^\+ "

    def starts_with_h3(self):
        return '\n\+\+\+\s'

    def creature_type(self):
        return '...:\s.'

    def list_indentation_depth_symbol(self):
        return " "

    def numbered_list_single_line(self):
        return "\s*#.+"

    def numbered_list_entry(self):
        return "\s*#"

    def profile_metadata_in_masterlist(self):
        return '(.+karty-postaci-\d\d\d\d.)(.+)(\|.+)'

    def report_metadata_in_masterlist(self):
        return '(.+:\d\d\d\d\d\d-)(.+)(\|.+)'

    def mock_aggregated_source_single_in_masterlist(self):
        return '(single:\s)(.+)'

    def mock_masterlist_record(self):
        return '(mlist:\s)(\w+)'

    def backup_file_pattern(self):
        return '(\d\d\d\d\d\d\d\d)(_.+)(.zip)'

    def faction_header_pattern(self):
        return '[^\+]\+\+\+\s'

    def faction_first_header_pattern(self):
        return '^\+\+\+\s'

    def profile_masterlist_metadata_record(self):
        return '(\[\[\[)(.+)(\|)(.+)(\]\]\])(.*)'

    def complete_dramatis_personae_record(self):
        return "...:\s\[\[\[.+:.+\]\]\]"

    def dramatis_personae_record_from_report(self):
        """
        :return: matches the dramatis personae in a report with both delimiters and includes ???: mask
        """
        return "(.{3}?)(:\s)(.*?)(\sjako|,)(.*)"

    def location_masterlist_meadata_name(self):
        return "(#\s*)(.+)"
