import json

from RdReportsReader.Src.AppLogic.FileOps import absolute_file_location
from RdReportsReader.Src.AppLogic.FileOps import read_file


class ReadConfigJson:

    read_config = None

    # Less file reading == good stuff.

    def __init__(self):
        if ReadConfigJson.read_config is None:
            ReadConfigJson.read_config = self.__read_json_config()
        else:
            pass

    # Categories of configuration go here

    def general_files_area(self):
        return ReadConfigJson.read_config['general_files']

    def zip_source_area(self):
        return ReadConfigJson.read_config['zip_source']

    # Private method. For pseudo-singleton approach.

    def __read_json_config(self):
        config_path = absolute_file_location(__file__, "../rd_tool_config.json")
        read_json = read_file(config_path)
        parsed_config = json.loads(read_json)
        return parsed_config
