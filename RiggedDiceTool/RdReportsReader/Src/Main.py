from RiggedDiceTool.RdReportsReader.Src.Business.Selection import selected_business_actions
from RiggedDiceTool.RdReportsReader.Src.Business.Selection import BusinessActionConfig


def start_rd_tool():

    config = BusinessActionConfig()
    actions_to_execute = selected_business_actions(config)

    for action in actions_to_execute:
        print(action.on_start())
        action.do()
        print(action.on_end())

    print("Everything is finished, all is done.")

start_rd_tool()
