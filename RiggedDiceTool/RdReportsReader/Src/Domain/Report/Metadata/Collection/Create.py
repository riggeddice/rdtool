from RdReportsReader.Src.Domain.Report.Metadata.Collection.ReportMetadataCollection import ReportMetadataCollection


def create_report_metadata_empty_collection():
    return ReportMetadataCollection([])


def create_report_metadata_collection(metadata_array):
    return ReportMetadataCollection(metadata_array)
