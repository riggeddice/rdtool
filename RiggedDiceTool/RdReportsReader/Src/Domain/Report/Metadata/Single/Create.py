from RdReportsReader.Src.Domain.Report.Metadata.Single.SingleReportMetadata import SingleReportMetadata
from RdReportsReader.Src.Config.Providers.ProvideInternalTokens import ProvideInternalTokens


def create_single_report_metadata_from_raw_record(record):

    start = record.index(ProvideInternalTokens().one_of_reports_path_prefix())
    finish = record.index(ProvideInternalTokens().link_end())
    comment_start = finish + len(ProvideInternalTokens().link_end())
    comment_finish = len(record)

    agnostic = record[start: finish]
    extra_comments = record[comment_start: comment_finish]

    return create_single_report_metadata_from_valid_metadata_record(agnostic, ProvideInternalTokens().link_display_separator(), extra_comments)


def create_single_report_metadata_from_valid_metadata_record(record, delimiter, extra_comments):

    separator_offset = record.index(delimiter)
    seq_no_offset = separator_offset + len(delimiter)
    date_offset = separator_offset + len(delimiter) + len("xxxx -")
    name_offset = separator_offset + len(delimiter) + len("xxxx -") + len(" yymmdd -")

    name = record[name_offset: len(record)]
    link = record[0: separator_offset]
    date = record[date_offset: date_offset + len("yymmdd")]
    sequence_number = record[seq_no_offset: seq_no_offset + 3]

    return SingleReportMetadata(name, link, date, sequence_number, extra_comments)
