class SingleReportMetadata:

    def __init__(self, name, link, date, seq_no, extra_comments):
        self.metadata_name = name
        self.link = link
        self.mission_date = date
        self.mission_sequence_number = seq_no
        self.extraComments = extra_comments

    # properties

    def weblink(self):
        return self.link

    def ziplink(self):
        return self.link.replace(":", "_")

    def name(self):
        return self.metadata_name

    def date(self):
        return self.mission_date

    def sequence_number(self):
        return self.mission_sequence_number

    def set_sequence_number(self, stringSeqNo):
        self.mission_sequence_number = stringSeqNo

    def parentCampaign(self):
        return self.parent_campaign

    # operations

    def updateWithCampaign(self, campaign):
        self.parent_campaign = campaign

    def to_rd_text_no_list_no_comments(self):
        rd_text = "[[[" + self.weblink() + "|" + self.sequence_number() + " - " + self.date() + " - " + self.name() + "]]]"
        return rd_text

    def to_rd_text(self):
        rd_text = "* " + self.to_rd_text_no_list_no_comments() + self.extraComments
        return rd_text
