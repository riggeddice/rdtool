from RdReportsReader.Src.Domain.Location.Metadata.CompositeRecordCollection.Create import create_location_composite_record_collection


def get_all_dramatis_personae_record_collection(reports):

    all_dramatis_personae_records = []

    for single_report in reports:
        records_from_single_report = single_report.getDramatisPersonae().get_dp_records()

        for single_record_from_single_report in records_from_single_report:
            all_dramatis_personae_records.append(single_record_from_single_report)

    return all_dramatis_personae_records


def get_composite_location_collection(reports):

    all_composite_records_on_missions = []
    for single_report in reports:
        single_locations_in_report = single_report.getLocationsInReport()
        single_composite_record = single_locations_in_report.composite_metadata_record()
        all_composite_records_on_missions.append(single_composite_record)

    composite_record_collection = create_location_composite_record_collection(all_composite_records_on_missions)

    return composite_record_collection


def retrieve_single_report(metadata_to_match, reports):

    for report in reports:
        if metadata_to_match.weblink() == report.link():
            return report

    return None
