from RdReportsReader.Src.Domain.CharacterProfile.Generated.Collection.Create import create_generated_character_profile_collection
from RdReportsReader.Src.Domain.Report.BuildingBlocks.ActorToActorRelationMapCollection.Create import create_actor_to_actor_relation_map_collection
from RdReportsReader.Src.Domain.Report.BuildingBlocks.ActorToRecordsCollection.Create import create_actor_to_records_collection
from .Retrieve import get_all_dramatis_personae_record_collection


def generate_profiles_from_report_collection(reports):
    dramatis_personae_records = get_all_dramatis_personae_record_collection(reports)

    actor_to_dp_records_col = create_actor_to_records_collection(dramatis_personae_records)
    actor_to_actor_relation_map_col = create_actor_to_actor_relation_map_collection(dramatis_personae_records)

    actor_name_list = actor_to_dp_records_col.all_actor_names_list()

    return create_generated_character_profile_collection(actor_name_list, actor_to_dp_records_col, actor_to_actor_relation_map_col)
