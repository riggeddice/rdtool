from RdReportsReader.Src.Domain.Report.Collection.ReportCollection import ReportCollection
from RdReportsReader.Src.Domain.Report.Single.Complete.Create import create_single_mission_report


def create_report_collection(rd_reports, report_masterlist):

    reports = []

    for report in rd_reports:
        __try_update_reports_from(reports, report, report_masterlist)

    return ReportCollection(reports)


def __try_update_reports_from(reports, rd_report, report_masterlist):

    try:

        single_report = create_single_mission_report(rd_report, report_masterlist)
        reports.append(single_report)

    except:

        print("RD_PARSE_FAILURE: Report: " + rd_report.name())

    else:
        pass
        # print("RD_PARSE_SUCCESS: Report: " + rd_report.name())
