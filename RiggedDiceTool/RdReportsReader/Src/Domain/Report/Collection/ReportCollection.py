from .Action.Generate import generate_profiles_from_report_collection
from .Action.Retrieve import get_composite_location_collection
from .Action.Retrieve import retrieve_single_report


class ReportCollection:

    def __init__(self, mission_reports):
        self._reports = mission_reports

    def mission_reports(self):
        return self._reports

    def all_locations_on_missions(self):
        return get_composite_location_collection(self._reports)

    def count(self):
        return len(self._reports)

    def generate_profiles(self):
        return generate_profiles_from_report_collection(self._reports)

    def report_having_metadata(self, metadata):
        return retrieve_single_report(metadata, self._reports)
