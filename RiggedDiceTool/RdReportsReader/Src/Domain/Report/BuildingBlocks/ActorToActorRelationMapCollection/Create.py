from RdReportsReader.Src.Domain.Report.BuildingBlocks.ActorToActorRelationMapCollection.DpActorToActorRelationMapCollection import DpActorToActorRelationMapCollection
from RdReportsReader.Src.Domain.Report.BuildingBlocks.ActorToActorRelationMapCollection.Structure.ActorToActorRelationMap.Create import create_actor_to_actor_relation_map
from RdReportsReader.Src.Domain.Report.BuildingBlocks.ActorToRecordsCollection.Create import create_actor_to_records_collection


def create_actor_to_actor_relation_map_collection(dramatis_personae_records):

    atr_collection = create_actor_to_records_collection(dramatis_personae_records)

    relation_maps = []
    for single_actor_records_map in atr_collection.all_actors():

        actor_to_actor_relation_map = create_actor_to_actor_relation_map(single_actor_records_map)
        relation_maps.append(actor_to_actor_relation_map)

    final_collection = DpActorToActorRelationMapCollection(relation_maps)
    return final_collection
