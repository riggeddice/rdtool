

class DpActorToActorRelationMapCollection:

    def __init__(self, relation_maps):
        self._relation_maps = relation_maps

    def get_for_single_actor(self, actor_name):
        found = [single_map for single_map in self._relation_maps if single_map.actor_name() == actor_name]
        return None if len(found) == 0 else found[0]
