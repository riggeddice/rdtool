from RdReportsReader.Src.Domain.Report.BuildingBlocks.ActorToActorRelationMapCollection.Structure.RelationWithActor.RelationWithSingleActor import RelationWithSingleActor


def update_ata_map_with_record(ata_relation_dict, dp_record):

    char_name = dp_record.name()

    if char_name not in ata_relation_dict:

        # New instance
        new_relation = RelationWithSingleActor(char_name)
        ata_relation_dict[char_name] = new_relation

    ata_relation_dict[char_name].update_with(dp_record)
