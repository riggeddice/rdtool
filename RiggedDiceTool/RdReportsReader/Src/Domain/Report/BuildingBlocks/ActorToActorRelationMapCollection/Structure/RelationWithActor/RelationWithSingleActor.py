class RelationWithSingleActor:

    def __init__(self, char_name):
        self._char_name = char_name
        self._relation_intensity = 0
        self._records = []

    def intensity(self):
        return self._relation_intensity

    def dp_records(self):
        return self._records

    def update_with(self, dp_record):
        self._relation_intensity += 1
        self._records.append(dp_record)
