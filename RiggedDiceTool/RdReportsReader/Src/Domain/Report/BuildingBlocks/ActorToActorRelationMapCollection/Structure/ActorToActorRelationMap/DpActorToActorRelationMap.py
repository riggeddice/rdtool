from .Action.All import update_ata_map_with_record


class DpActorToActorRelationMap:

    def __init__(self, actor_name):
        self._name = actor_name
        self._ata_relations = {}

    def actor_name(self):
        return self._name

    def actor_to_actor_relations(self):
        return self._ata_relations

    def update_with_record(self, dp_record):
        update_ata_map_with_record(self._ata_relations, dp_record)
