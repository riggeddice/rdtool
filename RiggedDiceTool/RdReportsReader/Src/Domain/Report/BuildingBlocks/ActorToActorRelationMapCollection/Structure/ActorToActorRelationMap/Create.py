from RdReportsReader.Src.Domain.Report.BuildingBlocks.ActorToActorRelationMapCollection.Structure.ActorToActorRelationMap.DpActorToActorRelationMap import DpActorToActorRelationMap


def create_actor_to_actor_relation_map(single_actor_records_map):

    actor_relation_map = DpActorToActorRelationMap(single_actor_records_map.actor_name())

    for dp_record in single_actor_records_map.all_records():
        dramatis_personae = dp_record.parent_dramatis_personae()
        sibling_records_on_mission = dramatis_personae.get_only_dp_records_sibling_to(dp_record)

        for siblingRecord in sibling_records_on_mission:
            actor_relation_map.update_with_record(siblingRecord)

    return actor_relation_map
