

class DpActorToRecordsCollection:

    def __init__(self, actor_record_map_collection):
        self._actor_record_map_collection = actor_record_map_collection

    def all_actors(self):
        return self._actor_record_map_collection

    def all_actor_names_list(self):
        return [single_record_map.actor_name() for single_record_map in self._actor_record_map_collection]

    def get_for_single_actor(self, actor_name):
        found = [single_record_map for single_record_map in self._actor_record_map_collection if single_record_map.actor_name() == actor_name]
        return None if len(found) == 0 else found[0]
