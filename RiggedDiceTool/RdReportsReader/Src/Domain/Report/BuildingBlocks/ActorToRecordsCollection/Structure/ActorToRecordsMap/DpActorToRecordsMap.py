class DpActorToRecordsMap:

    def __init__(self, actor_name):
        self._name = actor_name
        self._dp_records = []

    def all_records(self):
        return self._dp_records

    def actor_name(self):
        return self._name

    def append_record(self, current_record):
        self._dp_records.append(current_record)
