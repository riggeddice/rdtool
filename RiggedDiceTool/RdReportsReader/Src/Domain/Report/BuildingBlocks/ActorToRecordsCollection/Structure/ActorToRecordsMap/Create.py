from RdReportsReader.Src.Domain.Report.BuildingBlocks.ActorToRecordsCollection.Structure.ActorToRecordsMap.DpActorToRecordsMap import DpActorToRecordsMap


def create_actor_to_records_map(current_name):
    return DpActorToRecordsMap(current_name)
