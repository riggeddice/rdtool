from RdReportsReader.Src.Domain.Report.BuildingBlocks.ActorToRecordsCollection.DpActorToRecordsCollection import DpActorToRecordsCollection
from RdReportsReader.Src.Domain.Report.BuildingBlocks.ActorToRecordsCollection.Structure.ActorToRecordsMap.Create import create_actor_to_records_map


def create_actor_to_records_collection(dramatis_personae_records):

    name_sorted_records = sorted(dramatis_personae_records, key=lambda record: record.name())

    actor_records_array = []
    current_name = ""
    current_actor_record_map = None

    for currentRecord in name_sorted_records:

        if current_name != currentRecord.name():
            current_name = currentRecord.name()
            current_actor_record_map = create_actor_to_records_map(current_name)
            actor_records_array.append(current_actor_record_map)

        # this has guaranteed safety - currentName CANNOT be empty
        current_actor_record_map.append_record(currentRecord)

    collection = DpActorToRecordsCollection(actor_records_array)
    return collection
