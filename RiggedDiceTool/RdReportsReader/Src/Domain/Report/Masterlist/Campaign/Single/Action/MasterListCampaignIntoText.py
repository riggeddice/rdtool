from RdReportsReader.Src.AppLogic.StripText import strip_special_symbols


def master_list_campaign_into_text(campaign):

    name_text = "++++ " + campaign.name() + "\n"
    campaign_mission_count_text = "Ilość misji: " + __pad_to_3_digit_string(campaign.mission_count()) + "\n"
    campaign_summary_link = __campaign_summary_link(campaign.name()) + "\n"
    separator = "\n"
    metadata_text = __metadata_to_text(campaign.all_metadata())

    if metadata_text == "":
        combined = name_text
    else:
        combined = name_text + campaign_mission_count_text + campaign_summary_link + separator + metadata_text

    return combined


def __campaign_summary_link(campaign_name):
    link_formed_campaign_name = strip_special_symbols(campaign_name.lower().replace(" ", "-"))[:25]
    if link_formed_campaign_name[-1] == "-":
        link_formed_campaign_name = link_formed_campaign_name[:-1]

    link = "[[[inwazja-konspekty-kampanie:" + link_formed_campaign_name + "|Streszczenie kampanii]]]"
    return link


def __metadata_to_text(all_metadata):

    all_metadata_combined_text = ""

    for metadata in all_metadata:
        all_metadata_combined_text = all_metadata_combined_text + metadata.to_rd_text() + "\n"

    return all_metadata_combined_text.strip()


def __pad_to_3_digit_string(value):
    return '%03d' % value
