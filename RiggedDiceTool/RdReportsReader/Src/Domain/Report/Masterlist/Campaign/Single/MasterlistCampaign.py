from RdReportsReader.Src.Domain.Report.Masterlist.Campaign.Single.Action.MasterListCampaignIntoText import master_list_campaign_into_text
from RdReportsReader.Src.Domain.Report.Metadata.Collection.Create import create_report_metadata_empty_collection
from .Action.CorrectCampaignSequenceNumbers import correct_campaign_sequence_numbers


class MasterlistCampaign:

    def __init__(self, name):
        self.campaign_name = name
        self.metadata_collection = create_report_metadata_empty_collection()

    def name(self):
        return self.campaign_name

    def mission_count(self):
        return self.metadata_collection.mission_count()

    def all_metadata(self):
        return self.metadata_collection.all_metadata()

    def append_report_metadata(self, metadata):
        metadata.updateWithCampaign(self)
        self.metadata_collection.append_report_metadata(metadata)

    def metadata_at_position(self, position):
        return self.metadata_collection.single_metadata_at_position(position)

    def metadata_having_link(self, weblink):
        return self.metadata_collection.single_metadata_having_link(weblink)

    def correct_sequence_numbers_starting_at(self, start_value):
        self.metadata_collection = correct_campaign_sequence_numbers(self.metadata_collection, start_value)

    def to_rd_text(self):
        return master_list_campaign_into_text(self)
