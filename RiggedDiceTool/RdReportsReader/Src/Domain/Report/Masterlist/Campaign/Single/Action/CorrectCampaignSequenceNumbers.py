from RdReportsReader.Src.Domain.Report.Metadata.Collection.Create import create_report_metadata_collection


def correct_campaign_sequence_numbers(metadata_collection, start_value):

    all_metadata = metadata_collection.all_metadata()

    corrected_metadata = []
    for single_metadata in all_metadata:

        if single_metadata.sequence_number().casefold() != "xxx".casefold():

            string_seq_no = __pad_to_3_digits_string(start_value)
            single_metadata.set_sequence_number(string_seq_no)

            corrected_metadata.append(single_metadata)

            start_value += 1

        else:
            corrected_metadata.append(single_metadata)

    return create_report_metadata_collection(corrected_metadata)


def __pad_to_3_digits_string(value):
    return "%03d" % value
