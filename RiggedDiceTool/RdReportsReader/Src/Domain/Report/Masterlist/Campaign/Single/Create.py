from RdReportsReader.Src.Config.Providers.ProvideInternalTokens import ProvideInternalTokens
from RdReportsReader.Src.Domain.Report.Masterlist.Campaign.Single.MasterlistCampaign import MasterlistCampaign
from RdReportsReader.Src.Domain.Report.Metadata.Single.Create import create_single_report_metadata_from_raw_record


def create_masterlist_empty_campaign(campaign_name_line):

    campaign_name = campaign_name_line[4: len(campaign_name_line)].strip()
    return MasterlistCampaign(campaign_name)


def create_masterlist_campaign(campaign_text_section):

    campaign = None

    for record in campaign_text_section.split('\n'):

        if ProvideInternalTokens().masterlist_campaign_header() in record:
            campaign = create_masterlist_empty_campaign(record)

        elif ProvideInternalTokens().one_of_reports_path_prefix() in record:
            metadata = create_single_report_metadata_from_raw_record(record)
            campaign.append_report_metadata(metadata)

        else:
            # ignore every other type of line
            pass

    return campaign
