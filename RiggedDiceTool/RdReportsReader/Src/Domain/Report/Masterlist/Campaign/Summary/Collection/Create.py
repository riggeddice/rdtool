from RdReportsReader.Src.Domain.Report.Masterlist.Campaign.Summary.Collection.CampaignSummaryCollection import CampaignSummaryCollection
from RdReportsReader.Src.Domain.Report.Masterlist.Campaign.Summary.Single.Create import create_campaign_summary


def create_campaign_summary_collection(report_collection, report_masterlist):

    all_campaigns = report_masterlist.all_campaigns()

    campaign_summaries = []
    for campaign in all_campaigns:

        reports_in_campaign = []
        for metadata in campaign.all_metadata():
            corresponding_report = report_collection.report_having_metadata(metadata)
            reports_in_campaign.append(corresponding_report)

        campaign_summary = create_campaign_summary(campaign, reports_in_campaign)
        campaign_summaries.append(campaign_summary)

    return CampaignSummaryCollection(campaign_summaries)
