from .Action.ToText import campaign_summary_to_rd_text


class CampaignSummary:

    def __init__(self, campaign, report_consequences):
        self.campaign = campaign
        self.report_consequences = report_consequences

    def campaign_name(self):
        return self.campaign.name()

    def to_rd_text(self):
        return campaign_summary_to_rd_text(self.campaign, self.report_consequences)
