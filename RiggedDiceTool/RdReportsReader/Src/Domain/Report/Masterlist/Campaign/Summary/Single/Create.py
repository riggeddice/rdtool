from RdReportsReader.Src.Domain.Report.Masterlist.Campaign.Summary.Single.CampaignSummary import CampaignSummary


def create_campaign_summary(campaign, reports_in_campaign):

    report_consequences = [report.consequences() for report in reports_in_campaign]
    return CampaignSummary(campaign, report_consequences)
