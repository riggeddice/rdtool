

def metadata_at_position(campaigns, position):

    selected_campaign = None
    offset = 0

    iterator = 0
    while True:
        current_campaign = campaigns[iterator]

        if offset + current_campaign.mission_count() >= position:
            selected_campaign = current_campaign
            break
        else:
            offset = offset + current_campaign.mission_count()
            iterator += 1

    in_campaign_position = position - offset
    selected_report_metadata = selected_campaign.metadata_at_position(in_campaign_position)
    return selected_report_metadata


def campaign_having_link(campaigns, weblink):

    selected_campaign = None

    for campaign in campaigns:
        operated_metadata = campaign.metadata_having_link(weblink)
        if operated_metadata is not None:
            selected_campaign = operated_metadata
            break

    return selected_campaign


def all_campaign_metadata(campaigns_collection):

    missions = []
    for campaign in campaigns_collection:
        missions = missions + campaign.all_metadata()

    return missions
