from RdReportsReader.Src.Domain.Report.Masterlist.Campaign.Single.Create import create_masterlist_campaign
from RdReportsReader.Src.Domain.Report.Masterlist.Complete.Structure.Core.ReportMasterlistCore import ReportMasterlistCore


def create_report_masterlist_core(report_masterlist_text):

    core_text_section = __extract_core_text_section_from(report_masterlist_text)  #.split("\n")
    campaign_text_sections = __split_core_text_into_campaign_texts(core_text_section)
    campaigns = __build_campaigns_from_text_sections(campaign_text_sections)
    masterlist_core = ReportMasterlistCore(campaigns)
    return masterlist_core


def __extract_core_text_section_from(report_masterlist_text):

    start_index = report_masterlist_text.index("+++ Inwazja")
    end_index = report_masterlist_text.index("++++ Koniec znanej historii Inwazji")
    masterlist_core_text = report_masterlist_text[start_index: end_index]
    return masterlist_core_text


def __split_core_text_into_campaign_texts(core_text_section):

    split_core = core_text_section.split("++++")
    only_relevant = split_core[1:]

    campaign_text_sections = ["++++" + section for section in only_relevant]

    return campaign_text_sections


def __build_campaigns_from_text_sections(text_sections):
    return [create_masterlist_campaign(section) for section in text_sections]
