from RdReportsReader.Src.Domain.Report.Masterlist.Complete.Structure.Prelude.ReportMasterlistPrelude import ReportMasterlistPrelude


def create_report_masterlist_prelude(report_masterlist_text):

    start_of_reports_index = report_masterlist_text.index("+++ Inwazja")

    prelude_text = report_masterlist_text[0: 0 + start_of_reports_index]
    prelude = ReportMasterlistPrelude(prelude_text.strip())

    return prelude
