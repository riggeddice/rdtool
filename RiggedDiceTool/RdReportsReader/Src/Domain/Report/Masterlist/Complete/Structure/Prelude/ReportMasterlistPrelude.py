class ReportMasterlistPrelude:

    def __init__(self, text):
        self._prelude_text = text

    def to_rd_text(self):
        return self._prelude_text
