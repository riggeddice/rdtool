

def report_masterlist_core_to_text(report_core):

    header = "+++ Inwazja:\n\n"
    campaigns_text = __campaigns_to_text(report_core.all_campaigns())

    combined = header + campaigns_text
    return combined


def __campaigns_to_text(campaigns):

    campaigns_text = ""

    for campaign in campaigns:
        campaigns_text.strip()
        campaigns_text = campaigns_text + campaign.to_rd_text() + "\n\n"

    return campaigns_text.strip()
