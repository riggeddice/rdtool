from RdReportsReader.Src.Domain.Report.Masterlist.Complete.Structure.Core.Action.Modify import correct_sequence_numbers
from .Action.Count import count_report_masterlist_core_metadata
from .Action.Retrieve import metadata_at_position
from .Action.Retrieve import campaign_having_link
from .Action.Retrieve import all_campaign_metadata
from .Action.ToText import report_masterlist_core_to_text


class ReportMasterlistCore:

    def __init__(self, campaign_list):
        self.campaign_list = campaign_list

    def all_campaigns(self):
        return self.campaign_list

    def all_metadata(self):
        return all_campaign_metadata(self.all_campaigns())

    def report_at_position(self, position):
        return metadata_at_position(self.all_campaigns(), position)

    def report_having_link(self, weblink):
        return campaign_having_link(self.all_campaigns(), weblink)

    def to_rd_text(self):
        return report_masterlist_core_to_text(self)

    def campaign_count(self):
        return len(self.all_campaigns())

    def metadata_count(self):
        return count_report_masterlist_core_metadata(self.all_campaigns())

    def correct_report_seq_numbers(self):
        correct_sequence_numbers(self.all_campaigns())
