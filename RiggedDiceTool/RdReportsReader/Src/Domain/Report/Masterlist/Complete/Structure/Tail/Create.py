from RdReportsReader.Src.Domain.Report.Masterlist.Complete.Structure.Tail.ReportMasterListTail import ReportMasterlistTail


def create_report_masterlist_tail(report_masterlist_text):

    start_of_tail_index = report_masterlist_text.index("++++ Koniec znanej historii Inwazji")

    tail_text = report_masterlist_text[start_of_tail_index: len(report_masterlist_text)]
    tail = ReportMasterlistTail(tail_text.strip())

    return tail
