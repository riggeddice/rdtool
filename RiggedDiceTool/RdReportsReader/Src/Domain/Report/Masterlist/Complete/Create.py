from RdReportsReader.Src.Domain.Report.Masterlist.Complete.ReportMasterlist import ReportMasterlist
from RdReportsReader.Src.Domain.Report.Masterlist.Complete.Structure.Core.Create import create_report_masterlist_core
from RdReportsReader.Src.Domain.Report.Masterlist.Complete.Structure.Prelude.Create import create_report_masterlist_prelude
from RdReportsReader.Src.Domain.Report.Masterlist.Complete.Structure.Tail.Create import create_report_masterlist_tail


def create_report_masterlist_from_elements(prelude, core, tail):

    masterlist = ReportMasterlist(prelude, core, tail)
    masterlist.correct_report_seq_numbers()
    return masterlist


def create_report_masterlist_from_text(report_masterlist_text):

    prelude = create_report_masterlist_prelude(report_masterlist_text)
    core = create_report_masterlist_core(report_masterlist_text)
    tail = create_report_masterlist_tail(report_masterlist_text)

    return create_report_masterlist_from_elements(prelude, core, tail)
