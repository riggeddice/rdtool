from .Action.ToText import report_masterlist_to_text


class ReportMasterlist:

    def __init__(self, prelude, core, tail):
        self.mlist_prelude = prelude
        self.mlist_core = core
        self.mlist_tail = tail

    # properties

    def prelude(self):
        return self.mlist_prelude

    def core(self):
        return self.mlist_core

    def tail(self):
        return self.mlist_tail

    # operations

    def report_number(self, position):
        return self.core().report_at_position(position)

    def report_having_link(self, link):
        return self.core().report_having_link(link)

    def to_rd_text(self):
        return report_masterlist_to_text(self.prelude(), self.core(), self.tail())

    def metadata_count(self):
        return self.core().metadata_count()

    def campaign_count(self):
        return self.core().campaign_count()

    def correct_report_seq_numbers(self):
        self.core().correct_report_seq_numbers()

    def all_metadata(self):
        return self.core().all_metadata()

    def all_campaigns(self):
        return self.core().all_campaigns()
