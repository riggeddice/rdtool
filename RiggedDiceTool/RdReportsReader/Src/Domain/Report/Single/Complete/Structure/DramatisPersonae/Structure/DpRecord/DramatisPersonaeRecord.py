from .Action.All import are_the_same_record_within_one_mission


class DramatisPersonaeRecord:

    def __init__(self, actor_type, name, mission_deed):

        self._actor_type = actor_type
        self._mission_name = name
        self._mission_deed = mission_deed

    def set_parent_dramatis_personae(self, dramatis_personae):
        self._parent_d_personae = dramatis_personae

    def name(self):
        return self._mission_name

    def actor_type(self):
        return self._actor_type

    def mission_deed(self):
        return self._mission_deed

    def sequence_number(self):
        return self._parent_d_personae.get_parent_report().sequence_number()

    def parent_dramatis_personae(self):
        return self._parent_d_personae

    def parent_report(self):
        return self._parent_d_personae.get_parent_report()

    def is_the_same_as(self, other_record):
        return are_the_same_record_within_one_mission(self, other_record)
