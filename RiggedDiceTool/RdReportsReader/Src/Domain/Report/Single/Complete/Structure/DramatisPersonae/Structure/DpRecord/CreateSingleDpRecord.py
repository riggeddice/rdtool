import re

from RdReportsReader.Src.Domain.Report.Single.Complete.Structure.DramatisPersonae.Structure.DpRecord.DramatisPersonaeRecord import DramatisPersonaeRecord
from RdReportsReader.Src.Config.Constants.RegexConstants import RegexConstants


def create_single_dp_record(record_line):

    parsed = re.findall(RegexConstants().dramatis_personae_record_from_report(), record_line)

    character_type = parsed[0][0].strip()
    potential_actor_name = parsed[0][2].strip()
    actor_deed = parsed[0][4].strip()

    if "[[[" in potential_actor_name:
        correction = re.findall("(\|)(.+?)(])", potential_actor_name)
        actor_name = correction[0][1].strip()
    else:
        actor_name = potential_actor_name

    return DramatisPersonaeRecord(character_type, actor_name, actor_deed)
