import re
from .CreateSingleDpRecord import create_single_dp_record
from RdReportsReader.Src.Config.Constants.RegexConstants import RegexConstants


def create_dp_records_from_valid_lines(record_lines_only):
    return [create_single_dp_record(record_line) for record_line in record_lines_only]


def create_dp_records_from_dramatis_personae_section(dramatis_personae_text_section):
    personae_section_lines = dramatis_personae_text_section.split("\n")
    record_lines_only = __extract_d_personae_record_lines_from(personae_section_lines)

    dp_records = create_dp_records_from_valid_lines(record_lines_only)
    return dp_records


def __extract_d_personae_record_lines_from(d_personae_section_lines):

    d_personae_record_lines = []
    for potential_record in d_personae_section_lines:

        regex_match = re.findall(RegexConstants().creature_type(), potential_record)

        if len(regex_match) > 0:
            d_personae_record_lines.append(potential_record)

    return d_personae_record_lines

