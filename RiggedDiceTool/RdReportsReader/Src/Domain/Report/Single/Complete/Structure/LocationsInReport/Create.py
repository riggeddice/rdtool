import re

from RdReportsReader.Src.Domain.Location.Metadata.SingleCompositeRecord.Create import create_single_location_metadata_composite_record_from_text_block
from RdReportsReader.Src.Domain.Location.Metadata.SingleCompositeRecord.Create import create_single_location_metadata_empty_composite_record
from RdReportsReader.Src.Config.Constants.RegexConstants import RegexConstants
from RdReportsReader.Src.Domain.Report.Single.Complete.Structure.LocationsInReport.LocationsInReport import LocationsInReport


def create_locations_in_report(rd_report):

    location_text_section = rd_report.location_text_section()

    if not location_text_section:
        locations_in_report = __create_empty()
    else:
        try:
            locations_in_report = __create_from_rd_text_section(location_text_section)

        except:
            locations_in_report = __create_empty()
            print("RD_PARSE_FAILURE_LOCATION: Report: " + rd_report.name())

    return locations_in_report


def __create_from_location_metadata_collection(location_metadata_collection):
    return LocationsInReport(location_metadata_collection)


def __create_empty():
    location_metadata_empty_collection = create_single_location_metadata_empty_composite_record()
    return __create_from_location_metadata_collection(location_metadata_empty_collection)


def __create_from_rd_text_section(location_text_section):
    location_lines = __split_location_text_section_into_valid_location_lines(location_text_section)
    location_metadata_collection = create_single_location_metadata_composite_record_from_text_block(location_lines)
    return __create_from_location_metadata_collection(location_metadata_collection)


def __split_location_text_section_into_valid_location_lines(location_text_section):
    location_lines = re.findall(RegexConstants().numbered_list_single_line(), location_text_section)
    return location_lines
