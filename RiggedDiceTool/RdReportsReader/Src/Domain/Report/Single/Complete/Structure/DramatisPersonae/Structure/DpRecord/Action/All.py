

def are_the_same_record_within_one_mission(record1, record2):

    if (record1.name() == record2.name()) and (record1.mission_deed() == record2.mission_deed()):
        return True

    return False
