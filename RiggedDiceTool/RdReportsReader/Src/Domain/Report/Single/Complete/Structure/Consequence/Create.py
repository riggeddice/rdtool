from RdReportsReader.Src.Domain.Report.Single.Complete.Structure.Consequence.ReportConsequence import ReportConsequence


def create_report_consequence(rd_report, report_metadata):

    consequences_text_section = rd_report.consequences_text_section()

    if not consequences_text_section:
        consequence_text = ""
    else:
        consequence_text = consequences_text_section

    return ReportConsequence(consequence_text, report_metadata)
