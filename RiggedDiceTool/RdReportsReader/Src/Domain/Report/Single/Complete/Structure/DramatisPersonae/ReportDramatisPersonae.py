from .Action.All import update_dp_records_at_creation
from .Action.All import siblings_of_dp_record


class ReportDramatisPersonae:

    def __init__(self, d_personae_records):
        self._dramatis_personae_records = d_personae_records
        update_dp_records_at_creation(self, self._dramatis_personae_records)

    def set_parent_report(self, parent_report):
        self._parent_report = parent_report

    def get_parent_report(self):
        return self._parent_report

    def get_dp_records(self):
        return self._dramatis_personae_records

    def get_only_dp_records_sibling_to(self, original_record):
        return siblings_of_dp_record(original_record, self._dramatis_personae_records)
