from RdReportsReader.Src.Domain.Report.Single.Complete.Structure.DramatisPersonae.ReportDramatisPersonae import ReportDramatisPersonae
from RdReportsReader.Src.Domain.Report.Single.Complete.Structure.DramatisPersonae.Structure.DpRecord.CreateDpRecords import create_dp_records_from_dramatis_personae_section


def create_dramatis_personae(rd_report):
    personae_text_section = rd_report.dramatis_personae_text_section()
    d_personae_records = create_dp_records_from_dramatis_personae_section(personae_text_section)
    return ReportDramatisPersonae(d_personae_records)
