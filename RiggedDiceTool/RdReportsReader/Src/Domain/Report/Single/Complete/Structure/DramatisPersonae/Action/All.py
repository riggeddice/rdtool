

def update_dp_records_at_creation(dramatis_personae, dramatis_personae_records):

    for record in dramatis_personae_records:
        record.set_parent_dramatis_personae(dramatis_personae)


def siblings_of_dp_record(original_record, dramatis_personae_records):
    siblings = [record for record in dramatis_personae_records if not record.is_the_same_as(original_record)]
    return siblings
