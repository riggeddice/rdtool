from .Action.ToText import consequence_to_rd_text


class ReportConsequence:

    def __init__(self, consequence_text, report_metadata):
        self._text = consequence_text
        self._report_metadata = report_metadata

    def to_rd_text(self):
        return consequence_to_rd_text(self._text, self._report_metadata)
