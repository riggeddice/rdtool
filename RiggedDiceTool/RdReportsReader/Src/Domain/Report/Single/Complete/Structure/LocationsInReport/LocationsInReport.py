class LocationsInReport:

    def __init__(self, location_metadata_composite_record):
        self.location_metadata_composite_record = location_metadata_composite_record

    def composite_metadata_record(self):
        return self.location_metadata_composite_record
