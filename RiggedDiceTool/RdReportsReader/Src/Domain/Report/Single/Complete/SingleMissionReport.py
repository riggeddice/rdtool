class SingleMissionReport:

    def __init__(self, report_text, report_metadata, dramatis_personae, locations_in_report, consequences):
        self._report_text = report_text
        self._report_metadata = report_metadata
        self._dramatis_personae = dramatis_personae
        self._locations_in_report = locations_in_report
        self._consequences_of_mission = consequences

    def getDramatisPersonae(self):
        return self._dramatis_personae

    def getLocationsInReport(self):
        return self._locations_in_report

    def consequences(self):
        return self._consequences_of_mission

    def missionDate(self):
        return self._report_metadata.date()

    def missionName(self):
        return self._report_metadata.name()

    def link(self):
        return self._report_metadata.weblink()

    def sequence_number(self):
        return self._report_metadata.sequence_number()

    def parentCampaign(self):
        return self._report_metadata.parentCampaign()
