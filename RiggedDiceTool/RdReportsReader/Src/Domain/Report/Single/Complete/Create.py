from RdReportsReader.Src.Domain.Report.Single.Complete.SingleMissionReport import SingleMissionReport
from RdReportsReader.Src.Domain.Report.Single.Complete.Structure.Consequence.Create import create_report_consequence
from RdReportsReader.Src.Domain.Report.Single.Complete.Structure.DramatisPersonae.Create import create_dramatis_personae
from RdReportsReader.Src.Domain.Report.Single.Complete.Structure.LocationsInReport.Create import create_locations_in_report


def create_single_mission_report(rd_report, report_masterlist):

    report_metadata = __find_corresponding_metadata_from_masterlist(rd_report, report_masterlist)

    report_text = rd_report.non_reserved_text_section_block()
    dramatis_personae = create_dramatis_personae(rd_report)
    locations = create_locations_in_report(rd_report)
    consequences = create_report_consequence(rd_report, report_metadata)

    report = SingleMissionReport(report_text, report_metadata, dramatis_personae, locations, consequences)

    dramatis_personae.set_parent_report(report)

    return report


def __find_corresponding_metadata_from_masterlist(rd_report, report_masterlist):

    metadata_to_find = rd_report.metadata()
    found_metadata = report_masterlist.report_having_link(metadata_to_find.weblink())
    return found_metadata
