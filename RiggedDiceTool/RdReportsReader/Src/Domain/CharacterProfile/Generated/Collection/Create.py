from RdReportsReader.Src.Domain.CharacterProfile.Generated.Collection.GeneratedCharacterProfileCollection import GeneratedCharacterProfileCollection
from RdReportsReader.Src.Domain.CharacterProfile.Generated.Single.Create import create_single_generated_character_profile


def create_generated_character_profile_collection(actor_names, actor_to_dp_records_collection, actor_to_actor_relation_map_collection):

    generated_profiles = []
    for actorName in actor_names:

        their_dp_records_collection = actor_to_dp_records_collection.get_for_single_actor(actorName)
        their_ata_relations_map = actor_to_actor_relation_map_collection.get_for_single_actor(actorName)

        single_profile = create_single_generated_character_profile(actorName, their_dp_records_collection, their_ata_relations_map)
        generated_profiles.append(single_profile)

    generated_profile_collection = GeneratedCharacterProfileCollection(generated_profiles)
    return generated_profile_collection
