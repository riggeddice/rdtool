from RdReportsReader.Src.Domain.CharacterProfile.CommonTools.ProfileListDataExtractor import ProfileListDataExtractor


class GeneratedCharacterProfileCollection:

    def __init__(self, generated_profiles):
        self.generated_profiles = generated_profiles

    def count(self):
        return len(self.generated_profiles)

    def all_names(self):
        return ProfileListDataExtractor().extract_all_names_from_collection(self.generated_profiles)

    def get_profile_by_name(self, single_name):
        return ProfileListDataExtractor().extract_selected_profile_from_collection_by(single_name, self.generated_profiles)
