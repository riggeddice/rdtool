from RdReportsReader.Src.Domain.CharacterProfile.Generated.Single.GeneratedSingleCharacterProfile import GeneratedSingleCharacterProfile


def create_single_generated_character_profile(actor_name, dp_records_collection, ata_relations_map):
    return GeneratedSingleCharacterProfile(actor_name, dp_records_collection, ata_relations_map)
