class GeneratedSingleCharacterProfile:

    def __init__(self, actor_name, dp_records_collection, ata_relations_map):
        self.actor_name = actor_name
        self.dp_records_collection = dp_records_collection
        self.ata_relations_map = ata_relations_map

    def name(self):
        return self.actor_name

    def all_dp_mission_records(self):
        return self.dp_records_collection.all_records()

    def allActorToActorRelations(self):
        return self.ata_relations_map.actor_to_actor_relations()
