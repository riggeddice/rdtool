from RdReportsReader.Src.Domain.CharacterProfile.Masterlist.ProfileMasterlist import ProfileMasterlist
from RdReportsReader.Src.Domain.CharacterProfile.Masterlist.Structure.Core.Create import create_profile_masterlist_core
from RdReportsReader.Src.Domain.CharacterProfile.Masterlist.Structure.Prelude.Create import create_profile_masterlist_prelude
from RdReportsReader.Src.Domain.CharacterProfile.Masterlist.Structure.Tail.Create import create_profile_masterlist_tail


def create_profile_masterlist_from_text(profile_masterlist_text):

    prelude = create_profile_masterlist_prelude(profile_masterlist_text)
    core = create_profile_masterlist_core(profile_masterlist_text)
    tail = create_profile_masterlist_tail(profile_masterlist_text)

    return ProfileMasterlist(prelude, core, tail)
