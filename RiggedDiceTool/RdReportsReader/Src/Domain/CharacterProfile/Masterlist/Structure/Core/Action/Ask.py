def count_all_metadata_in_factions(factions):

    metadata_count = 0
    for faction in factions:
        metadata_count = metadata_count + faction.metadata_count()

    return metadata_count


def does_contain_metadata_with_name(factions, name):

    contains = False
    for faction in factions:
        if faction.contains_metadata_with_name(name):
            contains = True
            break

    return contains
