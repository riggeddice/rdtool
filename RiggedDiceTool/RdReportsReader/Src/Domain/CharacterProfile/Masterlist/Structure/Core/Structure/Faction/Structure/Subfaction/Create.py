from RdReportsReader.Src.Domain.CharacterProfile.Masterlist.Structure.Core.Structure.Faction.Structure.Subfaction.ProfileMasterlistSubfaction import ProfileMasterlistSubfaction
from RdReportsReader.Src.Domain.CharacterProfile.Metadata.Collection.Create import create_profile_metadata_collection


def create_profile_masterlist_subfaction(name, metadata_text_block):
    metadata_collection = create_profile_metadata_collection(metadata_text_block)
    subfaction = ProfileMasterlistSubfaction(name, metadata_collection)
    return subfaction
