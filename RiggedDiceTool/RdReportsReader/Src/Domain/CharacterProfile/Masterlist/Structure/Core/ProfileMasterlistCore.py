from .Action.Ask import count_all_metadata_in_factions
from .Action.Ask import does_contain_metadata_with_name
from .Action.Retrieve import retrieve_all_metadata
from .Action.Retrieve import retrieve_metadata_with_name


class ProfileMasterlistCore:

    def __init__(self, factions):
        self.factions = factions

    def metadata_count(self):
        return count_all_metadata_in_factions(self.factions)

    def contains_profile_name(self, profile_name):
        return does_contain_metadata_with_name(self.factions, profile_name)

    def all_metadata(self):
        return retrieve_all_metadata(self.factions)

    def retrieve_metadata_having_name(self, profile_name):
        return retrieve_metadata_with_name(self.factions, profile_name)
