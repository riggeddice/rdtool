def does_contain_metadata_with_name(metadata_collection, subfactions, profile_name):

    if metadata_collection.contains_profile_name(profile_name):
        return True

    for single_subfaction in subfactions:
        if single_subfaction.contains_metadata_with_name(profile_name):
            return True

    return False


def count_all_metadata(metadata_collection, subfactions):

    metadata_count = metadata_collection.profiles_count()

    for subfaction in subfactions:
        metadata_count = metadata_count + subfaction.metadata_count()

    return metadata_count
