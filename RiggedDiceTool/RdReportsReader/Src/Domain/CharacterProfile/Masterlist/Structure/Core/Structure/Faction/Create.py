from RdReportsReader.Src.Domain.CharacterProfile.Masterlist.Structure.Core.Structure.Faction.ProfileMasterlistFaction import ProfileMasterlistFaction
from RdReportsReader.Src.Domain.CharacterProfile.Masterlist.Structure.Core.Structure.Faction.Structure.Subfaction.Create import create_profile_masterlist_subfaction
from RdReportsReader.Src.Domain.CharacterProfile.Metadata.Collection.Create import create_profile_metadata_collection
from RdReportsReader.Src.Domain.CharacterProfile.Metadata.Single.Action.All import is_record_a_valid_profile_metadata_candidate


def create_profile_masterlist_faction(name, text):

    subfactionless_text = __extract_subfactionless_text(text)
    subfactions_texts = __extract_subfaction_texts(text)

    subfactions = __create_subfactions_from(subfactions_texts)
    direct_metadata = __create_metadata_from(subfactionless_text)

    faction = ProfileMasterlistFaction(name, direct_metadata, subfactions)
    return faction


def __create_metadata_from(metadata_text_block):
    return create_profile_metadata_collection(metadata_text_block)


def __create_subfactions_from(subfactions_texts):

    subfactions = []
    for subfaction_text in subfactions_texts:
        if subfaction_text:
            name = subfaction_text.split("\n")[0].strip()
            metadata_text_block = subfaction_text.replace(name, "").strip()
            subfactions.append(create_profile_masterlist_subfaction(name, metadata_text_block))

    return subfactions


def __extract_subfaction_texts(text):

    relevant_texts = []
    if "+++++" in text:
        split_text = text.split("+++++")
        if not is_record_a_valid_profile_metadata_candidate(split_text[0]):
            relevant_texts = split_text
        else:
            relevant_texts = split_text[1:]

    return relevant_texts


def __extract_subfactionless_text(text):

    if "+++++" in text:
        split_text = text.split("+++++")
        relevant_text = split_text[0].strip()
    else:
        relevant_text = text.strip()

    return relevant_text
