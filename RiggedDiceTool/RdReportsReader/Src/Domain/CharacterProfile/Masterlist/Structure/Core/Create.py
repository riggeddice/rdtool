import re

from RdReportsReader.Src.Config.Constants.RegexConstants import RegexConstants
from RdReportsReader.Src.Domain.CharacterProfile.Masterlist.Structure.Core.ProfileMasterlistCore import ProfileMasterlistCore
from RdReportsReader.Src.Domain.CharacterProfile.Masterlist.Structure.Core.Structure.Faction.Create import create_profile_masterlist_faction


def create_profile_masterlist_core(profile_masterlist_text):

    core_section_text = __extract_relevant_text_from(profile_masterlist_text)
    factions_texts = __split_into_faction_text(core_section_text)
    factions = __create_factions(factions_texts)
    return ProfileMasterlistCore(factions)


def __create_factions(factions_texts):

    factions = []
    for factionText in factions_texts:
        name = factionText.split("\n")[0].strip()
        remainder = factionText.replace(name, "").strip()
        factions.append(create_profile_masterlist_faction(name, remainder))

    return factions


def __split_into_faction_text(core_section_text):

    faction_text_blocks = re.split(RegexConstants().faction_header_pattern(), core_section_text)
    non_empty_faction_texts = [ft for ft in faction_text_blocks if ft is not False]

    cleared_faction_text_blocks = __clear_factions_from_header(
        RegexConstants().faction_first_header_pattern(),
        RegexConstants().faction_header_pattern(),
        non_empty_faction_texts)
    return cleared_faction_text_blocks


def __extract_relevant_text_from(masterlist_wikidot_text):
    #TODO: move to property file
    beginning_index = masterlist_wikidot_text.index("+ Karty postaci Inwazji:") + len("+ Karty postaci Inwazji:")
    ending_index = masterlist_wikidot_text.index("++++ Koniec kart postaci Inwazji")
    core_text = masterlist_wikidot_text[beginning_index: ending_index]
    return core_text.strip()


def __clear_factions_from_header(faction_first_header_pattern, faction_general_header_pattern, non_empty_faction_texts):
    cleared_faction_text_blocks = []
    for block in non_empty_faction_texts:
        first_sub = re.sub(faction_general_header_pattern, "", block)
        second_sub = re.sub(faction_first_header_pattern, "", first_sub)
        cleared_faction_text_blocks.append(second_sub)
    return cleared_faction_text_blocks
