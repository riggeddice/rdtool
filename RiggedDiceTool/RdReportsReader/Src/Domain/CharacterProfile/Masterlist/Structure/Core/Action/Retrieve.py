def retrieve_all_metadata(faction_collection):

    metadata = []
    for faction in faction_collection:
        metadata = metadata + faction.all_metadata()

    return metadata


def retrieve_metadata_with_name(faction_collection, profile_name):

    for faction in faction_collection:
        if faction.contains_metadata_with_name(profile_name):
            return faction.retrieve_metadata_having_name(profile_name)

    return None
