from .Action.Ask import count_all_metadata
from .Action.Ask import does_contain_metadata_with_name
from .Action.Retrieve import retrieve_all_metadata
from .Action.Retrieve import retrieve_metadata_having_name


class ProfileMasterlistFaction:

    def __init__(self, name, metadata, subfactions):
        self._name = name
        self._metadata_collection = metadata
        self._subfactions = subfactions

    def metadata_count(self):
        return count_all_metadata(self._metadata_collection, self._subfactions)

    def all_metadata(self):
        return retrieve_all_metadata(self._metadata_collection, self._subfactions)

    def contains_metadata_with_name(self, profile_name):
        return does_contain_metadata_with_name(self._metadata_collection, self._subfactions, profile_name)

    def retrieve_metadata_having_name(self, profile_name):
        return retrieve_metadata_having_name(self._metadata_collection, self._subfactions, profile_name)
