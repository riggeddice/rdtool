def retrieve_all_metadata(metadata_collection, subfactions):

    metadata = metadata_collection.all_metadata()
    for subfaction in subfactions:
        metadata = metadata + subfaction.all_metadata()

    return metadata


def retrieve_metadata_having_name(metadata_collection, subfactions, name):

    if metadata_collection.contains_profile_name(name):
        return metadata_collection.retrieve_single_metadata_having_name(name)

    for single_subfaction in subfactions:
        if single_subfaction.contains_metadata_with_name(name):
            return single_subfaction.retrieve_metadata_having_name(name)

    raise "No metadata with name " + name + "present."
