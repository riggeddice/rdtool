from RdReportsReader.Src.Domain.CharacterProfile.Masterlist.Structure.Tail.ProfileMasterlistTail import ProfileMasterlistTail


def create_profile_masterlist_tail(profile_masterlist_text):

    start_of_tail_index = profile_masterlist_text.index("++++ Koniec kart postaci Inwazji")

    tail_text = profile_masterlist_text[start_of_tail_index: len(profile_masterlist_text)]
    tail = ProfileMasterlistTail(tail_text.strip())

    return tail
