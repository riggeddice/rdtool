from RdReportsReader.Src.Domain.CharacterProfile.Metadata.Collection.ProfileMetadataCollection import ProfileMetadataCollection
from RdReportsReader.Src.Domain.CharacterProfile.Metadata.Single.Action.All import is_record_a_valid_profile_metadata_candidate
from RdReportsReader.Src.Domain.CharacterProfile.Metadata.Single.Create import create_single_profile_metadata_from_raw_record


def create_profile_metadata_collection(metadata_text_block):

    split_text_block = metadata_text_block.split("\n")

    metadata_collection = []
    for metadata_candidate in split_text_block:

        if is_record_a_valid_profile_metadata_candidate(metadata_candidate):
            profile_metadata = create_single_profile_metadata_from_raw_record(metadata_candidate)
            metadata_collection.append(profile_metadata)

    return ProfileMetadataCollection(metadata_collection)
