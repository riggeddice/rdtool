import re
from RdReportsReader.Src.Config.Constants.RegexConstants import RegexConstants


def is_record_a_valid_profile_metadata_candidate(metadata_candidate):

    valid = re.search(RegexConstants().complete_dramatis_personae_record(), metadata_candidate)
    return None if not valid else valid
