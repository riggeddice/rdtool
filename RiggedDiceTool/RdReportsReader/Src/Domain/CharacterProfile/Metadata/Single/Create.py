import re

from RdReportsReader.Src.Domain.CharacterProfile.Metadata.Single.SingleProfileMetadata import SingleProfileMetadata
from RdReportsReader.Src.Config.Constants.RegexConstants import RegexConstants


def create_single_profile_metadata_from_raw_record(metadata_candidate):

    stripped = metadata_candidate.strip()

    regex_split = re.search(RegexConstants().profile_masterlist_metadata_record(), stripped)

    category = stripped[0:3]
    weblink = regex_split.group(2)
    name = regex_split.group(4)
    trivia = regex_split.group(6)

    return SingleProfileMetadata(name, weblink, category, trivia)
