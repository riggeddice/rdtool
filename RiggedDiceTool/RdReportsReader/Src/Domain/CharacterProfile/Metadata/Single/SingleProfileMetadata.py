class SingleProfileMetadata:

    def __init__(self, name, weblink, category, trivia):
        self._name = name
        self._weblink = weblink
        self._category = category
        self._trivia = trivia

    def ziplink(self):
        return self._weblink.replace(":", "_")

    def name(self):
        return self._name

    def to_rd_text(self):
        return "[[[" + self._weblink + "|" + self._name + "]]]"
