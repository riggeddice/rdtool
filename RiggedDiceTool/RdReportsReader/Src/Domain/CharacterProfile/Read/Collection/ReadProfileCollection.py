from RdReportsReader.Src.Domain.CharacterProfile.CommonTools.ProfileListDataExtractor import ProfileListDataExtractor


class ReadProfileCollection:

    def __init__(self, profiles):
        self.profiles = profiles

    def count(self):
        return len(self.profiles)

    def all_names(self):
        return ProfileListDataExtractor().extract_all_names_from_collection(self.profiles)
        
    def get_profile_by_name(self, single_name):
        return ProfileListDataExtractor().extract_selected_profile_from_collection_by(single_name, self.profiles)
