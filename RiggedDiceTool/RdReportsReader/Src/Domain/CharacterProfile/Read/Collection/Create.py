from RdReportsReader.Src.Domain.CharacterProfile.Read.Collection.ReadProfileCollection import ReadProfileCollection
from RdReportsReader.Src.Domain.CharacterProfile.Read.Single.Create import create_single_read_profile


def create_read_profile_collection(rd_profiles):

    profiles = [create_single_read_profile(rd_profile) for rd_profile in rd_profiles]
    return ReadProfileCollection(profiles)
