class SingleReadProfile:

    def __init__(self, character_name, profile_sections):
        self.character_name = character_name
        self.profile_sections = profile_sections

    def name(self):
        return self.character_name

    def section_collection(self):
        return self.profile_sections
