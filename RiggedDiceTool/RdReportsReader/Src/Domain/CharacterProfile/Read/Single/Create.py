from RdReportsReader.Src.Domain.CharacterProfile.BuildingBlocks.Section.Collection.Create import create_section_collection_from_text
from RdReportsReader.Src.Domain.CharacterProfile.Read.Single.SingleReadProfile import SingleReadProfile


def create_single_read_profile(rd_profile):

    character_name = rd_profile.name()
    nameless_rd_profile_text = __profile_text_without_name(rd_profile, character_name)
    profile_sections = create_section_collection_from_text(nameless_rd_profile_text)

    single_profile = SingleReadProfile(character_name, profile_sections)
    return single_profile


def __profile_text_without_name(read_rd_profile, character_name):
    text = read_rd_profile.text()
    result = text.replace("+ " + character_name, '')
    return result
