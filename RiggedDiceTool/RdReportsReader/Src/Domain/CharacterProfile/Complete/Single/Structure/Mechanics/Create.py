from collections import namedtuple

from RdReportsReader.Src.Domain.CharacterProfile.BuildingBlocks.Section.Collection.Create import create_empty_section_collection
from RdReportsReader.Src.Domain.CharacterProfile.Complete.Single.Structure.Mechanics.CharacterMechanics import CharacterMechanics


def create_mechanics():

    def empty():
        section_collection = create_empty_section_collection()
        return CharacterMechanics(section_collection)

    def from_profile(read_profile):
        return CharacterMechanics(read_profile.section_collection())

    creator = namedtuple("Creator", ["empty", "from_profile"])
    return creator(empty=empty, from_profile=from_profile)
