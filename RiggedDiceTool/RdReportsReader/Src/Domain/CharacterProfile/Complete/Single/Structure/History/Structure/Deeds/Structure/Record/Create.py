from RdReportsReader.Src.Domain.CharacterProfile.Complete.Single.Structure.History.Structure.Deeds.Structure.Record.DeedRecord import DeedRecord


def create_deed_record(single_dp_record):

    report = single_dp_record.parent_report()

    mission_deed = single_dp_record.mission_deed()
    mission_name = report.missionName()
    mission_sequence_number = report.sequence_number()
    link = report.link()

    record = DeedRecord(mission_deed, mission_sequence_number, link, mission_name)
    return record
