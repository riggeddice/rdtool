from RdReportsReader.Src.Domain.CharacterProfile.Complete.Single.Structure.History.Structure.Deeds.DeedsHistory import DeedsHistory
from RdReportsReader.Src.Domain.CharacterProfile.Complete.Single.Structure.History.Structure.Deeds.Structure.Record.Create import create_deed_record


def create_deeds_history(generated_profile):

    mission_records = generated_profile.all_dp_mission_records()

    mission_deeds = []
    for singleRecord in mission_records:
        mission_deed_record = create_deed_record(singleRecord)
        mission_deeds.append(mission_deed_record)

    presence_history = DeedsHistory(mission_deeds)
    return presence_history


def create_empty_deeds_history():
    return DeedsHistory([])
