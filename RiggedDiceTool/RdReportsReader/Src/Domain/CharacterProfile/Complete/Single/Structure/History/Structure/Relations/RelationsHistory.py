from .Action.ToSection import relations_history_to_section


class RelationsHistory:

    def __init__(self, ata_relation_history_records):
        self.ata_relation_history_records = ata_relation_history_records

    def to_profile_section(self, masterlist):
        self.sort_for_display()
        return relations_history_to_section(self.ata_relation_history_records, masterlist)

    def sort_for_display(self):
        self.ata_relation_history_records = sorted(self.ata_relation_history_records,
                                                   key=lambda record: (-record.intensity(), record.related_actor_name()))
