from RdReportsReader.Src.Domain.CharacterProfile.BuildingBlocks.Section.Single.Create import create_single_profile_section_from_headline_text


def deeds_to_profile_section(mission_deed_records):

    headline = "Dokonania:"

    combined_section_text = ""
    for single_record in mission_deed_records:
        combined_section_text = combined_section_text + single_record.to_rd_string() + "\n"

    cleaned_section_text = combined_section_text.strip()

    profile_section = create_single_profile_section_from_headline_text(headline, cleaned_section_text)
    return profile_section
