from RdReportsReader.Src.AppLogic.FormatText import change_text_into_hyperlink


def ata_relation_history_record_to_rd_table_row(related_actor_name, intensity, mission_records, profile_masterlist):

    mission_identifier_composite_string = ""
    for single_record in mission_records:
        mission_identifier_composite_string = mission_identifier_composite_string + __form_single_identifier_from(single_record)

    mission_identifier_string = mission_identifier_composite_string[:-2]
    return "||" + __potential_name_link(related_actor_name, profile_masterlist) + "||" + str(intensity) + "||" + mission_identifier_string + "||"


def __potential_name_link(related_actor_name, profile_masterlist):

    if profile_masterlist.contains_profile_name(related_actor_name):
        actor_metadata = profile_masterlist.retrieve_metadata_having_name(related_actor_name)
        actual_actor_identifier = actor_metadata.to_rd_text()
    else:
        actual_actor_identifier = related_actor_name

    return actual_actor_identifier


def __form_single_identifier_from(mission_record):

    mission_date = str(mission_record.parent_report().missionDate())
    mission_link = mission_record.parent_report().link()

    return change_text_into_hyperlink(mission_link, mission_date) + ", "
