from RdReportsReader.Src.Domain.CharacterProfile.Complete.Single.Structure.History.Structure.Relations.Structure.Record.RelationRecord import RelationRecord


def create_relation_record(actor_name, intensity, dp_records):
    return RelationRecord(actor_name, intensity, dp_records)
