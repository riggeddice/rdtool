from .Action.ToSection import presence_history_to_profile_section


class PresenceHistory:

    def __init__(self, mission_presences):
        self.mission_presence_records = mission_presences

    def to_profile_section(self):
        self.sort_for_display()
        return presence_history_to_profile_section(self.mission_presence_records)

    def sort_for_display(self):
        self.mission_presence_records = sorted(self.mission_presence_records, key=lambda record: record.sequence_number())
