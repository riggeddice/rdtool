from RdReportsReader.Src.Domain.CharacterProfile.BuildingBlocks.Section.Single.Create import create_single_profile_section_from_headline_text


def presence_history_to_profile_section(mission_presence_records):

    headline = "Na misjach:"

    overall_report_text = __aggregate_report(mission_presence_records)
    presence_report_text = __single_records_report(mission_presence_records)
    cleaned_presence_section_text = presence_report_text.strip()

    cleaned_section_text = overall_report_text + "\n" + cleaned_presence_section_text

    return create_single_profile_section_from_headline_text(headline, cleaned_section_text)


def __single_records_report(mission_presence_records):

    combined_section_text = "||~ Data ||~ Numer ||~ Misja ||~ W kampanii ||\n"

    for single_record in mission_presence_records:
        combined_section_text = combined_section_text + single_record.to_rd_string() + "\n"

    return combined_section_text


def __aggregate_report(mission_presence_records):

    overall_presence_on_missions = "+++++ Postać wystąpiła na: " + str(len(mission_presence_records)) + " misjach."
    aggregate_report = overall_presence_on_missions + "\n" + __overall_presence_on_campaigns(mission_presence_records) + "\n"
    return aggregate_report


def __overall_presence_on_campaigns(mission_presence_records):

    all_campaign_names = []
    for record in mission_presence_records:
        all_campaign_names.append(record.parent_campaign_name())

    unique_names = list(set(all_campaign_names))

    campaign_string = ""

    for name in unique_names:
        campaign_string += name + ", "

    campaign_string = campaign_string[:-2]

    overall_presence = "+++++ Postać wystąpiła w: " + str(len(unique_names)) + " kampaniach: " + campaign_string
    return overall_presence
