class CharacterMechanics:

    def __init__(self, section_collection):
        self.section_collection = section_collection

    def delete_generated_sections(self, generated_profile_section_collection):
        self.section_collection.delete_duplicate_sections(generated_profile_section_collection)
        
    def to_rd_text(self):
        return self.section_collection.to_rd_text()
