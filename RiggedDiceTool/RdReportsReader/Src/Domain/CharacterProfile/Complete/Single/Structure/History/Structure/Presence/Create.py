from RdReportsReader.Src.Domain.CharacterProfile.Complete.Single.Structure.History.Structure.Presence.PresenceHistory import PresenceHistory
from RdReportsReader.Src.Domain.CharacterProfile.Complete.Single.Structure.History.Structure.Presence.Structure.Record.Create import create_presence_record


def create_mission_presence_history(generated_profile):

    mission_records = generated_profile.all_dp_mission_records()

    mission_presences = []
    for single_record in mission_records:
        single_presence = create_presence_record(single_record)
        mission_presences.append(single_presence)

    return PresenceHistory(mission_presences)


def create_empty_mission_presence_history():
    return PresenceHistory([])
