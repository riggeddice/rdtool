from .Action.ToProfileSection import deeds_to_profile_section


class DeedsHistory:

    def __init__(self, mission_deeds):
        self.mission_deed_records = mission_deeds

    def to_profile_section(self):
        self.sort_for_display()
        return deeds_to_profile_section(self.mission_deed_records)

    def sort_for_display(self):
        self.mission_deed_records = sorted(self.mission_deed_records, key=lambda record: record.sequence_number())
