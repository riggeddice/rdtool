from RdReportsReader.Src.Domain.CharacterProfile.Complete.Single.Structure.History.Structure.Relations.RelationsHistory import RelationsHistory
from RdReportsReader.Src.Domain.CharacterProfile.Complete.Single.Structure.History.Structure.Relations.Structure.Record.Create import create_relation_record


def create_ata_relation_history(generated_profile):

    relation_map = generated_profile.allActorToActorRelations()
    names = relation_map.keys()

    ata_relation_history_records = []
    for singleName in names:
        relation_with_actor = relation_map[singleName]

        intensity = relation_with_actor.intensity()
        dp_records = relation_with_actor.dp_records()

        single_historical_relation_record = create_relation_record(singleName, intensity, dp_records)
        ata_relation_history_records.append(single_historical_relation_record)

    return RelationsHistory(ata_relation_history_records)


def create_empty_ata_relation_history():
    return RelationsHistory([])
