from collections import namedtuple

from RdReportsReader.Src.Domain.CharacterProfile.Complete.Single.Structure.History.CharacterHistory import CharacterHistory
from RdReportsReader.Src.Domain.CharacterProfile.Complete.Single.Structure.History.Structure.Deeds.Create import create_deeds_history
from RdReportsReader.Src.Domain.CharacterProfile.Complete.Single.Structure.History.Structure.Deeds.Create import create_empty_deeds_history
from RdReportsReader.Src.Domain.CharacterProfile.Complete.Single.Structure.History.Structure.Presence.Create import create_empty_mission_presence_history
from RdReportsReader.Src.Domain.CharacterProfile.Complete.Single.Structure.History.Structure.Presence.Create import create_mission_presence_history
from RdReportsReader.Src.Domain.CharacterProfile.Complete.Single.Structure.History.Structure.Relations.Create import create_ata_relation_history
from RdReportsReader.Src.Domain.CharacterProfile.Complete.Single.Structure.History.Structure.Relations.Create import create_empty_ata_relation_history


def create_history():

    def empty():

        mission_presence = create_empty_mission_presence_history()
        mission_deeds = create_empty_deeds_history()
        actor_to_actor_relation_map = create_empty_ata_relation_history()

        history = CharacterHistory(mission_presence, mission_deeds, actor_to_actor_relation_map)
        return history

    def from_profile(generated_profile):

        mission_presence = create_mission_presence_history(generated_profile)
        mission_deeds = create_deeds_history(generated_profile)
        actor_to_actor_relation_map = create_ata_relation_history(generated_profile)

        history = CharacterHistory(mission_presence, mission_deeds, actor_to_actor_relation_map)
        return history

    creator = namedtuple("Creator", ["empty", "from_profile"])
    return creator(empty=empty, from_profile=from_profile)
