from RdReportsReader.Src.Domain.CharacterProfile.Complete.Single.Structure.History.Structure.Presence.Structure.Record.PresenceRecord import PresenceRecord


def create_presence_record(single_dp_record):

    report = single_dp_record.parent_report()

    mission_date = report.missionDate()
    mission_name = report.missionName()
    mission_seq_no = report.sequence_number()
    link = report.link()
    campaign_name = report.parentCampaign().name()

    return PresenceRecord(mission_date, mission_name, mission_seq_no, link, campaign_name)
