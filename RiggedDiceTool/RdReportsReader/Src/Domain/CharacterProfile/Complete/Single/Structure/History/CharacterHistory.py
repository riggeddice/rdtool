from .Action.ToSections import history_to_text_sections


class CharacterHistory:

    def __init__(self, mission_presence, mission_deeds, actor_to_actor_relation_history):
        self._presence = mission_presence
        self._deeds = mission_deeds
        self._ata_relation_history = actor_to_actor_relation_history

    def to_rd_text(self, profile_masterlist):
        return self.to_profile_sections(profile_masterlist).to_rd_text()

    def to_profile_sections(self, profile_masterlist):
        return history_to_text_sections(self._presence, self._deeds, self._ata_relation_history, profile_masterlist)
