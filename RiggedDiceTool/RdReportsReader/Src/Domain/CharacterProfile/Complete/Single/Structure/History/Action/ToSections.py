from RdReportsReader.Src.Domain.CharacterProfile.BuildingBlocks.Section.Collection.Create import create_section_collection


def history_to_text_sections(mission_presence, mission_deeds, ata_relation_history, profile_masterlist):

    presence_section = mission_presence.to_profile_section()
    deeds_section = mission_deeds.to_profile_section()
    ata_relation_section = ata_relation_history.to_profile_section(profile_masterlist)

    history_section_list = [presence_section, deeds_section, ata_relation_section]

    section_collection = create_section_collection(history_section_list)
    return section_collection
