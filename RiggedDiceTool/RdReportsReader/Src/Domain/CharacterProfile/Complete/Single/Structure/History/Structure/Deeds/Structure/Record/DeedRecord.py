from RdReportsReader.Src.AppLogic.FormatText import change_text_into_hyperlink


class DeedRecord:

    def __init__(self, mission_deed, mission_sequence_number, link, mission_name):
        self.mission_deed = mission_deed
        self.mission_sequence_number = mission_sequence_number
        self.link = link
        self.mission_name = mission_name

    def sequence_number(self):
        return self.mission_sequence_number

    def to_rd_string(self):
        return "|| " + change_text_into_hyperlink(self.link, self.mission_name) + " || " + self.mission_deed + " ||"
