

def single_profile_to_rd_text(name, mechanics, history, masterlist):

    name_rd_text = "+ " + name
    mechanics_rd_text = mechanics.to_rd_text().strip()
    history_rd_text = history.to_rd_text(masterlist).strip()

    complete_profile_rd_text = name_rd_text + "\n\n" + mechanics_rd_text + "\n\n" + history_rd_text
    return complete_profile_rd_text