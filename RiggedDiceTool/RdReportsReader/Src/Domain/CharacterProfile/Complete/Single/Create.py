from RdReportsReader.Src.Domain.CharacterProfile.Complete.Single.SingleCompleteProfile import SingleCompleteProfile
from RdReportsReader.Src.Domain.CharacterProfile.Complete.Single.Structure.History.Create import create_history
from RdReportsReader.Src.Domain.CharacterProfile.Complete.Single.Structure.Mechanics.Create import create_mechanics


def create_single_complete_profile(character_name, read_profile, generated_profile, profile_masterlist):

    character_history = __create_nullable(generated_profile, create_history())
    character_mechanics = __create_nullable(read_profile, create_mechanics())

    generated_profile_sections = character_history.to_profile_sections(profile_masterlist)
    character_mechanics.delete_generated_sections(generated_profile_sections)

    complete_profile = SingleCompleteProfile(character_name, character_mechanics, character_history, profile_masterlist)
    return complete_profile


def __create_nullable(generated_profile, creator):

    if generated_profile is None:
        component = creator.empty()
    else:
        component = creator.from_profile(generated_profile)

    return component
