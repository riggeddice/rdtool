from .Action.ToText import single_profile_to_rd_text


class SingleCompleteProfile:

    def __init__(self, name, mechanics, history, masterlist):
        self._name = name
        self._mechanics = mechanics
        self._history = history
        self._masterlist = masterlist

    def name(self):
        return self._name

    def to_rd_text(self):
        return single_profile_to_rd_text(self._name, self._mechanics, self._history, self._masterlist)
