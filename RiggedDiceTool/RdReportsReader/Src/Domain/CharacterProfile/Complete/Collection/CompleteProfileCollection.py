class CompleteProfileCollection:

    def __init__(self, complete_profiles):
        self.profiles = complete_profiles

    def count(self):
        return len(self.profiles)

    def profile_at_index(self, index):
        return self.profiles[index]

    def profile_having_name(self, character_name):
        profile = [single for single in self.profiles if single.name() == character_name][0]
        return profile

    def all(self):
        return self.profiles
