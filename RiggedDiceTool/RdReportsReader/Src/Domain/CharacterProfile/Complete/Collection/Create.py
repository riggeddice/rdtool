from RdReportsReader.Src.Domain.CharacterProfile.Complete.Collection.CompleteProfileCollection import CompleteProfileCollection
from RdReportsReader.Src.Domain.CharacterProfile.Complete.Single.Create import create_single_complete_profile


def create_complete_profile_collection(read_profiles, generated_profiles, profile_masterlist):

    all_unique_names = __sorted_unique_list_of_names(read_profiles, generated_profiles)

    complete_profiles = []
    for single_name in all_unique_names:
        single_read_profile = read_profiles.get_profile_by_name(single_name)
        single_generated_profile = generated_profiles.get_profile_by_name(single_name)

        single_complete_profile = create_single_complete_profile(single_name, single_read_profile, single_generated_profile, profile_masterlist)
        complete_profiles.append(single_complete_profile)

    collection = CompleteProfileCollection(complete_profiles)
    return collection


def __sorted_unique_list_of_names(read_profiles, generated_profiles):

    read_profile_names = read_profiles.all_names()
    generated_profile_names = generated_profiles.all_names()

    read_profile_names.extend(generated_profile_names)
    merged_names = list(set(read_profile_names))
    merged_names.sort()
    return merged_names

