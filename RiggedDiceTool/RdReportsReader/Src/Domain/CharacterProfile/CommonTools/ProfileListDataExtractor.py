class ProfileListDataExtractor:

    def extract_all_names_from_collection(self, profile_list):
        return [item.name() for item in profile_list]

    def extract_selected_profile_from_collection_by(self, actor_name, profile_list):

        for singleProfile in profile_list:
            if singleProfile.name() == actor_name:
                return singleProfile

        return None
