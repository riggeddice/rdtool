import re

from RdReportsReader.Src.Config.Constants.RegexConstants import RegexConstants
from RdReportsReader.Src.Domain.CharacterProfile.BuildingBlocks.Section.Collection.SectionCollection import SectionCollection
from RdReportsReader.Src.Domain.CharacterProfile.BuildingBlocks.Section.Single.Create import create_single_profile_section_from_text_block


def create_section_collection_from_text(rd_report_text_without_name):

    text_section_prototypes = re.split(RegexConstants().starts_with_h3(), rd_report_text_without_name)

    profile_sections = []
    for sectionTextCandidate in text_section_prototypes:
        if sectionTextCandidate.strip():
            single_profile_section = create_single_profile_section_from_text_block(sectionTextCandidate)

            if not single_profile_section.is_empty():
                profile_sections.append(single_profile_section)

    return create_section_collection(profile_sections)


def create_empty_section_collection():
    return create_section_collection([])


def create_section_collection(section_list):
    return SectionCollection(section_list)
