from RdReportsReader.Src.Domain.CharacterProfile.BuildingBlocks.Section.Collection.Action.Sections import deduplicate_sections
from RdReportsReader.Src.Domain.CharacterProfile.BuildingBlocks.Section.Collection.Action.ToText import sections_to_rd_text


class SectionCollection:

    def __init__(self, profile_sections):
        self.profile_sections = profile_sections

    def all_sections(self):
        return self.profile_sections

    def delete_duplicate_sections(self, other_collection):
        self.profile_sections = deduplicate_sections(self.all_sections(), other_collection.all_sections())

    def to_rd_text(self):
        return sections_to_rd_text(self.profile_sections)
