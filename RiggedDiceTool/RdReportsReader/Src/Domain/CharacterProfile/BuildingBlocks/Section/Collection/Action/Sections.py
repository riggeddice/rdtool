def deduplicate_sections(originals, duplicates):

    new_list = originals[:]
    for potentially_duplicate_section in duplicates:
        for core_section in originals:
            if potentially_duplicate_section.headline() == core_section.headline():
                new_list.remove(core_section)
                break

    return new_list
