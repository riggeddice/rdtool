def sections_to_rd_text(text_sections):

    plain_text = ""
    for single_section in text_sections:
        plain_text = plain_text + single_section.to_rd_text()

    return plain_text
