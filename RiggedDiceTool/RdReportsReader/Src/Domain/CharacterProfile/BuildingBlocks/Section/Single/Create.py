import re

from RdReportsReader.Src.Domain.CharacterProfile.BuildingBlocks.Section.Single.SingleProfileSection import SingleProfileSection
from RdReportsReader.Src.Config.Constants.RegexConstants import RegexConstants


def create_single_profile_section_from_text_block(section_text_candidate):

    headline_list = re.findall(RegexConstants().first_line_is_word(), section_text_candidate)

    if len(headline_list) > 0:
        headline = headline_list[0]
    else:
        headline = ""

    section_text = section_text_candidate.replace(headline, '').strip()

    single_profile_section = create_single_profile_section_from_headline_text(headline, section_text)
    return single_profile_section


def create_single_profile_section_from_headline_text(headline, section_text):

    single_profile_section = SingleProfileSection(headline, section_text)
    return single_profile_section
