class SingleProfileSection:

    def __init__(self, headline, sectionText):
        self.headline_text = headline
        self.section_text = sectionText

    def headline(self):
        return self.headline_text

    def is_empty(self):

        if self.headline_text.strip() == "" and self.section_text.strip() == "":
            return True
        else:
            return False

    def to_rd_text(self):
        text = "+++ " + self.headline_text.strip() + "\n\n" + self.section_text.strip() + "\n\n"
        return text
