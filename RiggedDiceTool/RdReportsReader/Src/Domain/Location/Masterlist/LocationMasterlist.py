from .Action.ToText import location_masterlist_to_text


class LocationMasterlist:

    def __init__(self, composite_metadata):
        self.composite_metadata = composite_metadata

    def to_rd_text(self):
        return location_masterlist_to_text(self.composite_metadata)
