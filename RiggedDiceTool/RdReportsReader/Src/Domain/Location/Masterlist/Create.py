from RdReportsReader.Src.Domain.Location.Masterlist.LocationMasterlist import LocationMasterlist


def create_location_masterlist_from_composite_metadata(composite_metadata):
    return LocationMasterlist(composite_metadata)


def create_location_masterlist_from_report_collection(report_collection):

    all_locations_on_missions = report_collection.all_locations_on_missions()
    combined_location_composite_record = all_locations_on_missions.to_single_composite_record()

    masterlist = create_location_masterlist_from_composite_metadata(combined_location_composite_record)
    return masterlist
