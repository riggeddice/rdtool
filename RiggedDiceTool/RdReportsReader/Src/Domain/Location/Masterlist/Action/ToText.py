from RdReportsReader.Src.Config.Providers.ProvideInternalTokens import ProvideInternalTokens


def location_masterlist_to_text(masterlist_node):

    line = ProvideInternalTokens().line()
    city_depth = ProvideInternalTokens().location_city_depth()

    header = ProvideInternalTokens().top_level_header() + " " + ProvideInternalTokens().location_header() + ":\n"
    partial_prefix = "\n" + line + "\nWidok do poziomu miast:\n\n"
    partial_text = masterlist_node.to_rd_text(city_depth)
    full_prefix = "\n" + line + "\nWidok całości:\n\n"
    full_text = masterlist_node.to_rd_text()
    result = header + partial_prefix + partial_text + full_prefix + full_text
    return result
