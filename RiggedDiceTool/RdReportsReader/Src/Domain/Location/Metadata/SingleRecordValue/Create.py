import re

from RdReportsReader.Src.Domain.Location.Metadata.SingleRecordValue.SingleLocationMetadataRecordValue import SingleLocationMetadataRecordValue
from RdReportsReader.Src.Config.Constants.RegexConstants import RegexConstants


def create_location_metadata_record_value_root():
    return SingleLocationMetadataRecordValue("*", "")


def create_location_metadata_record_value(single_line):

    delimiter = ","

    record = re.findall(RegexConstants().location_masterlist_meadata_name(), single_line)[0][1]
    stripped = record.strip()

    first_delimiter_index = stripped.find(delimiter)

    if first_delimiter_index == -1:
        name = stripped
        deed = ""
    else:
        name = stripped[0: first_delimiter_index]
        deed = stripped[first_delimiter_index + len(delimiter): len(stripped)].strip()

    return SingleLocationMetadataRecordValue(name, deed)
