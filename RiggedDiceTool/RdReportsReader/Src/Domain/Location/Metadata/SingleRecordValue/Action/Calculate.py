import re
from RdReportsReader.Src.Config.Constants.RegexConstants import RegexConstants


def calculate_depth_level(location_line, indentation_depth_symbol):
    depth_section = re.findall(RegexConstants().numbered_list_entry(), location_line)[0]
    depth = depth_section.count(indentation_depth_symbol)
    return depth
