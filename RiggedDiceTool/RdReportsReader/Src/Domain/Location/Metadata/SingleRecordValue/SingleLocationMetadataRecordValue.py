from .Action.ToText import record_value_to_rd_text


class SingleLocationMetadataRecordValue:

    def __init__(self, name, deed):
        self.location_name = name
        self.location_deed = deed

    def name(self):
        return self.location_name

    def deed(self):
        return self.location_deed

    def to_rd_text(self, indentation_level):
        return record_value_to_rd_text(self.location_name, indentation_level)
