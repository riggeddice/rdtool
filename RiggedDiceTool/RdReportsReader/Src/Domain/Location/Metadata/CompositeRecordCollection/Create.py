from RdReportsReader.Src.Domain.Location.Metadata.CompositeRecordCollection.LocationCompositeRecordCollection import LocationCompositeRecordCollection


def create_location_composite_record_collection(composite_records):
    return LocationCompositeRecordCollection(composite_records)
