from .Action.Merge import merge_into_single_composite_record


class LocationCompositeRecordCollection:

    def __init__(self, composite_records):
        self.composite_records = composite_records

    def to_single_composite_record(self):
        return merge_into_single_composite_record(self.composite_records)
