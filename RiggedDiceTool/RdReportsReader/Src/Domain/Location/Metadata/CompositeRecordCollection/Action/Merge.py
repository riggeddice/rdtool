from RdReportsReader.Src.Domain.Location.Metadata.RecordValueCollection.Action.Compare import do_location_record_value_collections_have_different_origins_same_root
from RdReportsReader.Src.Domain.Location.Metadata.SingleCompositeRecord.Create import create_single_location_metadata_root_composite_record


def merge_into_single_composite_record(composite_records):

    merged_record = all_composites_to_common_root(composite_records)
    all_record_value_collections = merged_record.list_of_all_value_collections()
    purge_duplicate_composites(merged_record.root())
    __update_records_with_values(merged_record, all_record_value_collections)
    return merged_record


def purge_duplicate_composites(node):

    already_present = {}
    children_to_delete = []

    for single_child in node.children():

        if single_child.normalized_name() in already_present:

            child_to_link_grandchildren_to = already_present[single_child.normalized_name()]
            duplicate_child = single_child

            children_to_delete.append(duplicate_child)

            for grandchildToLink in duplicate_child.children():
                child_to_link_grandchildren_to.add_child(grandchildToLink)
                grandchildToLink.set_parent(child_to_link_grandchildren_to)

        else:
            already_present[single_child.normalized_name()] = single_child

    for childToDelete in children_to_delete:
        node.remove_child(childToDelete)

    for single_child in node.children():
        purge_duplicate_composites(single_child)

    node.children().sort(key=lambda x: x.normalized_name())


def all_composites_to_common_root(composite_records):

    merged = create_single_location_metadata_root_composite_record()

    for record in composite_records:
        for singleChild in record.children():
            singleChild.set_parent(merged)
            merged.add_child(singleChild)

    return merged


def __update_records_with_values(merged_record, all_record_value_collections):

    dominant_collections = merged_record.list_of_all_value_collections()

    for dominant_collection in dominant_collections:
        for potentially_orphaned_collections in all_record_value_collections:
            if do_location_record_value_collections_have_different_origins_same_root(dominant_collection, potentially_orphaned_collections):
                dominant_collection.extend_record_value_collection(potentially_orphaned_collections)
