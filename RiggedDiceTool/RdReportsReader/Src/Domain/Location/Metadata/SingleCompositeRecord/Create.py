from uuid import uuid4

from RdReportsReader.Src.Config.Constants.RegexConstants import RegexConstants
from RdReportsReader.Src.Domain.Location.Metadata.RecordValueCollection.Create import create_location_record_value_collection
from RdReportsReader.Src.Domain.Location.Metadata.RecordValueCollection.Create import create_location_record_value_collection_only_with_root
from RdReportsReader.Src.Domain.Location.Metadata.SingleRecordValue.Action.Calculate import calculate_depth_level


def create_single_location_metadata_composite_record_wrap_value_collection(value_collection, depth):
    wrapping_composite_record = SingleLocationMetadataCompositeRecord(value_collection, depth, uuid4())
    value_collection.set_parent_record(wrapping_composite_record)
    return wrapping_composite_record


def create_single_location_metadata_root_composite_record():
    value = create_location_record_value_collection_only_with_root()
    return create_single_location_metadata_composite_record_wrap_value_collection(value, -1)


def create_single_location_metadata_empty_composite_record():
    return create_single_location_metadata_root_composite_record()


def create_single_location_metadata_composite_record_from_single_line(single_line, indentation_depth_symbol):
    value = create_location_record_value_collection(single_line)
    depth = calculate_depth_level(single_line, indentation_depth_symbol)
    composite = create_single_location_metadata_composite_record_wrap_value_collection(value, depth)
    return composite


def create_single_location_metadata_composite_record_from_text_block(location_lines):

    indentation_depth_symbol = RegexConstants().list_indentation_depth_symbol()

    root = create_single_location_metadata_root_composite_record()
    previously_analyzed_location = root

    for single_line in location_lines:

        current_depth = calculate_depth_level(single_line, indentation_depth_symbol)
        if current_depth > previously_analyzed_location.indentation_level():

            location_metadata_composite = create_single_location_metadata_composite_record_from_single_line(single_line, indentation_depth_symbol)
            previously_analyzed_location.add_child(location_metadata_composite)
            previously_analyzed_location = location_metadata_composite

        elif current_depth == previously_analyzed_location.indentation_level():

            location_metadata_composite = create_single_location_metadata_composite_record_from_single_line(single_line, indentation_depth_symbol)
            previously_analyzed_location.add_sibling(location_metadata_composite)
            previously_analyzed_location = location_metadata_composite

        else:

            potential_parent_indentation = previously_analyzed_location.indentation_level()
            potential_parent = previously_analyzed_location

            while current_depth <= potential_parent_indentation:
                potential_parent = potential_parent.get_parent()
                potential_parent_indentation = potential_parent.indentation_level()

            location_metadata_composite = create_single_location_metadata_composite_record_from_single_line(single_line, indentation_depth_symbol)
            potential_parent.add_child(location_metadata_composite)
            previously_analyzed_location = location_metadata_composite

    return root

from RdReportsReader.Src.Domain.Location.Metadata.SingleCompositeRecord.SingleLocationMetadataCompositeRecord import SingleLocationMetadataCompositeRecord
