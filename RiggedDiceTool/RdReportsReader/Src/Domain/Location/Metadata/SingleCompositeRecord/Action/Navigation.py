from itertools import repeat


def parent_at_level_counted_from_root(composite_node, depth_level_of_parent):
    """
    Returns a parent at an appropriate depth level or None if the depth level of the record is lower
    than intended depth level (depth is lower  = depth is closer to root)
    :param composite_node: node in a composite tree structure
    :param depth_level_of_parent: counted from root, depth of a parent
    :return:  parent of a composite_metadata_record at depth_level from root
    """

    record_depth_level = composite_node.indentation_level()
    if record_depth_level < depth_level_of_parent:
        return None
    elif record_depth_level == depth_level_of_parent:
        return composite_node
    else:
        levels_to_go_up = record_depth_level - depth_level_of_parent
        parent = composite_node
        for _ in repeat(None, levels_to_go_up):
            parent = parent.get_parent()

        return parent
