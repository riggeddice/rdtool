

def path_from_root_in_text_form(composite_record):
    """
    Returns a _-delimited path from root to composite_record through all the nodes
    :param composite_record: node to find a path from root to
    :return: string representing how do you get from root to composite_record
    """

    names = [composite_record.normalized_name()]
    root = composite_record.root()

    parent = composite_record.get_parent()
    while parent is not root and parent is not None:
        names.append(parent.normalized_name())
        parent = parent.get_parent()

    names.reverse()

    path = ""
    for name in names:
        path = path + name + "_"

    without_trailing_underscore = path[:-1]
    return without_trailing_underscore


def composite_record_to_text(composite_record, depth_to_display=float('inf')):
    """
    Returns a complete and proper text representation (according to RD standards) of a composite record.
    :param composite_record: node to be changed into text form
    :param depth_to_display: how deep should the rendering go; at '3' it will render only the world-plane-region levels
    :return: complete text representation of a composite record in RD text form
    """

    header = ""
    body = __composite_to_text_recursively(composite_record.root(), depth_to_display)
    footer = "\n"
    return header + body + footer


def __composite_to_text_recursively(node, depth_to_display):
    """
    RECURSIVE FUNCTION! Transforms the node and all its children into proper to_text() form.
    :param node: node to be displayed in text form
    :param depth_to_display: how deep should the rendering go; at '3' it will render only the world-plane-region levels
    :return: displayable text form of a node (and all its children)
    """

    result = ""

    if node.indentation_level() > depth_to_display -1:
        return result

    for child in node.children():

        indentation_level = child.indentation_level()
        value = child.value()

        result = result + "\n" + value.to_rd_text_for_masterlist(indentation_level)

        if node.has_children():
            result = result + __composite_to_text_recursively(child, depth_to_display)

    return result
