

def flat_list_of_value_collections(single_composite_record):
    """
    For easier and more pleasant traversal / skimming, list [] containing all values of composite_record and all
    its children. No side effects in function.
    :param single_composite_record: composite node from which we make a list of values
    :return: list of values of a composite node and of all its children and their children and...
    """

    records_list = flat_list_of_single_composite_records(single_composite_record)
    values = [record.value() for record in records_list]
    return values


def flat_list_of_single_composite_records(single_composite_record):
    """
    For easier and more pleasant traversal / skimming, list [] containing composite_record and all its children.
    No side effects in function.
    :param single_composite_record: composite node from which we make a list of children records
    :return: list of nodes of a composite node and of all its children and their children and...
    """

    composite_record_children = single_composite_record.root().children()

    flat_list = []
    for child in composite_record_children:
        __add_records_to_list_recursively(child, flat_list)

    return flat_list


def __add_records_to_list_recursively(node, flat_list):
    """
    RECURSIVE! Adds all the direct children of the node to the list, then goes deeper to all the children.
    :param node: a node to be added to a list with all its children
    :param flat_list: a list referencing the node and all its children
    :return:
    """

    flat_list.append(node)

    for child in node.children():
        __add_records_to_list_recursively(child, flat_list)

    return flat_list
