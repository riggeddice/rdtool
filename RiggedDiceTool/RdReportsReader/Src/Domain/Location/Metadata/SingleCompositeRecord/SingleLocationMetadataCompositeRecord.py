import uuid

from RdReportsReader.Src.AppLogic.Structure.Composite.CompositeNode import CompositeNode
from .Action.ListRepresentation import *
from .Action.ToText import *
from .Action.Navigation import *


class SingleLocationMetadataCompositeRecord(CompositeNode):

    def __init__(self, value_collection, depth, unique_id):
        CompositeNode.__init__(self, unique_id)

        self.location_metadata_value_collection = value_collection
        self.indentation_depth_level = depth

    def value(self):
        return self.location_metadata_value_collection

    def to_rd_text(self, depth_to_display=float("inf")):
        return composite_record_to_text(self, depth_to_display)

    def normalized_name(self):
        return self.value().normalized_name()

    def list_of_all_single_composite_records(self):
        return flat_list_of_single_composite_records(self)

    def list_of_all_value_collections(self):
        return flat_list_of_value_collections(self)

    def indentation_level(self):
        return self.indentation_depth_level

    def path_from_root(self):
        return path_from_root_in_text_form(self)

    def parent_at_level_counted_from_root(self, level):
        return parent_at_level_counted_from_root(self, level)
