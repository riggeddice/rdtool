from RdReportsReader.Src.Domain.Location.Metadata.RecordValueCollection.Action.Normalize import normalize_record_value_name
from RdReportsReader.Src.Domain.Location.Metadata.RecordValueCollection.LocationRecordValueCollection import LocationRecordValueCollection
from RdReportsReader.Src.Domain.Location.Metadata.SingleRecordValue.Create import create_location_metadata_record_value
from RdReportsReader.Src.Domain.Location.Metadata.SingleRecordValue.Create import create_location_metadata_record_value_root


def create_location_record_value_collection_only_with_root():
    root = create_location_metadata_record_value_root()
    name = normalize_record_value_name(root.name())
    return LocationRecordValueCollection(name, [root])


def create_location_record_value_collection(single_line):
    single_value = create_location_metadata_record_value(single_line)
    name = normalize_record_value_name(single_value.name())
    return LocationRecordValueCollection(name, [single_value])
