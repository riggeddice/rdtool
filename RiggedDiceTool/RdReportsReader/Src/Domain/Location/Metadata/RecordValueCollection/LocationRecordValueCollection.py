from .Action.Normalize import normalize_record_value_name
from .Action.ToText import location_record_value_collection_to_rd_text_for_descriptor
from .Action.ToText import location_record_value_collection_to_rd_text_for_masterlist


class LocationRecordValueCollection:

    # Initialization and creation

    def __init__(self, normalized_name, single_record_values):
        self.name = normalized_name
        self.single_record_values = single_record_values

    def set_parent_record(self, composite_record):
        self.guid = composite_record.uid()
        self.path_from_root = composite_record.path_from_root()

    # Internal list manipulation

    def record_values(self):
        return self.single_record_values

    def append_record_value(self, single_record_value):
        self.single_record_values.append(single_record_value)

    def extend_record_value_collection(self, record_value_collection):
        self.single_record_values.extend(record_value_collection.record_values())

    # Identity methods

    def normalized_name(self):
        return normalize_record_value_name(self.name)

    def origin_guid(self):
        return self.guid

    def origin_path_from_root(self):
        return self.path_from_root

    # Primary functions

    def to_rd_text_for_descriptor(self):
        return location_record_value_collection_to_rd_text_for_descriptor(self.single_record_values)

    def to_rd_text_for_masterlist(self, indentation_level):
        return location_record_value_collection_to_rd_text_for_masterlist(self.single_record_values, indentation_level)
