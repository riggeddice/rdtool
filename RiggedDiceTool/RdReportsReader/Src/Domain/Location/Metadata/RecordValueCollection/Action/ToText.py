

def location_record_value_collection_to_rd_text_for_descriptor(single_records):

    deeds = [record.deed() for record in single_records]

    result = ""
    for deed in deeds:
        result = result + deed + "\n"

    result = result.strip()
    return result


def location_record_value_collection_to_rd_text_for_masterlist(single_records, indentation_level):
    return single_records[0].to_rd_text(indentation_level)
