

def do_location_record_value_collections_have_different_origins_same_root(collection_1, collection_2):

    guid_1 = collection_1.origin_guid()
    guid_2 = collection_2.origin_guid()
    path_1 = collection_1.origin_path_from_root()
    path_2 = collection_2.origin_path_from_root()

    if guid_1 != guid_2 and path_1 == path_2:
        return True
    else:
        return False
