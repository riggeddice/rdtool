import string


def normalize_record_value_name(not_normalized_name):
    return string.capwords(not_normalized_name.lower().strip())
