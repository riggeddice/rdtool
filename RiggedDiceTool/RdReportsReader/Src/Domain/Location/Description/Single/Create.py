import hashlib

from RdReportsReader.Src.Domain.Location.Description.Single.SingleLocationDescription import SingleLocationDescription
from RdReportsReader.Src.Config.Providers.ProvideInternalTokens import ProvideInternalTokens


def create_single_location_description(composite_record):

    path = composite_record.path_from_root()
    name = composite_record.normalized_name()

    prefix_identifier = __prefix_from_city_record(composite_record)
    unique_identifier = __calculate_stable_unique_identifier(prefix_identifier, path, name)
    value_record_collection = composite_record.value()

    return SingleLocationDescription(unique_identifier, value_record_collection)


def __prefix_from_city_record(composite_record):

    city_depth = ProvideInternalTokens().location_city_depth()

    city_record = composite_record.parent_at_level_counted_from_root(city_depth)
    if city_record is None:
        return "placeholder_city"
    else:
        city_record_value = city_record.value()
        city_name = city_record_value.normalized_name()
        return city_name


def __calculate_stable_unique_identifier(city, single_path, name):

    # Stable means: unchanging between application data expansion and between application runs.
    unique_enough_component = __calculate_unique_enough_hash(single_path)
    unique_readable_identifier = city + "_" + unique_enough_component + "_" + name
    return unique_readable_identifier


def __calculate_unique_enough_hash(single_path):
    byte_line = single_path.encode('utf-8')
    num = int(hashlib.md5(byte_line).hexdigest(), 16)
    # reasonably strong
    unique_enough_component = str(hex(num))[2:18]
    return unique_enough_component
