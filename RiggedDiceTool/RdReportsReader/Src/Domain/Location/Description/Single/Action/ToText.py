from RdReportsReader.Src.Config.Providers.ProvideInternalTokens import ProvideInternalTokens


def description_to_rd_text(record_value_collection):

    header = ProvideInternalTokens().top_level_header()
    location_name = record_value_collection.normalized_name()
    body = record_value_collection.to_rd_text_for_descriptor()

    complete_text = header + location_name + "\n" + body
    return complete_text
