from .Action.ToText import description_to_rd_text


class SingleLocationDescription:

    def __init__(self, unique_identifier, record_value_collection):
        self.record_value_collection = record_value_collection
        self.uniq_identifier = unique_identifier

    def unique_identifier(self):
        return self.uniq_identifier

    def to_rd_text(self):
        return description_to_rd_text(self.record_value_collection)
