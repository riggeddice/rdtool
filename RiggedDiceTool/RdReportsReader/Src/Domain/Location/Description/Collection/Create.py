from RdReportsReader.Src.Domain.Location.Description.Collection.LocationDescriptionCollection import LocationDescriptionCollection
from RdReportsReader.Src.Domain.Location.Description.Single.Create import create_single_location_description


def create_location_descriptions_from_composite_record(composite_record):

    all_value_collections = composite_record.list_of_all_single_composite_records()

    single_location_descriptions = []
    for value_collection in all_value_collections:
        single_location_description = create_single_location_description(value_collection)
        single_location_descriptions.append(single_location_description)

    return LocationDescriptionCollection(single_location_descriptions)


def create_location_descriptions_from_report_collection(report_collection):

    all_locations_on_missions = report_collection.all_locations_on_missions()
    combined_location_composite_record = all_locations_on_missions.to_single_composite_record()

    return create_location_descriptions_from_composite_record(combined_location_composite_record)
