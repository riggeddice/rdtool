

def persist_report_masterlist(masterlist, destination):
    masterlist_text = masterlist.to_rd_text()
    destination.persist_report_masterlist(masterlist_text)


def persist_report_collection(collection, data_destination):
    summary_list = collection.summaries()
    for summary in summary_list:
        __single_at_dest(summary, data_destination)


def __single_at_dest(summary, data_destination):
    data_destination.persist_campaign_summary(summary.campaign_name(), summary.to_rd_text())
