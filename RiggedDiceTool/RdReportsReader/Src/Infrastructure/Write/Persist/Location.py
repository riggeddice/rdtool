

def persist_location_masterlist(masterlist, destination):
    masterlist_text = masterlist.to_rd_text()
    destination.persist_location_masterlist(masterlist_text)


def persist_location_collection(collection, destination):
    all_descriptions = collection.all_descriptions()
    for description in all_descriptions:
        __single_at_dest(description, destination)


def __single_at_dest(description, destination):
    destination.persist_single_location_description(description.unique_identifier(), description.to_rd_text())
