

def persist_profile_collection(complete_profile_collection, destination):
    complete_profiles = complete_profile_collection.all()
    for profile in complete_profiles:
        __single_at_dest(profile, destination)


def __single_at_dest(profile, destination):
    destination.persist_single_profile(profile.name(), profile.to_rd_text())
