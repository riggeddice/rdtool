from RdReportsReader.Src.Config.Providers.ProvideGeneralFilepathConfig import ProvideGeneralFilepathConfig
from RdReportsReader.Src.Infrastructure.Write.Destination.File.Store.Masterlist import persist_masterlist
from RdReportsReader.Src.Infrastructure.Write.Destination.File.Store.SingleEntity import persist_single_entity


class FileAggregatedDestination:

    def persist_report_masterlist(self, masterlist_text):
        persist_masterlist(masterlist_text, ProvideGeneralFilepathConfig().output_report_masterlist_file_to)

    def persist_location_masterlist(self, masterlist_text):
        persist_masterlist(masterlist_text, ProvideGeneralFilepathConfig().output_location_masterlist_file_to)

    def persist_single_profile(self, character_name, profile_text):
        self.__persist_single_entity(character_name, profile_text, "Profiles",
                                     ProvideGeneralFilepathConfig().output_complete_profile_files_to_folder)

    def persist_campaign_summary(self, campaign_name, summary_text):
        self.__persist_single_entity(campaign_name, summary_text, "CampaignSummaries",
                                     ProvideGeneralFilepathConfig().output_campaign_summaries_to_folder)

    def persist_single_location_description(self, unique_identifier, description_text):
        self.__persist_single_entity(unique_identifier, description_text, "Locations",
                                     ProvideGeneralFilepathConfig().output_location_descriptions_to_folder)

    def __persist_single_entity(self, name, text, category_name, output_folder):
        persist = persist_single_entity(category_name, output_folder)
        persist(name, text)
