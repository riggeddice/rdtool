import os
from collections import defaultdict

from RdReportsReader.Src.AppLogic.FileOps import save_file
from RdReportsReader.Src.AppLogic.StripText import strip_symbols_unsafe_for_file_path
from RdReportsReader.Src.Config.Providers.ProvideGeneralFilepathConfig import ProvideGeneralFilepathConfig


def persist_single_entity(entity_domain_name, output_folder):

    save_path = __path_to_persist_entities_in(output_folder)
    __create_folder_if_needed(save_path, entity_domain_name)

    def do(entity_identifier, text_to_save):

        target_file = strip_symbols_unsafe_for_file_path(entity_identifier, save_path)

        try:
            save_file(text_to_save, target_file)
            __announce_saves(entity_domain_name)

        except Exception as e:
            print("Cannot persist Profile: " + target_file + ": " + str(e))

    return do


def __create_folder_if_needed(save_path, entity_domain_name):

    if __number_of_saves[entity_domain_name] == 0:
        if not os.path.exists(save_path):
            os.makedirs(save_path)


def __path_to_persist_entities_in(subfolder):

    if not subfolder:
        rd_save_path = ProvideGeneralFilepathConfig().rd_dir_path
    else:
        rd_save_path = ProvideGeneralFilepathConfig().rd_dir_path + "/" + subfolder
    return rd_save_path


def __announce_saves(entity_domain_name):

    __number_of_saves[entity_domain_name] += 1
    if __number_of_saves[entity_domain_name] % 50 == 0:
        print("Successfully saved: " + str(__number_of_saves[entity_domain_name]) + "+ " + entity_domain_name)


__number_of_saves = defaultdict(int)
