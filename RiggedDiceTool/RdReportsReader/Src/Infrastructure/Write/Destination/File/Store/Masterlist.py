from RdReportsReader.Src.AppLogic.FileOps import save_file
from RdReportsReader.Src.Config.Providers.ProvideGeneralFilepathConfig import ProvideGeneralFilepathConfig


def persist_masterlist(masterlist_text, masterlist_folder):

    rd_save_path = ProvideGeneralFilepathConfig().rd_dir_path
    masterlist_save_path = rd_save_path + "/" + masterlist_folder

    try:
        save_file(masterlist_text, masterlist_save_path)

        print("LocationMasterlist saved at: " + masterlist_save_path)

    except:
        print("Cannot persist LocationMasterlist: " + masterlist_save_path)
