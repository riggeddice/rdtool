from collections import namedtuple

from RdReportsReader.Src.Domain.CharacterProfile.Masterlist.Create import create_profile_masterlist_from_text
from RdReportsReader.Src.Domain.CharacterProfile.Read.Collection.Create import create_read_profile_collection


def provide_from_profile_domain(source):

    def masterlist():
        profile_masterlist_text = source.profile_masterlist_text()
        profile_masterlist = create_profile_masterlist_from_text(profile_masterlist_text)
        return profile_masterlist

    def collection():
        rd_profiles = source.rd_profiles()
        profiles_collection = create_read_profile_collection(rd_profiles)
        return profiles_collection

    provided = namedtuple("Provide", ["masterlist", "collection"])
    return provided(masterlist=masterlist, collection=collection)
