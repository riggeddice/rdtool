from collections import namedtuple

from RdReportsReader.Src.Domain.Report.Collection.Create import create_report_collection
from RdReportsReader.Src.Domain.Report.Masterlist.Complete.Create import create_report_masterlist_from_text


def provide_from_report_domain(source):

    def masterlist():
        report_masterlist_text = source.report_masterlist_text()
        report_masterlist = create_report_masterlist_from_text(report_masterlist_text)
        return report_masterlist

    def collection():
        rd_reports = source.rd_reports()
        mission_report_collection = create_report_collection(rd_reports, masterlist())
        return mission_report_collection

    provided = namedtuple("Provide", ["masterlist", "collection"])
    return provided(masterlist=masterlist, collection=collection)
