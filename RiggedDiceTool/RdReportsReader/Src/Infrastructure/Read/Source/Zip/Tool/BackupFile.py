import os
import re

from RdReportsReader.Src.Config.Constants.RegexConstants import RegexConstants
from RdReportsReader.Src.AppLogic.FileOps import unpack_zip_file
from RdReportsReader.Src.Config.Providers.ProvideZipSourceConfig import ProvideZipSourceConfig


def unpack_zip_backup_file_into_temp_file():

    rd_master_file_location = ProvideZipSourceConfig().folder_with_zip_backup_file() + "/"
    master_file_pattern = ProvideZipSourceConfig().zip_backup_file_pattern()
    newest_zip_master_file = __find_most_recent_backup_file(rd_master_file_location, master_file_pattern)

    newest_zip_master_file_path = ProvideZipSourceConfig().folder_with_zip_backup_file() + "/" + newest_zip_master_file
    destination = ProvideZipSourceConfig().temp_dir_to_create_patch()
    unpack_zip_file(newest_zip_master_file_path, destination)


def __find_most_recent_backup_file(zip_file_folder, master_file_pattern):

    all_zip_file_names = __all_zip_files_in(zip_file_folder)
    most_recent_backup_file = __select_most_recent_file(all_zip_file_names, master_file_pattern)
    return most_recent_backup_file


def __select_most_recent_file(all_zip_file_names, master_file_pattern):

    most_recent_file = ""
    most_recent_file_date = 0

    for single_filename in all_zip_file_names:
        depatterned = single_filename.replace(master_file_pattern, "")
        regex_split = re.search(RegexConstants().backup_file_pattern(), depatterned).groups()
        day_of_backup = regex_split[0]

        if day_of_backup:   # not none or empty
            day_from_this_file = int(day_of_backup)

            if day_from_this_file > most_recent_file_date:
                most_recent_file_date = day_from_this_file
                most_recent_file = single_filename

    return most_recent_file


def __all_zip_files_in(zip_file_folder):

    all_files = os.listdir(zip_file_folder)
    matching_files = [fi for fi in all_files if fi.endswith(".zip")]
    return matching_files
