from RdReportsReader.Src.Config.Providers.ProvideZipSourceConfig import ProvideZipSourceConfig
from RdReportsReader.Src.Infrastructure.Entities.Profile.Create import create_rd_profile
from RdReportsReader.Src.Infrastructure.Entities.Report.Create import create_rd_report
from RdReportsReader.Src.Infrastructure.Read.Source.Zip.Load.Masterlist import masterlist_text
from RdReportsReader.Src.Infrastructure.Read.Source.Zip.Load.Masterlist import profile_masterlist
from RdReportsReader.Src.Infrastructure.Read.Source.Zip.Load.Masterlist import report_masterlist
from RdReportsReader.Src.Infrastructure.Read.Source.Zip.Load.SingleEntity import rd_entities
from RdReportsReader.Src.Infrastructure.Read.Source.Zip.Tool.BackupFile import unpack_zip_backup_file_into_temp_file


class ZipAggregatedSource:

    def __init__(self):
        unpack_zip_backup_file_into_temp_file()

    def profile_masterlist_text(self):
        return masterlist_text(ProvideZipSourceConfig().profile_masterlist_file_to_read())

    def report_masterlist_text(self):
        return masterlist_text(ProvideZipSourceConfig().report_masterlist_file_to_read())

    def rd_profiles(self):
        return rd_entities(profile_masterlist(), create_rd_profile)

    def rd_reports(self):
        return rd_entities(report_masterlist(), create_rd_report)
