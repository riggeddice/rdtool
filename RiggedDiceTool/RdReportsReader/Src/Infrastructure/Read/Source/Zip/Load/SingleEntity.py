from RdReportsReader.Src.AppLogic.FileOps import read_file
from RdReportsReader.Src.Config.Providers.ProvideZipSourceConfig import ProvideZipSourceConfig


def rd_entities(masterlist, create_entity):
    metadata_collection = masterlist.all_metadata()

    collection = __with_metadata_collection(metadata_collection, create_entity)
    return collection


def __with_metadata_collection(metadata_collection, create_entity):

    created_collection = []
    for single_metadata in metadata_collection:
        text = __read_rd_entity_text_from_link(single_metadata.ziplink())
        entity = create_entity(single_metadata, text)
        created_collection.append(entity)

    return created_collection


def __read_rd_entity_text_from_link(ziplink):

    file_location = ProvideZipSourceConfig().unpacked_folder_with_all_text_files() + ziplink + ".txt"
    return read_file(file_location)
