from RdReportsReader.Src.Domain.CharacterProfile.Masterlist.Create import create_profile_masterlist_from_text
from RdReportsReader.Src.Domain.Report.Masterlist.Complete.Create import create_report_masterlist_from_text
from RdReportsReader.Src.AppLogic.FileOps import read_file
from RdReportsReader.Src.Config.Providers.ProvideZipSourceConfig import ProvideZipSourceConfig


def profile_masterlist():
    text = masterlist_text(ProvideZipSourceConfig().profile_masterlist_file_to_read())
    return create_profile_masterlist_from_text(text)


def report_masterlist():
    text = masterlist_text(ProvideZipSourceConfig().report_masterlist_file_to_read())
    return create_report_masterlist_from_text(text)


def masterlist_text(masterlist_file_to_read):
    file_location = ProvideZipSourceConfig().unpacked_folder_with_all_text_files() + masterlist_file_to_read
    return read_file(file_location)
