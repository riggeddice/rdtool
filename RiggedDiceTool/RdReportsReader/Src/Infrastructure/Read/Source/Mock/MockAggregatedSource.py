from RdReportsReader.Resources.TestData.MockSourceResources import load_described_profile_resource
from RdReportsReader.Resources.TestData.MockSourceResources import load_described_report_resource
from RdReportsReader.Src.Infrastructure.Read.Source.Mock.Load.SingleEntity import return_profiles_having_keys
from RdReportsReader.Src.Infrastructure.Read.Source.Mock.Load.SingleEntity import return_reports_having_keys
from RdReportsReader.Src.Infrastructure.Read.Source.Mock.Load.Masterlist import load_masterlist_described_in_collection


class MockAggregatedSource:

    def __init__(self):
        self._selected_profile_masterlist_text = None
        self._selected_profile_collection_text = None
        self._selected_report_masterlist_text = None
        self._selected_report_collection_text = None

    # Common source methods:

    def profile_masterlist_text(self):
        if not self._selected_profile_masterlist_text:
            raise KeyError("ProfileMasterList dataset not selected from resources.")
        return self._selected_profile_masterlist_text

    def report_masterlist_text(self):
        if not self._selected_report_masterlist_text:
            raise KeyError("ReportMasterList dataset not selected from resources.")
        return self._selected_report_masterlist_text

    def rd_profiles(self):
        if not self._selected_profile_collection_text or not self._selected_profile_masterlist_text:
            raise KeyError("ProfileCollection dataset not selected from resources or malformed.")
        return return_profiles_having_keys(self._selected_profile_masterlist_text, self._selected_profile_collection_text)

    def rd_reports(self):
        if not self._selected_report_collection_text or not self._selected_report_masterlist_text:
            raise KeyError("ReportCollection dataset not selected from resources or malformed.")
        return return_reports_having_keys(self._selected_report_masterlist_text, self._selected_report_collection_text)

    # Mock specific methods:

    def select_profile_masterlist(self, selected_masterlist):
        self._selected_profile_masterlist_text = selected_masterlist

    def select_profiles(self, selected_collection):
        self._selected_profile_masterlist_text = load_masterlist_described_in_collection(selected_collection, load_described_profile_resource)
        self._selected_profile_collection_text = selected_collection

    def select_report_masterlist(self, selected_masterlist):
        self._selected_report_masterlist_text = selected_masterlist

    def select_reports(self, selected_collection):
        self._selected_report_masterlist_text = load_masterlist_described_in_collection(selected_collection, load_described_report_resource)
        self._selected_report_collection_text = selected_collection
