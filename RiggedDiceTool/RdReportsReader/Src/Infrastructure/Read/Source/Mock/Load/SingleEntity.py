import re

from RdReportsReader.Src.Config.Constants.RegexConstants import RegexConstants
from RdReportsReader.Src.Domain.CharacterProfile.Metadata.Single.Create import create_single_profile_metadata_from_raw_record
from RdReportsReader.Src.Domain.Report.Metadata.Single.Create import create_single_report_metadata_from_raw_record
from RdReportsReader.Src.Infrastructure.Entities.Profile.Create import create_rd_profile
from RdReportsReader.Src.Infrastructure.Entities.Report.Create import create_rd_report
from RdReportsReader.Resources.TestData.MockSourceResources import load_described_profile_resource
from RdReportsReader.Resources.TestData.MockSourceResources import load_described_report_resource


def return_profiles_having_keys(masterlist_text, collection_text):

    rd_entities_link_texts = __retrieve_link_texts_from_collection(collection_text, load_described_profile_resource)
    rd_entities = __create_rd_entities(rd_entities_link_texts, masterlist_text,
                                       create_rd_profile, create_single_profile_metadata_from_raw_record,
                                       RegexConstants().profile_metadata_in_masterlist())

    return rd_entities


def return_reports_having_keys(masterlist_text, collection_text):

    rd_entities_link_texts = __retrieve_link_texts_from_collection(collection_text, load_described_report_resource)

    rd_entities = __create_rd_entities(rd_entities_link_texts, masterlist_text,
                                       create_rd_report, create_single_report_metadata_from_raw_record,
                                       RegexConstants().report_metadata_in_masterlist())

    return rd_entities


def __create_rd_entities(rd_entity_link_texts, masterlist_text, create_entity, create_single_metadata, metadata_in_masterlist_regex):

    rd_entities = []
    for link_text in rd_entity_link_texts:
        metadata = __construct_metadata_from_pseudo_link_text(link_text[0], masterlist_text, create_single_metadata, metadata_in_masterlist_regex)
        text = link_text[-1]

        rd_entity = create_entity(metadata, text)
        rd_entities.append(rd_entity)

    return rd_entities


def __construct_metadata_from_pseudo_link_text(pseudolink, masterlist_text, create_single_metadata, metadata_in_masterlist_regex):

    split_records = re.findall(metadata_in_masterlist_regex, masterlist_text)
    selected_record = [found for found in split_records if
                       (pseudolink.lower() in found[1].lower() or found[1].lower() in pseudolink.lower())][0]
    metadata_record = ''.join(selected_record)

    return create_single_metadata(metadata_record)


def __retrieve_link_texts_from_collection(selected_collection_text, load_resource):

    extracted = re.findall(RegexConstants().mock_aggregated_source_single_in_masterlist(), selected_collection_text)
    single_entity_descriptions = [pair[-1] for pair in extracted]

    link_texts = []
    for resource in single_entity_descriptions:
        text = load_resource("Single", resource)
        link_text = [resource, text]
        link_texts.append(link_text)

    return link_texts
