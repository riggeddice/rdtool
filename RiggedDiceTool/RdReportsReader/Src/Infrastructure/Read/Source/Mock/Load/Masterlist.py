import re

from RdReportsReader.Src.Config.Constants.RegexConstants import RegexConstants


def load_masterlist_described_in_collection(loaded_collection, load_described_resource):

    extracted = re.search(RegexConstants().mock_masterlist_record(), loaded_collection)
    described_masterlist = extracted.groups()[-1]

    read_masterlist = load_described_resource("Masterlist", described_masterlist)
    return read_masterlist
