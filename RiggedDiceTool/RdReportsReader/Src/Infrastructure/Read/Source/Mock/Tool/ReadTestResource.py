from RdReportsReader.Src.AppLogic.FileOps import read_file


def load_test_resource(resource_folder, resource_name):

    resource_file_path = resource_folder + "/" + resource_name
    resource = read_file(resource_file_path)
    return resource


def __build_file_path(resource_folder, resource_name):
    return resource_folder + "/" + resource_name
