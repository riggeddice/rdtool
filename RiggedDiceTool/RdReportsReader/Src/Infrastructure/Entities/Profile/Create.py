from .RdProfile import RdProfile


def create_rd_profile(metadata, text):
    return RdProfile(metadata=metadata, text=text)
