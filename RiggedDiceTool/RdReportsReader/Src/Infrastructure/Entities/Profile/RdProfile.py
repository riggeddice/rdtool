class RdProfile:

    def __init__(self, metadata, text):
        self._metadata = metadata
        self._text = text

    def name(self):
        return self._metadata.name()

    def text(self):
        return self._text
