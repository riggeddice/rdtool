from RdReportsReader.Src.Config.Providers.ProvideInternalTokens import ProvideInternalTokens


class RdReportHeaderKeys:

    location_header = ProvideInternalTokens().location_header()
    consequences_header = ProvideInternalTokens().consequences_header()
    dramatis_personae_header = ProvideInternalTokens().dramatis_personae_header()

    all_reserved_headers = [location_header, consequences_header, dramatis_personae_header]
