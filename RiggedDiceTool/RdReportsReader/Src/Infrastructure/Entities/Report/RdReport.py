from .Action.Extract import get_non_reserved_text_sections
from .Action.Extract import get_text_section_having_header
from .Constants.RdReportHeaderKeys import RdReportHeaderKeys


class RdReport:

    def __init__(self, metadata, text):
        self._metadata = metadata
        self._text = text

    # properties

    def name(self):
        return self._metadata.name()

    def link(self):
        return self._metadata.weblink()

    def text(self):
        return self._text

    def metadata(self):
        return self._metadata

    # subset methods

    def dramatis_personae_text_section(self):
        return get_text_section_having_header(RdReportHeaderKeys.dramatis_personae_header, self._text)

    def location_text_section(self):
        return get_text_section_having_header(RdReportHeaderKeys.location_header, self._text)

    def consequences_text_section(self):
        return get_text_section_having_header(RdReportHeaderKeys.consequences_header, self._text)

    def non_reserved_text_section_block(self):
        return get_non_reserved_text_sections(self._text)
