from .RdReport import RdReport


def create_rd_report(metadata, text):
    return RdReport(metadata=metadata, text=text)
