import re

from RdReportsReader.Src.Config.Constants.RegexConstants import RegexConstants
from RdReportsReader.Src.Infrastructure.Entities.Report.Constants.RdReportHeaderKeys import RdReportHeaderKeys


def get_text_section_having_header(matching_header, report_text):

    sections = re.split(RegexConstants().starts_with_h1(), report_text, flags=re.MULTILINE)

    for section in sections:
        if section:
            first_line = re.search(RegexConstants().first_line_is_word(), section).group(0)
            lowercased = first_line.lower()

            if matching_header.lower() in lowercased:
                headerless_section = section.replace(first_line, "").strip()
                return headerless_section

    return ""


def get_non_reserved_text_sections(report_text):

    # this one is actually fun: we want to return only those sections used nowhere else.

    report_sections = re.split(RegexConstants().starts_with_h1(), report_text, flags=re.MULTILINE)

    report_text = ""
    for section in report_sections:
        if section:
            first_line = re.search(RegexConstants().first_line_is_word(), section).group(0)
            lowercased = first_line.lower()

            ignore = False
            if lowercased:
                for reserved in RdReportHeaderKeys.all_reserved_headers:
                    if reserved.lower() in lowercased:
                        ignore = True

            if not ignore:
                report_text = report_text + "+ " + section

    return report_text
