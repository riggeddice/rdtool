from RdReportsReader.Src.Infrastructure.Read.Provide.Report import provide_from_report_domain
from RdReportsReader.Src.Infrastructure.Read.Source.Zip.ZipAggregatedSource import ZipAggregatedSource
from RdReportsReader.Src.Infrastructure.Write.Destination.File.FileAggregatedDestination import FileAggregatedDestination
from RdReportsReader.Src.Infrastructure.Write.Persist.Report import persist_report_masterlist


class UpdateReportMasterlistSequenceNumbers:

    def on_start(self):
        return "ReportMasterlist sequence number update started."

    def on_end(self):
        return "ReportMasterlist updated with correct sequence numbers."

    def do(self):
        report_masterlist = provide_from_report_domain(ZipAggregatedSource()).masterlist()
        persist_report_masterlist(report_masterlist, FileAggregatedDestination())
