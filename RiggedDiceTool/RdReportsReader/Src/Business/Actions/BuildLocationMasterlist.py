from RdReportsReader.Src.Domain.Location.Masterlist.Create import create_location_masterlist_from_report_collection
from RdReportsReader.Src.Infrastructure.Read.Provide.Report import provide_from_report_domain
from RdReportsReader.Src.Infrastructure.Read.Source.Zip.ZipAggregatedSource import ZipAggregatedSource
from RdReportsReader.Src.Infrastructure.Write.Destination.File.FileAggregatedDestination import FileAggregatedDestination
from RdReportsReader.Src.Infrastructure.Write.Persist.Location import persist_location_masterlist


class BuildLocationMasterlist:

    def on_start(self):
        return "Generation of LocationMasterlist started."

    def on_end(self):
        return "Generation of LocationMasterlist finished."

    def do(self):
        report_collection = provide_from_report_domain(ZipAggregatedSource()).collection()
        generated_location_masterlist = create_location_masterlist_from_report_collection(report_collection)
        persist_location_masterlist(generated_location_masterlist, FileAggregatedDestination())
