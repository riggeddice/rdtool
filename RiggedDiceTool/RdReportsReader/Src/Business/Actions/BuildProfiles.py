from RdReportsReader.Src.Domain.CharacterProfile.Complete.Collection.Create import create_complete_profile_collection
from RdReportsReader.Src.Infrastructure.Read.Provide.Profile import provide_from_profile_domain
from RdReportsReader.Src.Infrastructure.Read.Provide.Report import provide_from_report_domain
from RdReportsReader.Src.Infrastructure.Read.Source.Zip.ZipAggregatedSource import ZipAggregatedSource
from RdReportsReader.Src.Infrastructure.Write.Destination.File.FileAggregatedDestination import FileAggregatedDestination
from RdReportsReader.Src.Infrastructure.Write.Persist.Profile import persist_profile_collection


class BuildProfiles:

    def on_start(self):
        return "Integration of RdReports and RdCharacters started."

    def on_end(self):
        return "Integration of RdReports and RdCharacters completed."

    def do(self):

        report_provider = provide_from_report_domain(ZipAggregatedSource())
        reports = report_provider.collection()

        profile_provider = provide_from_profile_domain(ZipAggregatedSource())
        read_profiles_component = profile_provider.collection()
        profile_masterlist = profile_provider.masterlist()

        generated_profiles_component = reports.generate_profiles()

        combined_profile_collection = create_complete_profile_collection(read_profiles_component, generated_profiles_component, profile_masterlist)
        persist_profile_collection(combined_profile_collection, FileAggregatedDestination())
