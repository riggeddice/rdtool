from RdReportsReader.Src.Domain.Report.Masterlist.Campaign.Summary.Collection.Create import create_campaign_summary_collection
from RdReportsReader.Src.Infrastructure.Read.Provide.Report import provide_from_report_domain
from RdReportsReader.Src.Infrastructure.Read.Source.Zip.ZipAggregatedSource import ZipAggregatedSource
from RdReportsReader.Src.Infrastructure.Write.Destination.File.FileAggregatedDestination import FileAggregatedDestination
from RdReportsReader.Src.Infrastructure.Write.Persist.Report import persist_report_collection


class BuildCampaignSummaries:

    def on_start(self):
        return "Generation of CampaignSummaries started."

    def on_end(self):
        return "Generation of CampaignSummaries completed."

    def do(self):
        report_provider = provide_from_report_domain(ZipAggregatedSource())
        report_collection = report_provider.collection()
        report_masterlist = report_provider.masterlist()

        campaign_summaries = create_campaign_summary_collection(report_collection, report_masterlist)

        persist_report_collection(campaign_summaries, FileAggregatedDestination())
