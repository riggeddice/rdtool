from RdReportsReader.Src.Business.Actions.BuildCampaignSummaries import BuildCampaignSummaries
from .Actions.BuildProfiles import BuildProfiles
from .Actions.UpdateReportMasterlistSequenceNumbers import UpdateReportMasterlistSequenceNumbers
from .Actions.BuildLocationMasterlist import BuildLocationMasterlist


def selected_business_actions(config):

    created = []

    if not config.is_selected:
        raise KeyError("Lack of main command for the application to use.")

    if config.form_character_profiles_from_rd:
        created.append(BuildProfiles())

    if config.correct_report_masterlist_seq_no:
        created.append(UpdateReportMasterlistSequenceNumbers())

    if config.generate_location_masterlist:
        created.append(BuildLocationMasterlist())

    if config.generate_campaign_summaries:
        created.append(BuildCampaignSummaries())

    return created


class BusinessActionConfig:

    def __init__(self, form_character_profiles_from_rd=True, correct_report_masterlist_seq_no=True,
                 generate_location_masterlist=True, generate_campaign_summaries=True):
        """
        :param form_character_profiles_from_rd: should CombineIntoCharacterProfiles be run
        :type form_character_profiles_from_rd: bool
        :param correct_report_masterlist_seq_no: should CorrectReportMasterlistSequenceNumbers be run
        :type correct_report_masterlist_seq_no: bool
        :param generate_location_masterlist: should GenerateLocationMasterlist be run
        :type generate_location_masterlist: bool
        :param generate_campaign_summaries: should GenerateCampaignSummaries be run
        :type generate_campaign_summaries: bool
        :return: configuration object for HighLvCommandFactory
        :rtype: BusinessActionConfig
        """
        self.form_character_profiles_from_rd = form_character_profiles_from_rd
        self.correct_report_masterlist_seq_no = correct_report_masterlist_seq_no
        self.generate_location_masterlist = generate_location_masterlist
        self.generate_campaign_summaries = generate_campaign_summaries

    @property
    def is_selected(self):
        return self.form_character_profiles_from_rd or self.correct_report_masterlist_seq_no or \
               self.generate_location_masterlist or self.generate_campaign_summaries
