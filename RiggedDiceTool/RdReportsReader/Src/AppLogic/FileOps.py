import os
import zipfile


def absolute_file_location(reference_file, target_file):
    file_location = os.path.dirname(os.path.abspath(reference_file))
    combined_file_location = os.path.normpath(os.path.join(file_location, target_file))
    return combined_file_location


def read_file(file_location):

    file_to_read = open(file_location, mode='r', encoding='utf-8')
    read_text = file_to_read.read()
    file_to_read.close()

    return read_text


def save_file(what, where):
    to_write = open(where, 'w', encoding='utf-8')
    to_write.write(what)
    to_write.close()


def unpack_zip_file(source_zip_file, destination):

    zip = zipfile.ZipFile(source_zip_file)
    zip.extractall(destination)
    zip.close()
