

def strip_symbols_unsafe_for_file_path(name, rd_save_path):

    spaceless_name = name.replace(" ", "")
    char_file_name = strip_special_symbols(spaceless_name) + ".txt"
    target_file = rd_save_path + "/" + char_file_name
    return target_file


def strip_special_symbols(text):

    standard_strip = __strip_special_symbols_without_polish_letters(text)
    strip_vowels = standard_strip.replace("Ą", "A").replace("ą", "a").replace("Ę", 'E').replace("ę", "e").replace("Ó", "O").replace("ó", "o")
    strip_zet = strip_vowels.replace("Ż", "Z").replace("ż", "z").replace("Ź", "Z").replace("ź", "z")
    strip_ln = strip_zet.replace("Ł", "L").replace("ł", "l").replace("Ń", "N").replace("ń", "n")
    strip_sc = strip_ln.replace("Ś", "S").replace("ś", "s").replace("Ć", "C").replace("ć", "c")

    return strip_sc


def __strip_special_symbols_without_polish_letters(text):

    strip_quotes = text.replace("\"", "").replace("'", "")
    strip_braces = strip_quotes.replace("(", "").replace(")", "").replace("<", "").replace(">", "").replace("{", "").replace("}", "")
    strip_math = strip_braces.replace("+", "").replace("*", "").replace("/", "").replace("\\", "").replace("=", "")
    strip_symbols = strip_math.replace(":", "").replace(";", "")

    return strip_symbols


