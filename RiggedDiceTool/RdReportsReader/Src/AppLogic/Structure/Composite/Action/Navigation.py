import itertools


def remove_child_from_composite(parent, child_to_remove):
    """
    Removes all instances of a child from the composite based on a specific criterion: uid() method
    :param parent: parent node to remove a child from
    :param child_to_remove: child to be removed from a parent
    """

    new_children_list = [child for child in parent.children() if child.uid() != child_to_remove.uid()]
    parent._children = new_children_list


def add_child_to_composite(parent_to_be, child_to_be):
    """
    Adds a child to a composite in a proper way maintaining parent -> child and child -> parent link
    :param parent_to_be: a node to be a parent to a child-to-be
    :param child_to_be: a node added to a parent-to-be
    """

    parent_to_be.children().append(child_to_be)
    child_to_be.set_parent(parent_to_be)


def get_root_of_composite(composite_node):
    """
    Returns a highest possible level parent, one without a parent
    :param composite_node: node to get a root of
    :return: root of a record; a record without a parent
    """

    current = composite_node
    parent = composite_node.get_parent()

    while parent is not None:
        current = parent
        parent = parent.get_parent()

    return current
