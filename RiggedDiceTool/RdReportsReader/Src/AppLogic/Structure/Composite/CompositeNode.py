from .Action.Navigation import *


class CompositeNode:

    def __init__(self, uuid):
        self._children = []
        self._parent = None
        self._unique_id = uuid

    def uid(self):
        return self._unique_id

    def root(self):
        return get_root_of_composite(self)

    def add_child(self, child_to_be):
        add_child_to_composite(self, child_to_be)

    def remove_child(self, child_to_remove):
        remove_child_from_composite(self, child_to_remove)

    def children(self):
        return self._children

    def has_children(self):
        return len(self._children) != 0

    def set_parent(self, composite_node):
        self._parent = composite_node

    def get_parent(self):
        return self._parent

    def add_sibling(self, composite_node):
        self._parent.add_child(composite_node)
