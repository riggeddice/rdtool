import os

from RdReportsReader.Src.AppLogic.FileOps import absolute_file_location
from RdReportsReader.Src.AppLogic.FileOps import read_file


def load_described_profile_resource(category, *descriptions):
    """
    :param category: Single, Masterlist, Collection? Which superfolder is it located in?
    :param descriptions: words, phrases or filepath describing the file to be loaded
    :return: text of a loaded file
    """
    return __load_described_resource("Profile", category, *descriptions)


def load_described_report_resource(category, *descriptions):
    """
    :param category: Single, Masterlist, Collection? Which superfolder is it located in?
    :param descriptions: words, phrases or filepath describing the file to be loaded
    :return: text of a loaded file
    """
    return __load_described_resource("Report", category, *descriptions)


def __load_described_resource(domain_family, category, *descriptions):
    """
    :param domain_family: Profile, Report...? Which domain family is it located in?
    :param category: Single, Masterlist, Collection? Which superfolder is it located in?
    :param descriptions: words, phrases or filepath describing the file to be loaded
    :return: text of a loaded file
    """

    path = absolute_file_location(__file__, domain_family + "/" + category.title())
    if not os.path.isdir(path):
        raise ValueError("There exists no directory: " + path)

    everything_in_path = os.listdir(path)

    matches = []
    for candidate in everything_in_path:
        for description in descriptions:
            if description.lower() not in candidate.lower():
                break
            matches.append(candidate)

    described_resources = list(set(matches))

    if len(described_resources) == 0:
        description_text = ''.join(descriptions)
        raise ValueError("There exists no file with those descriptions in path: "
                         + path + " with descriptions: " + description_text)

    if len(described_resources) > 1:
        description_text = ''.join(descriptions)
        raise ValueError("More than one file can be described with those descriptions in path: "
                         + path + " with descriptions: " + description_text)

    file_to_load = path + "/" + described_resources[0]
    loaded_resource = read_file(file_to_load)

    return loaded_resource
