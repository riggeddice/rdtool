+ Augustyn Szczypiorek



+++ Na misjach:

+++++ Postać wystąpiła na: 4 misjach.
+++++ Postać wystąpiła w: 1 kampaniach: Czarodziejka Luster

||~ Data ||~ Numer ||~ Misja ||~ W kampanii ||
|| 130503 || 009 || [[[konspekty-zacmienie:130503-renowacja-obrazu-andromedy|Renowacja obrazu Andromedy (An)]]]||Czarodziejka Luster||
|| 130506 || 010 || [[[konspekty-zacmienie:130506-sekrety-rezydencji-szczypiorkow|Sekrety rezydencji Szczypiorków (An)]]]||Czarodziejka Luster||
|| 130511 || 011 || [[[konspekty-zacmienie:130511-oltarz-podniesionej-dloni|Ołtarz Podniesionej Dłoni (An)]]]||Czarodziejka Luster||
|| 131008 || 012 || [[[konspekty-zacmienie:131008-moj-aniol|<<Mój Anioł>> (An)]]]||Czarodziejka Luster||

+++ Dokonania:

|| [[[konspekty-zacmienie:130503-renowacja-obrazu-andromedy|Renowacja obrazu Andromedy (An)]]] || (83 l) nestor rodu, któremu zrobiono portret. Wygląda, jakby młodniał z czasem. Trzęsie rodem. ||
|| [[[konspekty-zacmienie:130506-sekrety-rezydencji-szczypiorkow|Sekrety rezydencji Szczypiorków (An)]]] || (83 l) najsilniejszy w rodzie Szczypiorków i twardą ręką dążący do zaprowadzenia prawa i porządku niezależnie od kosztu. ||
|| [[[konspekty-zacmienie:130511-oltarz-podniesionej-dloni|Ołtarz Podniesionej Dłoni (An)]]] || (83 l) "Anioł" który poświęci wszystko dla religii i kontroli magów dla swych ideałów. ||
|| [[[konspekty-zacmienie:131008-moj-aniol|<<Mój Anioł>> (An)]]] || (83 l) : Anioł Podniesionej Dłoni. KIA. ||

+++ Relacje z postaciami:

||~ Z kim ||~ Intensywność ||~ Na misjach ||
||Andromeda (Kasia Nowak)||4||[[[konspekty-zacmienie:130503-renowacja-obrazu-andromedy|130503]]], [[[konspekty-zacmienie:130506-sekrety-rezydencji-szczypiorkow|130506]]], [[[konspekty-zacmienie:130511-oltarz-podniesionej-dloni|130511]]], [[[konspekty-zacmienie:131008-moj-aniol|131008]]]||
||August Bankierz||4||[[[konspekty-zacmienie:130503-renowacja-obrazu-andromedy|130503]]], [[[konspekty-zacmienie:130506-sekrety-rezydencji-szczypiorkow|130506]]], [[[konspekty-zacmienie:130511-oltarz-podniesionej-dloni|130511]]], [[[konspekty-zacmienie:131008-moj-aniol|131008]]]||
||Inga Wójt||4||[[[konspekty-zacmienie:130503-renowacja-obrazu-andromedy|130503]]], [[[konspekty-zacmienie:130506-sekrety-rezydencji-szczypiorkow|130506]]], [[[konspekty-zacmienie:130511-oltarz-podniesionej-dloni|130511]]], [[[konspekty-zacmienie:131008-moj-aniol|131008]]]||
||Radosław Szczypiorek||4||[[[konspekty-zacmienie:130503-renowacja-obrazu-andromedy|130503]]], [[[konspekty-zacmienie:130506-sekrety-rezydencji-szczypiorkow|130506]]], [[[konspekty-zacmienie:130511-oltarz-podniesionej-dloni|130511]]], [[[konspekty-zacmienie:131008-moj-aniol|131008]]]||
||Samira Diakon||4||[[[konspekty-zacmienie:130503-renowacja-obrazu-andromedy|130503]]], [[[konspekty-zacmienie:130506-sekrety-rezydencji-szczypiorkow|130506]]], [[[konspekty-zacmienie:130511-oltarz-podniesionej-dloni|130511]]], [[[konspekty-zacmienie:131008-moj-aniol|131008]]]||
||Żanna Szczypiorek||4||[[[konspekty-zacmienie:130503-renowacja-obrazu-andromedy|130503]]], [[[konspekty-zacmienie:130506-sekrety-rezydencji-szczypiorkow|130506]]], [[[konspekty-zacmienie:130511-oltarz-podniesionej-dloni|130511]]], [[[konspekty-zacmienie:131008-moj-aniol|131008]]]||
||Sandra Stryjek||3||[[[konspekty-zacmienie:130506-sekrety-rezydencji-szczypiorkow|130506]]], [[[konspekty-zacmienie:130511-oltarz-podniesionej-dloni|130511]]], [[[konspekty-zacmienie:131008-moj-aniol|131008]]]||
||Jolanta Wójt||2||[[[konspekty-zacmienie:130506-sekrety-rezydencji-szczypiorkow|130506]]], [[[konspekty-zacmienie:131008-moj-aniol|131008]]]||
||Antoni Wójt||1||[[[konspekty-zacmienie:131008-moj-aniol|131008]]]||
||Artur Szmelc||1||[[[konspekty-zacmienie:130503-renowacja-obrazu-andromedy|130503]]]||
||Dariusz Germont||1||[[[konspekty-zacmienie:131008-moj-aniol|131008]]]||
||Feliks Bozur||1||[[[konspekty-zacmienie:130511-oltarz-podniesionej-dloni|130511]]]||
||Herbert Zioło||1||[[[konspekty-zacmienie:130511-oltarz-podniesionej-dloni|130511]]]||
||Izabela Kruczek||1||[[[konspekty-zacmienie:130506-sekrety-rezydencji-szczypiorkow|130506]]]||
||Zofia Szczypiorek||1||[[[konspekty-zacmienie:131008-moj-aniol|131008]]]||