+ Samira Diakon



+++ Na misjach:

+++++ Postać wystąpiła na: 13 misjach.
+++++ Postać wystąpiła w: 1 kampaniach: Czarodziejka Luster

||~ Data ||~ Numer ||~ Misja ||~ W kampanii ||
|| 120918 || 005 || [[[konspekty-zacmienie:120918-mozna-doprowadzic-maga-do|Można doprowadzić maga do problemu... (An)]]]||Czarodziejka Luster||
|| 120920 || 006 || [[[konspekty-zacmienie:120920-ale-nie-mozna-zmusic-go-do|Ale nie można zmusić go do jego rozwiązania... (An)]]]||Czarodziejka Luster||
|| 121013 || 007 || [[[konspekty-zacmienie:121013-zwlaszcza-gdy-cos-jest-ciagle|Zwłaszcza, gdy coś jest ciągle nie tak. (An)]]]||Czarodziejka Luster||
|| 121104 || 008 || [[[konspekty-zacmienie:121104-banshee-corka-mandragory|Banshee, córka mandragory. (An)]]]||Czarodziejka Luster||
|| 130503 || 009 || [[[konspekty-zacmienie:130503-renowacja-obrazu-andromedy|Renowacja obrazu Andromedy (An)]]]||Czarodziejka Luster||
|| 130506 || 010 || [[[konspekty-zacmienie:130506-sekrety-rezydencji-szczypiorkow|Sekrety rezydencji Szczypiorków (An)]]]||Czarodziejka Luster||
|| 130511 || 011 || [[[konspekty-zacmienie:130511-oltarz-podniesionej-dloni|Ołtarz Podniesionej Dłoni (An)]]]||Czarodziejka Luster||
|| 131008 || 012 || [[[konspekty-zacmienie:131008-moj-aniol|<<Mój Anioł>> (An)]]]||Czarodziejka Luster||
|| 141218 || 013 || [[[konspekty-zacmienie:141218-portret-boga|Portret Boga (An)]]]||Czarodziejka Luster||
|| 141220 || 014 || [[[konspekty-zacmienie:141220-bogini-marzen-w-zonkiborze|Bogini Marzeń w Żonkiborze (An)]]]||Czarodziejka Luster||
|| 141227 || 015 || [[[konspekty-zacmienie:141227-przyczajona-andromeda-ukryty|Przyczajona Andromeda, ukryty Maus (An)]]]||Czarodziejka Luster||
|| 141230 || 016 || [[[konspekty-zacmienie:141230-ofiara-z-wampira-dla-arazille|Ofiara z wampira dla Arazille (An)]]]||Czarodziejka Luster||
|| 150105 || 019 || [[[konspekty-zacmienie:150105-dar-iliusitiusa-dla-andromedy|Dar Iliusitiusa dla Andromedy (An)]]]||Czarodziejka Luster||

+++ Dokonania:

|| [[[konspekty-zacmienie:120918-mozna-doprowadzic-maga-do|Można doprowadzić maga do problemu... (An)]]] || wybitna iluzjonistka i NIE czarodziejka ||
|| [[[konspekty-zacmienie:120920-ale-nie-mozna-zmusic-go-do|Ale nie można zmusić go do jego rozwiązania... (An)]]] || załamana dziewczyna, której próbuje pomóc terminus ||
|| [[[konspekty-zacmienie:121013-zwlaszcza-gdy-cos-jest-ciagle|Zwłaszcza, gdy coś jest ciągle nie tak. (An)]]] || wybitna iluzjonistka i NIE czarodziejka, która nareszcie jest znów całością ||
|| [[[konspekty-zacmienie:121104-banshee-corka-mandragory|Banshee, córka mandragory. (An)]]] || wybitna iluzjonistka i NIE czarodziejka (choć eks-czarodziejka luster) ||
|| [[[konspekty-zacmienie:130503-renowacja-obrazu-andromedy|Renowacja obrazu Andromedy (An)]]] || żywy dowód na to, że NPCów nie powinno się porywać ||
|| [[[konspekty-zacmienie:130506-sekrety-rezydencji-szczypiorkow|Sekrety rezydencji Szczypiorków (An)]]] || osoba w coraz gorszym stanie fizycznym. Najpewniej przez Kasię. Nadal jednak ma złośliwy humor. ||
|| [[[konspekty-zacmienie:130511-oltarz-podniesionej-dloni|Ołtarz Podniesionej Dłoni (An)]]] || w sumie żywa bateria dla "zaklęć" Kasi przy użyciu mocy luster (których Kasia i tak nie umie kontrolować). ||
|| [[[konspekty-zacmienie:131008-moj-aniol|<<Mój Anioł>> (An)]]] || słabiutka już iluzjonistka, bateria mocy Kasi. ||
|| [[[konspekty-zacmienie:141218-portret-boga|Portret Boga (An)]]] || do której lgną wszystkie możliwe lustra a zwłaszcza te co nie powinny. ||
|| [[[konspekty-zacmienie:141220-bogini-marzen-w-zonkiborze|Bogini Marzeń w Żonkiborze (An)]]] || społeczna iluzjonistka której niewiedza o magii coraz bardziej przeszkadza. ||
|| [[[konspekty-zacmienie:141227-przyczajona-andromeda-ukryty|Przyczajona Andromeda, ukryty Maus (An)]]] || społeczna iluzjonistka której niewiedza o magii coraz bardziej przeszkadza. ||
|| [[[konspekty-zacmienie:141230-ofiara-z-wampira-dla-arazille|Ofiara z wampira dla Arazille (An)]]] || która rozbraja mniej stabilnych psychicznie sojuszników i wszyscy są za to jej wdzięczni. ||
|| [[[konspekty-zacmienie:150105-dar-iliusitiusa-dla-andromedy|Dar Iliusitiusa dla Andromedy (An)]]] || wreszcie uświadomiona o magii, która dzielnie przebijała się przez koszmary Iliusitiusa. ||

+++ Relacje z postaciami:

||~ Z kim ||~ Intensywność ||~ Na misjach ||
||Andromeda (Kasia Nowak)||13||[[[konspekty-zacmienie:120918-mozna-doprowadzic-maga-do|120918]]], [[[konspekty-zacmienie:120920-ale-nie-mozna-zmusic-go-do|120920]]], [[[konspekty-zacmienie:121013-zwlaszcza-gdy-cos-jest-ciagle|121013]]], [[[konspekty-zacmienie:121104-banshee-corka-mandragory|121104]]], [[[konspekty-zacmienie:130503-renowacja-obrazu-andromedy|130503]]], [[[konspekty-zacmienie:130506-sekrety-rezydencji-szczypiorkow|130506]]], [[[konspekty-zacmienie:130511-oltarz-podniesionej-dloni|130511]]], [[[konspekty-zacmienie:131008-moj-aniol|131008]]], [[[konspekty-zacmienie:141218-portret-boga|141218]]], [[[konspekty-zacmienie:141220-bogini-marzen-w-zonkiborze|141220]]], [[[konspekty-zacmienie:141227-przyczajona-andromeda-ukryty|141227]]], [[[konspekty-zacmienie:141230-ofiara-z-wampira-dla-arazille|141230]]], [[[konspekty-zacmienie:150105-dar-iliusitiusa-dla-andromedy|150105]]]||
||August Bankierz||10||[[[konspekty-zacmienie:120918-mozna-doprowadzic-maga-do|120918]]], [[[konspekty-zacmienie:120920-ale-nie-mozna-zmusic-go-do|120920]]], [[[konspekty-zacmienie:121013-zwlaszcza-gdy-cos-jest-ciagle|121013]]], [[[konspekty-zacmienie:121104-banshee-corka-mandragory|121104]]], [[[konspekty-zacmienie:130503-renowacja-obrazu-andromedy|130503]]], [[[konspekty-zacmienie:130506-sekrety-rezydencji-szczypiorkow|130506]]], [[[konspekty-zacmienie:130511-oltarz-podniesionej-dloni|130511]]], [[[konspekty-zacmienie:131008-moj-aniol|131008]]], [[[konspekty-zacmienie:141218-portret-boga|141218]]], [[[konspekty-zacmienie:150105-dar-iliusitiusa-dla-andromedy|150105]]]||
||Sandra Stryjek||8||[[[konspekty-zacmienie:130506-sekrety-rezydencji-szczypiorkow|130506]]], [[[konspekty-zacmienie:130511-oltarz-podniesionej-dloni|130511]]], [[[konspekty-zacmienie:131008-moj-aniol|131008]]], [[[konspekty-zacmienie:141218-portret-boga|141218]]], [[[konspekty-zacmienie:141220-bogini-marzen-w-zonkiborze|141220]]], [[[konspekty-zacmienie:141227-przyczajona-andromeda-ukryty|141227]]], [[[konspekty-zacmienie:141230-ofiara-z-wampira-dla-arazille|141230]]], [[[konspekty-zacmienie:150105-dar-iliusitiusa-dla-andromedy|150105]]]||
||Herbert Zioło||5||[[[konspekty-zacmienie:130511-oltarz-podniesionej-dloni|130511]]], [[[konspekty-zacmienie:141220-bogini-marzen-w-zonkiborze|141220]]], [[[konspekty-zacmienie:141227-przyczajona-andromeda-ukryty|141227]]], [[[konspekty-zacmienie:141230-ofiara-z-wampira-dla-arazille|141230]]], [[[konspekty-zacmienie:150105-dar-iliusitiusa-dla-andromedy|150105]]]||
||Augustyn Szczypiorek||4||[[[konspekty-zacmienie:130503-renowacja-obrazu-andromedy|130503]]], [[[konspekty-zacmienie:130506-sekrety-rezydencji-szczypiorkow|130506]]], [[[konspekty-zacmienie:130511-oltarz-podniesionej-dloni|130511]]], [[[konspekty-zacmienie:131008-moj-aniol|131008]]]||
||Inga Wójt||4||[[[konspekty-zacmienie:130503-renowacja-obrazu-andromedy|130503]]], [[[konspekty-zacmienie:130506-sekrety-rezydencji-szczypiorkow|130506]]], [[[konspekty-zacmienie:130511-oltarz-podniesionej-dloni|130511]]], [[[konspekty-zacmienie:131008-moj-aniol|131008]]]||
||Radosław Szczypiorek||4||[[[konspekty-zacmienie:130503-renowacja-obrazu-andromedy|130503]]], [[[konspekty-zacmienie:130506-sekrety-rezydencji-szczypiorkow|130506]]], [[[konspekty-zacmienie:130511-oltarz-podniesionej-dloni|130511]]], [[[konspekty-zacmienie:131008-moj-aniol|131008]]]||
||Żanna Szczypiorek||4||[[[konspekty-zacmienie:130503-renowacja-obrazu-andromedy|130503]]], [[[konspekty-zacmienie:130506-sekrety-rezydencji-szczypiorkow|130506]]], [[[konspekty-zacmienie:130511-oltarz-podniesionej-dloni|130511]]], [[[konspekty-zacmienie:131008-moj-aniol|131008]]]||
||Andrzej Szop||3||[[[konspekty-zacmienie:141227-przyczajona-andromeda-ukryty|141227]]], [[[konspekty-zacmienie:141230-ofiara-z-wampira-dla-arazille|141230]]], [[[konspekty-zacmienie:150105-dar-iliusitiusa-dla-andromedy|150105]]]||
||Anna Kajak||3||[[[konspekty-zacmienie:141220-bogini-marzen-w-zonkiborze|141220]]], [[[konspekty-zacmienie:141227-przyczajona-andromeda-ukryty|141227]]], [[[konspekty-zacmienie:141230-ofiara-z-wampira-dla-arazille|141230]]]||
||Artur Szmelc||3||[[[konspekty-zacmienie:130503-renowacja-obrazu-andromedy|130503]]], [[[konspekty-zacmienie:141218-portret-boga|141218]]], [[[konspekty-zacmienie:141227-przyczajona-andromeda-ukryty|141227]]]||
||Feliks Bozur||3||[[[konspekty-zacmienie:130511-oltarz-podniesionej-dloni|130511]]], [[[konspekty-zacmienie:141230-ofiara-z-wampira-dla-arazille|141230]]], [[[konspekty-zacmienie:150105-dar-iliusitiusa-dla-andromedy|150105]]]||
||Iliusitius||3||[[[konspekty-zacmienie:141227-przyczajona-andromeda-ukryty|141227]]], [[[konspekty-zacmienie:141230-ofiara-z-wampira-dla-arazille|141230]]], [[[konspekty-zacmienie:150105-dar-iliusitiusa-dla-andromedy|150105]]]||
||Małgorzata Poran||3||[[[konspekty-zacmienie:141218-portret-boga|141218]]], [[[konspekty-zacmienie:141220-bogini-marzen-w-zonkiborze|141220]]], [[[konspekty-zacmienie:141227-przyczajona-andromeda-ukryty|141227]]]||
||Wojciech Kajak||3||[[[konspekty-zacmienie:141220-bogini-marzen-w-zonkiborze|141220]]], [[[konspekty-zacmienie:141227-przyczajona-andromeda-ukryty|141227]]], [[[konspekty-zacmienie:141230-ofiara-z-wampira-dla-arazille|141230]]]||
||Andrzej Domowierzec||2||[[[konspekty-zacmienie:141218-portret-boga|141218]]], [[[konspekty-zacmienie:141220-bogini-marzen-w-zonkiborze|141220]]]||
||Aneta Hanson||2||[[[konspekty-zacmienie:141227-przyczajona-andromeda-ukryty|141227]]], [[[konspekty-zacmienie:141230-ofiara-z-wampira-dla-arazille|141230]]]||
||Arazille||2||[[[konspekty-zacmienie:141227-przyczajona-andromeda-ukryty|141227]]], [[[konspekty-zacmienie:141230-ofiara-z-wampira-dla-arazille|141230]]]||
||Dariusz Germont||2||[[[konspekty-zacmienie:131008-moj-aniol|131008]]], [[[konspekty-zacmienie:150105-dar-iliusitiusa-dla-andromedy|150105]]]||
||Feliks Hanson||2||[[[konspekty-zacmienie:141227-przyczajona-andromeda-ukryty|141227]]], [[[konspekty-zacmienie:141230-ofiara-z-wampira-dla-arazille|141230]]]||
||Jolanta Wójt||2||[[[konspekty-zacmienie:130506-sekrety-rezydencji-szczypiorkow|130506]]], [[[konspekty-zacmienie:131008-moj-aniol|131008]]]||
||Józef Pimczak||2||[[[konspekty-zacmienie:141220-bogini-marzen-w-zonkiborze|141220]]], [[[konspekty-zacmienie:141227-przyczajona-andromeda-ukryty|141227]]]||
||Karradrael||2||[[[konspekty-zacmienie:141227-przyczajona-andromeda-ukryty|141227]]], [[[konspekty-zacmienie:141230-ofiara-z-wampira-dla-arazille|141230]]]||
||Michał Czuk||2||[[[konspekty-zacmienie:141220-bogini-marzen-w-zonkiborze|141220]]], [[[konspekty-zacmienie:141227-przyczajona-andromeda-ukryty|141227]]]||
||Netheria Diakon||2||[[[konspekty-zacmienie:141227-przyczajona-andromeda-ukryty|141227]]], [[[konspekty-zacmienie:141230-ofiara-z-wampira-dla-arazille|141230]]]||
||Patryk Romczak||2||[[[konspekty-zacmienie:141227-przyczajona-andromeda-ukryty|141227]]], [[[konspekty-zacmienie:150105-dar-iliusitiusa-dla-andromedy|150105]]]||
||Rafał Szczęślik||2||[[[konspekty-zacmienie:141227-przyczajona-andromeda-ukryty|141227]]], [[[konspekty-zacmienie:141230-ofiara-z-wampira-dla-arazille|141230]]]||
||Zofia Szczypiorek||2||[[[konspekty-zacmienie:131008-moj-aniol|131008]]], [[[konspekty-zacmienie:150105-dar-iliusitiusa-dla-andromedy|150105]]]||
||Anabela Diakon||1||[[[konspekty-zacmienie:150105-dar-iliusitiusa-dla-andromedy|150105]]]||
||Antoni Wójt||1||[[[konspekty-zacmienie:131008-moj-aniol|131008]]]||
||Ewa Czuk||1||[[[konspekty-zacmienie:141220-bogini-marzen-w-zonkiborze|141220]]]||
||Filip Sztukar||1||[[[konspekty-zacmienie:141227-przyczajona-andromeda-ukryty|141227]]]||
||Gabriel Newa||1||[[[konspekty-zacmienie:141230-ofiara-z-wampira-dla-arazille|141230]]]||
||Hektor Poran||1||[[[konspekty-zacmienie:141220-bogini-marzen-w-zonkiborze|141220]]]||
||Izabela Kruczek||1||[[[konspekty-zacmienie:130506-sekrety-rezydencji-szczypiorkow|130506]]]||
||Józef Pasan||1||[[[konspekty-zacmienie:150105-dar-iliusitiusa-dla-andromedy|150105]]]||
||Luiza Wanta||1||[[[konspekty-zacmienie:141227-przyczajona-andromeda-ukryty|141227]]]||
||Luksja Diakon||1||[[[konspekty-zacmienie:141227-przyczajona-andromeda-ukryty|141227]]]||
||Maciej Dworek||1||[[[konspekty-zacmienie:141230-ofiara-z-wampira-dla-arazille|141230]]]||
||Marcel Bankierz||1||[[[konspekty-zacmienie:121104-banshee-corka-mandragory|121104]]]||
||Robert Przerot||1||[[[konspekty-zacmienie:141230-ofiara-z-wampira-dla-arazille|141230]]]||
||Szymon Skubny||1||[[[konspekty-zacmienie:141227-przyczajona-andromeda-ukryty|141227]]]||