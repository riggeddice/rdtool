+ Andromeda (Kasia Nowak)

+++ Jaka jest:

wyrazi każde uczucie +++
każdego oszuka +++
przeżywa jak karaluch +++
nie zapomina +++

opiekuńcza wobec rodziny i przyjaciół ++
ludzie jej ufają / zwierzają się ++

+++ Co potrafi

artysta malarz +++
mistrzyni kłamstw +++
kontrolowana ofiara zaklęć mentalnych +++

zna się na okultyzmie ++
barmanka ++
pojedzie wszystkim ++

+++ Kogo zna:

studenci +++
środowisko artystyczne +++
okultyści +++

+++ Jakie ma zasoby:

zarobki jako Andromeda ++

+++ Specjalne:

magia zawsze ją znajdzie +++
magia luster +++

-----

+++ Na misjach:

+++++ Postać wystąpiła na: 16 misjach.
+++++ Postać wystąpiła w: 2 kampaniach: Światło w Zależu Leśnym, Czarodziejka Luster

||~ Data ||~ Numer ||~ Misja ||~ W kampanii ||
|| 150427 || 002 || [[[konspekty-zacmienie:150427-kult-zaleskiego-aniola|Kult zaleskiego anioła (An, Mo)]]]||Światło w Zależu Leśnym||
|| 120918 || 005 || [[[konspekty-zacmienie:120918-mozna-doprowadzic-maga-do|Można doprowadzić maga do problemu... (An)]]]||Czarodziejka Luster||
|| 120920 || 006 || [[[konspekty-zacmienie:120920-ale-nie-mozna-zmusic-go-do|Ale nie można zmusić go do jego rozwiązania... (An)]]]||Czarodziejka Luster||
|| 121013 || 007 || [[[konspekty-zacmienie:121013-zwlaszcza-gdy-cos-jest-ciagle|Zwłaszcza, gdy coś jest ciągle nie tak. (An)]]]||Czarodziejka Luster||
|| 121104 || 008 || [[[konspekty-zacmienie:121104-banshee-corka-mandragory|Banshee, córka mandragory. (An)]]]||Czarodziejka Luster||
|| 130503 || 009 || [[[konspekty-zacmienie:130503-renowacja-obrazu-andromedy|Renowacja obrazu Andromedy (An)]]]||Czarodziejka Luster||
|| 130506 || 010 || [[[konspekty-zacmienie:130506-sekrety-rezydencji-szczypiorkow|Sekrety rezydencji Szczypiorków (An)]]]||Czarodziejka Luster||
|| 130511 || 011 || [[[konspekty-zacmienie:130511-oltarz-podniesionej-dloni|Ołtarz Podniesionej Dłoni (An)]]]||Czarodziejka Luster||
|| 131008 || 012 || [[[konspekty-zacmienie:131008-moj-aniol|<<Mój Anioł>> (An)]]]||Czarodziejka Luster||
|| 141218 || 013 || [[[konspekty-zacmienie:141218-portret-boga|Portret Boga (An)]]]||Czarodziejka Luster||
|| 141220 || 014 || [[[konspekty-zacmienie:141220-bogini-marzen-w-zonkiborze|Bogini Marzeń w Żonkiborze (An)]]]||Czarodziejka Luster||
|| 141227 || 015 || [[[konspekty-zacmienie:141227-przyczajona-andromeda-ukryty|Przyczajona Andromeda, ukryty Maus (An)]]]||Czarodziejka Luster||
|| 141230 || 016 || [[[konspekty-zacmienie:141230-ofiara-z-wampira-dla-arazille|Ofiara z wampira dla Arazille (An)]]]||Czarodziejka Luster||
|| 150103 || 017 || [[[konspekty-zacmienie:150103-pryzmat-mysli-peka|Pryzmat Myśli pęka (An)]]]||Czarodziejka Luster||
|| 150104 || 018 || [[[konspekty-zacmienie:150104-terminus-defiler-kaplan-arazille|Terminus-defiler, kapłan Arazille (An)]]]||Czarodziejka Luster||
|| 150105 || 019 || [[[konspekty-zacmienie:150105-dar-iliusitiusa-dla-andromedy|Dar Iliusitiusa dla Andromedy (An)]]]||Czarodziejka Luster||

+++ Dokonania:

|| [[[konspekty-zacmienie:150427-kult-zaleskiego-aniola|Kult zaleskiego anioła (An, Mo)]]] || niewidzialna i subtelna siła wydobywająca kluczowe informacje od pani weterynarz. Też: magnes na terminusów. ||
|| [[[konspekty-zacmienie:120918-mozna-doprowadzic-maga-do|Można doprowadzić maga do problemu... (An)]]] || wybitna malarka z unikalną pamięcią,  przerażona mocą dostępną magom ||
|| [[[konspekty-zacmienie:120920-ale-nie-mozna-zmusic-go-do|Ale nie można zmusić go do jego rozwiązania... (An)]]] || wybitna malarka z unikalną pamięcią, która ratuje terminusa przed mandragorą ||
|| [[[konspekty-zacmienie:121013-zwlaszcza-gdy-cos-jest-ciagle|Zwłaszcza, gdy coś jest ciągle nie tak. (An)]]] || wybitna malarka z unikalną pamięcią, która po raz pierwszy zdaje sobie sprawę, że jest jakoś powiązana z lustrami ||
|| [[[konspekty-zacmienie:121104-banshee-corka-mandragory|Banshee, córka mandragory. (An)]]] || człowiek ZNOWU wykonujący robotę za terminusa ||
|| [[[konspekty-zacmienie:130503-renowacja-obrazu-andromedy|Renowacja obrazu Andromedy (An)]]] || dowód na to, że gdzie mag nie może, tam się babę pośle ||
|| [[[konspekty-zacmienie:130506-sekrety-rezydencji-szczypiorkow|Sekrety rezydencji Szczypiorków (An)]]] || mastermind grupy szturmującej Rezydencję Szczypiorków. Aha, dla Sandry: terminuska i koleżanka Augusta. ||
|| [[[konspekty-zacmienie:130511-oltarz-podniesionej-dloni|Ołtarz Podniesionej Dłoni (An)]]] || człowiek, którego nie da się kontrolować ołtarzem i osoba eksperymentująca z mocami luster. ||
|| [[[konspekty-zacmienie:131008-moj-aniol|<<Mój Anioł>> (An)]]] || dowód na to, że ważniejszy jest pomysł niż moc magiczna. ||
|| [[[konspekty-zacmienie:141218-portret-boga|Portret Boga (An)]]] || która musi pracować z tym co ma a wszystko jej się rozłazi. ||
|| [[[konspekty-zacmienie:141220-bogini-marzen-w-zonkiborze|Bogini Marzeń w Żonkiborze (An)]]] || która rozwikłuje swoich sojuszników przy okazji zagadek lokalnych bogiń ||
|| [[[konspekty-zacmienie:141227-przyczajona-andromeda-ukryty|Przyczajona Andromeda, ukryty Maus (An)]]] || która kradnie wampira sprzed nosa bogini ||
|| [[[konspekty-zacmienie:141230-ofiara-z-wampira-dla-arazille|Ofiara z wampira dla Arazille (An)]]] || która kombinuje jak mieć ciastko i zjeść ciastko (Iliusitius vs Arazille) i szantażuje/trolluje sojuszników jak pro. ||
|| [[[konspekty-zacmienie:150103-pryzmat-mysli-peka|Pryzmat Myśli pęka (An)]]] || zbierająca informacje i obserwująca jak rozpada się lustro i jej własne wyobrażenie o niej samej. ||
|| [[[konspekty-zacmienie:150104-terminus-defiler-kaplan-arazille|Terminus-defiler, kapłan Arazille (An)]]] || konsolidująca siły i decydująca się na to, po której stronie stanąć. ||
|| [[[konspekty-zacmienie:150105-dar-iliusitiusa-dla-andromedy|Dar Iliusitiusa dla Andromedy (An)]]] || która poznaje elementy swej przeszłości choć nadal ma liczne bariery mentalne (bardzo osłabione przez Iliusitiusa). ||

+++ Relacje z postaciami:

||~ Z kim ||~ Intensywność ||~ Na misjach ||
||Samira Diakon||13||[[[konspekty-zacmienie:120918-mozna-doprowadzic-maga-do|120918]]], [[[konspekty-zacmienie:120920-ale-nie-mozna-zmusic-go-do|120920]]], [[[konspekty-zacmienie:121013-zwlaszcza-gdy-cos-jest-ciagle|121013]]], [[[konspekty-zacmienie:121104-banshee-corka-mandragory|121104]]], [[[konspekty-zacmienie:130503-renowacja-obrazu-andromedy|130503]]], [[[konspekty-zacmienie:130506-sekrety-rezydencji-szczypiorkow|130506]]], [[[konspekty-zacmienie:130511-oltarz-podniesionej-dloni|130511]]], [[[konspekty-zacmienie:131008-moj-aniol|131008]]], [[[konspekty-zacmienie:141218-portret-boga|141218]]], [[[konspekty-zacmienie:141220-bogini-marzen-w-zonkiborze|141220]]], [[[konspekty-zacmienie:141227-przyczajona-andromeda-ukryty|141227]]], [[[konspekty-zacmienie:141230-ofiara-z-wampira-dla-arazille|141230]]], [[[konspekty-zacmienie:150105-dar-iliusitiusa-dla-andromedy|150105]]]||
||August Bankierz||12||[[[konspekty-zacmienie:120918-mozna-doprowadzic-maga-do|120918]]], [[[konspekty-zacmienie:120920-ale-nie-mozna-zmusic-go-do|120920]]], [[[konspekty-zacmienie:121013-zwlaszcza-gdy-cos-jest-ciagle|121013]]], [[[konspekty-zacmienie:121104-banshee-corka-mandragory|121104]]], [[[konspekty-zacmienie:130503-renowacja-obrazu-andromedy|130503]]], [[[konspekty-zacmienie:130506-sekrety-rezydencji-szczypiorkow|130506]]], [[[konspekty-zacmienie:130511-oltarz-podniesionej-dloni|130511]]], [[[konspekty-zacmienie:131008-moj-aniol|131008]]], [[[konspekty-zacmienie:141218-portret-boga|141218]]], [[[konspekty-zacmienie:150103-pryzmat-mysli-peka|150103]]], [[[konspekty-zacmienie:150104-terminus-defiler-kaplan-arazille|150104]]], [[[konspekty-zacmienie:150105-dar-iliusitiusa-dla-andromedy|150105]]]||
||Sandra Stryjek||10||[[[konspekty-zacmienie:130506-sekrety-rezydencji-szczypiorkow|130506]]], [[[konspekty-zacmienie:130511-oltarz-podniesionej-dloni|130511]]], [[[konspekty-zacmienie:131008-moj-aniol|131008]]], [[[konspekty-zacmienie:141218-portret-boga|141218]]], [[[konspekty-zacmienie:141220-bogini-marzen-w-zonkiborze|141220]]], [[[konspekty-zacmienie:141227-przyczajona-andromeda-ukryty|141227]]], [[[konspekty-zacmienie:141230-ofiara-z-wampira-dla-arazille|141230]]], [[[konspekty-zacmienie:150103-pryzmat-mysli-peka|150103]]], [[[konspekty-zacmienie:150104-terminus-defiler-kaplan-arazille|150104]]], [[[konspekty-zacmienie:150105-dar-iliusitiusa-dla-andromedy|150105]]]||
||Herbert Zioło||7||[[[konspekty-zacmienie:130511-oltarz-podniesionej-dloni|130511]]], [[[konspekty-zacmienie:141220-bogini-marzen-w-zonkiborze|141220]]], [[[konspekty-zacmienie:141227-przyczajona-andromeda-ukryty|141227]]], [[[konspekty-zacmienie:141230-ofiara-z-wampira-dla-arazille|141230]]], [[[konspekty-zacmienie:150103-pryzmat-mysli-peka|150103]]], [[[konspekty-zacmienie:150104-terminus-defiler-kaplan-arazille|150104]]], [[[konspekty-zacmienie:150105-dar-iliusitiusa-dla-andromedy|150105]]]||
||Feliks Bozur||5||[[[konspekty-zacmienie:130511-oltarz-podniesionej-dloni|130511]]], [[[konspekty-zacmienie:141230-ofiara-z-wampira-dla-arazille|141230]]], [[[konspekty-zacmienie:150103-pryzmat-mysli-peka|150103]]], [[[konspekty-zacmienie:150104-terminus-defiler-kaplan-arazille|150104]]], [[[konspekty-zacmienie:150105-dar-iliusitiusa-dla-andromedy|150105]]]||
||Anna Kajak||4||[[[konspekty-zacmienie:141220-bogini-marzen-w-zonkiborze|141220]]], [[[konspekty-zacmienie:141227-przyczajona-andromeda-ukryty|141227]]], [[[konspekty-zacmienie:141230-ofiara-z-wampira-dla-arazille|141230]]], [[[konspekty-zacmienie:150103-pryzmat-mysli-peka|150103]]]||
||Arazille||4||[[[konspekty-zacmienie:141227-przyczajona-andromeda-ukryty|141227]]], [[[konspekty-zacmienie:141230-ofiara-z-wampira-dla-arazille|141230]]], [[[konspekty-zacmienie:150103-pryzmat-mysli-peka|150103]]], [[[konspekty-zacmienie:150104-terminus-defiler-kaplan-arazille|150104]]]||
||Augustyn Szczypiorek||4||[[[konspekty-zacmienie:130503-renowacja-obrazu-andromedy|130503]]], [[[konspekty-zacmienie:130506-sekrety-rezydencji-szczypiorkow|130506]]], [[[konspekty-zacmienie:130511-oltarz-podniesionej-dloni|130511]]], [[[konspekty-zacmienie:131008-moj-aniol|131008]]]||
||Iliusitius||4||[[[konspekty-zacmienie:141227-przyczajona-andromeda-ukryty|141227]]], [[[konspekty-zacmienie:141230-ofiara-z-wampira-dla-arazille|141230]]], [[[konspekty-zacmienie:150103-pryzmat-mysli-peka|150103]]], [[[konspekty-zacmienie:150105-dar-iliusitiusa-dla-andromedy|150105]]]||
||Inga Wójt||4||[[[konspekty-zacmienie:130503-renowacja-obrazu-andromedy|130503]]], [[[konspekty-zacmienie:130506-sekrety-rezydencji-szczypiorkow|130506]]], [[[konspekty-zacmienie:130511-oltarz-podniesionej-dloni|130511]]], [[[konspekty-zacmienie:131008-moj-aniol|131008]]]||
||Netheria Diakon||4||[[[konspekty-zacmienie:141227-przyczajona-andromeda-ukryty|141227]]], [[[konspekty-zacmienie:141230-ofiara-z-wampira-dla-arazille|141230]]], [[[konspekty-zacmienie:150103-pryzmat-mysli-peka|150103]]], [[[konspekty-zacmienie:150104-terminus-defiler-kaplan-arazille|150104]]]||
||Radosław Szczypiorek||4||[[[konspekty-zacmienie:130503-renowacja-obrazu-andromedy|130503]]], [[[konspekty-zacmienie:130506-sekrety-rezydencji-szczypiorkow|130506]]], [[[konspekty-zacmienie:130511-oltarz-podniesionej-dloni|130511]]], [[[konspekty-zacmienie:131008-moj-aniol|131008]]]||
||Wojciech Kajak||4||[[[konspekty-zacmienie:141220-bogini-marzen-w-zonkiborze|141220]]], [[[konspekty-zacmienie:141227-przyczajona-andromeda-ukryty|141227]]], [[[konspekty-zacmienie:141230-ofiara-z-wampira-dla-arazille|141230]]], [[[konspekty-zacmienie:150103-pryzmat-mysli-peka|150103]]]||
||Żanna Szczypiorek||4||[[[konspekty-zacmienie:130503-renowacja-obrazu-andromedy|130503]]], [[[konspekty-zacmienie:130506-sekrety-rezydencji-szczypiorkow|130506]]], [[[konspekty-zacmienie:130511-oltarz-podniesionej-dloni|130511]]], [[[konspekty-zacmienie:131008-moj-aniol|131008]]]||
||Andrzej Szop||3||[[[konspekty-zacmienie:141227-przyczajona-andromeda-ukryty|141227]]], [[[konspekty-zacmienie:141230-ofiara-z-wampira-dla-arazille|141230]]], [[[konspekty-zacmienie:150105-dar-iliusitiusa-dla-andromedy|150105]]]||
||Artur Szmelc||3||[[[konspekty-zacmienie:130503-renowacja-obrazu-andromedy|130503]]], [[[konspekty-zacmienie:141218-portret-boga|141218]]], [[[konspekty-zacmienie:141227-przyczajona-andromeda-ukryty|141227]]]||
||Feliks Hanson||3||[[[konspekty-zacmienie:141227-przyczajona-andromeda-ukryty|141227]]], [[[konspekty-zacmienie:141230-ofiara-z-wampira-dla-arazille|141230]]], [[[konspekty-zacmienie:150104-terminus-defiler-kaplan-arazille|150104]]]||
||Gabriel Newa||3||[[[konspekty-zacmienie:141230-ofiara-z-wampira-dla-arazille|141230]]], [[[konspekty-zacmienie:150103-pryzmat-mysli-peka|150103]]], [[[konspekty-zacmienie:150104-terminus-defiler-kaplan-arazille|150104]]]||
||Józef Pimczak||3||[[[konspekty-zacmienie:141220-bogini-marzen-w-zonkiborze|141220]]], [[[konspekty-zacmienie:141227-przyczajona-andromeda-ukryty|141227]]], [[[konspekty-zacmienie:150103-pryzmat-mysli-peka|150103]]]||
||Luksja Diakon||3||[[[konspekty-zacmienie:141227-przyczajona-andromeda-ukryty|141227]]], [[[konspekty-zacmienie:150103-pryzmat-mysli-peka|150103]]], [[[konspekty-zacmienie:150104-terminus-defiler-kaplan-arazille|150104]]]||
||Małgorzata Poran||3||[[[konspekty-zacmienie:141218-portret-boga|141218]]], [[[konspekty-zacmienie:141220-bogini-marzen-w-zonkiborze|141220]]], [[[konspekty-zacmienie:141227-przyczajona-andromeda-ukryty|141227]]]||
||Patryk Romczak||3||[[[konspekty-zacmienie:141227-przyczajona-andromeda-ukryty|141227]]], [[[konspekty-zacmienie:150103-pryzmat-mysli-peka|150103]]], [[[konspekty-zacmienie:150105-dar-iliusitiusa-dla-andromedy|150105]]]||
||Andrzej Domowierzec||2||[[[konspekty-zacmienie:141218-portret-boga|141218]]], [[[konspekty-zacmienie:141220-bogini-marzen-w-zonkiborze|141220]]]||
||Aneta Hanson||2||[[[konspekty-zacmienie:141227-przyczajona-andromeda-ukryty|141227]]], [[[konspekty-zacmienie:141230-ofiara-z-wampira-dla-arazille|141230]]]||
||Dariusz Germont||2||[[[konspekty-zacmienie:131008-moj-aniol|131008]]], [[[konspekty-zacmienie:150105-dar-iliusitiusa-dla-andromedy|150105]]]||
||Jolanta Wójt||2||[[[konspekty-zacmienie:130506-sekrety-rezydencji-szczypiorkow|130506]]], [[[konspekty-zacmienie:131008-moj-aniol|131008]]]||
||Karradrael||2||[[[konspekty-zacmienie:141227-przyczajona-andromeda-ukryty|141227]]], [[[konspekty-zacmienie:141230-ofiara-z-wampira-dla-arazille|141230]]]||
||Michał Czuk||2||[[[konspekty-zacmienie:141220-bogini-marzen-w-zonkiborze|141220]]], [[[konspekty-zacmienie:141227-przyczajona-andromeda-ukryty|141227]]]||
||Rafał Szczęślik||2||[[[konspekty-zacmienie:141227-przyczajona-andromeda-ukryty|141227]]], [[[konspekty-zacmienie:141230-ofiara-z-wampira-dla-arazille|141230]]]||
||Zofia Szczypiorek||2||[[[konspekty-zacmienie:131008-moj-aniol|131008]]], [[[konspekty-zacmienie:150105-dar-iliusitiusa-dla-andromedy|150105]]]||
||Amelia Eter||1||[[[konspekty-zacmienie:150104-terminus-defiler-kaplan-arazille|150104]]]||
||Anabela Diakon||1||[[[konspekty-zacmienie:150105-dar-iliusitiusa-dla-andromedy|150105]]]||
||Aneta Kosicz||1||[[[konspekty-zacmienie:150427-kult-zaleskiego-aniola|150427]]]||
||Antoni Wójt||1||[[[konspekty-zacmienie:131008-moj-aniol|131008]]]||
||Ewa Czuk||1||[[[konspekty-zacmienie:141220-bogini-marzen-w-zonkiborze|141220]]]||
||Filip Sztukar||1||[[[konspekty-zacmienie:141227-przyczajona-andromeda-ukryty|141227]]]||
||Franciszek Błazoń||1||[[[konspekty-zacmienie:150427-kult-zaleskiego-aniola|150427]]]||
||Hektor Poran||1||[[[konspekty-zacmienie:141220-bogini-marzen-w-zonkiborze|141220]]]||
||Izabela Kruczek||1||[[[konspekty-zacmienie:130506-sekrety-rezydencji-szczypiorkow|130506]]]||
||Józef Pasan||1||[[[konspekty-zacmienie:150105-dar-iliusitiusa-dla-andromedy|150105]]]||
||Kamil Gurnat||1||[[[konspekty-zacmienie:150427-kult-zaleskiego-aniola|150427]]]||
||Karolina Błazoń||1||[[[konspekty-zacmienie:150427-kult-zaleskiego-aniola|150427]]]||
||Luiza Wanta||1||[[[konspekty-zacmienie:141227-przyczajona-andromeda-ukryty|141227]]]||
||Maciej Dworek||1||[[[konspekty-zacmienie:141230-ofiara-z-wampira-dla-arazille|141230]]]||
||Marcel Bankierz||1||[[[konspekty-zacmienie:121104-banshee-corka-mandragory|121104]]]||
||Marek Ossoliński||1||[[[konspekty-zacmienie:150427-kult-zaleskiego-aniola|150427]]]||
||Mateusz Kozociej||1||[[[konspekty-zacmienie:150427-kult-zaleskiego-aniola|150427]]]||
||Mirabelka Diakon||1||[[[konspekty-zacmienie:150104-terminus-defiler-kaplan-arazille|150104]]]||
||Olga Miodownik||1||[[[konspekty-zacmienie:150427-kult-zaleskiego-aniola|150427]]]||
||Paweł Franna||1||[[[konspekty-zacmienie:150427-kult-zaleskiego-aniola|150427]]]||
||Rafał Czapiek||1||[[[konspekty-zacmienie:150427-kult-zaleskiego-aniola|150427]]]||
||Robert Przerot||1||[[[konspekty-zacmienie:141230-ofiara-z-wampira-dla-arazille|141230]]]||
||Sebastian Linka||1||[[[konspekty-zacmienie:150103-pryzmat-mysli-peka|150103]]]||
||Szymon Skubny||1||[[[konspekty-zacmienie:141227-przyczajona-andromeda-ukryty|141227]]]||