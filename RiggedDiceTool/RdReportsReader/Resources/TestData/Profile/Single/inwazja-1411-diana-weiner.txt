+ Diana Weiner

+++ Jaka jest:

czarująca idealistka, fanatycznie oddana idei sanacji Świecy, "cel uświęca środki a historia nas osądzi", szeroko wykształcona, charyzmatyczna

+++ Co umie:

kapłanka Szlachty, świetna znajomość historii, publiczne przemowy,

+++ Kogo zna:

wysoki agent Szlachty,

+++ Co ma do dyspozycji:



+++ ElMet:

-

+++ Specjalne:

-

+++ Profil:

[[code]]
-----------------------------------
Pro / Re| DIRECT     | SUBTLE     |
-----------------------------------
acuity  |     3      |     2      |
body    |     1      |     2      |
heart   |     4      |     4      |
will    |     4      |     4      |
-----------------------------------
[[/code]]

+++ Archetyp:

[[[mechanika-inwazja-archetypy-cult-speaker|Cult Speaker]]]

+++ Inne:

Delikatna i eteryczna idealistka, choć całkowicie bezkompromisowa. Święcie wierzy w ideał oczyszczenia Srebrnej Świecy, nawet kosztem tymczasowego cierpienia niewinnych magów. Uważa, że Świeca musi zostać oczyszczona i że jest to jedyna nadzieja dla gildii. Wierzy w dobro i w czystość intencji swojej strony i ogólnie bardzo wierzy w magów - a zwłaszcza magów Świecy. To wszystko, całe to zło jest robione tylko i wyłącznie dla dobra Świecy a historia osądzi Szlachtę.

+++ Na misjach:

+++++ Postać wystąpiła na: 6 misjach.
+++++ Postać wystąpiła w: 1 kampaniach: Mała prywatna wojenka

||~ Data ||~ Numer ||~ Misja ||~ W kampanii ||
|| 150728 || 049 || [[[konspekty-zacmienie:150728-sojusz-przeciwko-szlachcie|Sojusz przeciwko Szlachcie (HB, SD)]]]||Mała prywatna wojenka||
|| 151001 || 053 || [[[konspekty-zacmienie:151001-plan-ujawnienia-z-hipernetu|Plan ujawnienia z Hipernetu (HB, DK)]]]||Mała prywatna wojenka||
|| 151007 || 057 || [[[konspekty-zacmienie:151007-nigdy-dosc-przyjaciol-sz-kur|Nigdy dość przyjaciół: Szlachta i Kurtyna (HB, SD)]]]||Mała prywatna wojenka||
|| 151021 || 062 || [[[konspekty-zacmienie:151021-przebudzenie-mojry|Przebudzenie Mojry (HB, SD)]]]||Mała prywatna wojenka||
|| 151110 || 064 || [[[konspekty-zacmienie:151110-romeo-i-hektor|Romeo i... Hektor (HB, AB, DK)]]]||Mała prywatna wojenka||
|| 151212 || 068 || [[[konspekty-zacmienie:151212-antyporadnik-wedkarza-arazille|Antyporadnik wędkarza Arazille (HB, AB, DK)]]]||Mała prywatna wojenka||

+++ Dokonania:

|| [[[konspekty-zacmienie:150728-sojusz-przeciwko-szlachcie|Sojusz przeciwko Szlachcie (HB, SD)]]] || wysoko postawiona członkini Szlachty w Srebrnej Świecy ciężko poraniona przez merkuriasza Marcelina przez pułapkę Leokadii Myszeczki. ||
|| [[[konspekty-zacmienie:151001-plan-ujawnienia-z-hipernetu|Plan ujawnienia z Hipernetu (HB, DK)]]] || bardzo sympatyczna i dążąca do pokoju (jak twierdzi) szalona kultystka (zdaniem Vladleny). Hektor ją lubi. ||
|| [[[konspekty-zacmienie:151007-nigdy-dosc-przyjaciol-sz-kur|Nigdy dość przyjaciół: Szlachta i Kurtyna (HB, SD)]]] || fanatyczna Sprawie, przyznała Hektorowi, że chce by prokuratura wróciła. Wybitna manipulatorka. Zainteresowana Silurią. ||
|| [[[konspekty-zacmienie:151021-przebudzenie-mojry|Przebudzenie Mojry (HB, SD)]]] || która podeszła Hektora i po raz pierwszy zobaczyła w Wiktorze faceta. ||
|| [[[konspekty-zacmienie:151110-romeo-i-hektor|Romeo i... Hektor (HB, AB, DK)]]] || która opowiedziała Hektorowi o Romeo Diakonie. ||
|| [[[konspekty-zacmienie:151212-antyporadnik-wedkarza-arazille|Antyporadnik wędkarza Arazille (HB, AB, DK)]]] || która zaręcza za Jurija i jego prawidłowość. Nie wie o "Stokrotce Diakon"... ||

+++ Relacje z postaciami:

||~ Z kim ||~ Intensywność ||~ Na misjach ||
||Edwin Blakenbauer||6||[[[konspekty-zacmienie:150728-sojusz-przeciwko-szlachcie|150728]]], [[[konspekty-zacmienie:151001-plan-ujawnienia-z-hipernetu|151001]]], [[[konspekty-zacmienie:151007-nigdy-dosc-przyjaciol-sz-kur|151007]]], [[[konspekty-zacmienie:151021-przebudzenie-mojry|151021]]], [[[konspekty-zacmienie:151110-romeo-i-hektor|151110]]], [[[konspekty-zacmienie:151212-antyporadnik-wedkarza-arazille|151212]]]||
||Hektor Blakenbauer||6||[[[konspekty-zacmienie:150728-sojusz-przeciwko-szlachcie|150728]]], [[[konspekty-zacmienie:151001-plan-ujawnienia-z-hipernetu|151001]]], [[[konspekty-zacmienie:151007-nigdy-dosc-przyjaciol-sz-kur|151007]]], [[[konspekty-zacmienie:151021-przebudzenie-mojry|151021]]], [[[konspekty-zacmienie:151110-romeo-i-hektor|151110]]], [[[konspekty-zacmienie:151212-antyporadnik-wedkarza-arazille|151212]]]||
||Margaret Blakenbauer||4||[[[konspekty-zacmienie:150728-sojusz-przeciwko-szlachcie|150728]]], [[[konspekty-zacmienie:151001-plan-ujawnienia-z-hipernetu|151001]]], [[[konspekty-zacmienie:151021-przebudzenie-mojry|151021]]], [[[konspekty-zacmienie:151212-antyporadnik-wedkarza-arazille|151212]]]||
||Siluria Diakon||4||[[[konspekty-zacmienie:150728-sojusz-przeciwko-szlachcie|150728]]], [[[konspekty-zacmienie:151007-nigdy-dosc-przyjaciol-sz-kur|151007]]], [[[konspekty-zacmienie:151021-przebudzenie-mojry|151021]]], [[[konspekty-zacmienie:151212-antyporadnik-wedkarza-arazille|151212]]]||
||Vladlena Zajcew||4||[[[konspekty-zacmienie:150728-sojusz-przeciwko-szlachcie|150728]]], [[[konspekty-zacmienie:151001-plan-ujawnienia-z-hipernetu|151001]]], [[[konspekty-zacmienie:151007-nigdy-dosc-przyjaciol-sz-kur|151007]]], [[[konspekty-zacmienie:151212-antyporadnik-wedkarza-arazille|151212]]]||
||Adrian Murarz||3||[[[konspekty-zacmienie:151001-plan-ujawnienia-z-hipernetu|151001]]], [[[konspekty-zacmienie:151007-nigdy-dosc-przyjaciol-sz-kur|151007]]], [[[konspekty-zacmienie:151021-przebudzenie-mojry|151021]]]||
||Dionizy Kret||3||[[[konspekty-zacmienie:151001-plan-ujawnienia-z-hipernetu|151001]]], [[[konspekty-zacmienie:151110-romeo-i-hektor|151110]]], [[[konspekty-zacmienie:151212-antyporadnik-wedkarza-arazille|151212]]]||
||GS 'Aegis' 0003||3||[[[konspekty-zacmienie:150728-sojusz-przeciwko-szlachcie|150728]]], [[[konspekty-zacmienie:151007-nigdy-dosc-przyjaciol-sz-kur|151007]]], [[[konspekty-zacmienie:151021-przebudzenie-mojry|151021]]]||
||Marcelin Blakenbauer||3||[[[konspekty-zacmienie:150728-sojusz-przeciwko-szlachcie|150728]]], [[[konspekty-zacmienie:151110-romeo-i-hektor|151110]]], [[[konspekty-zacmienie:151212-antyporadnik-wedkarza-arazille|151212]]]||
||Mojra||3||[[[konspekty-zacmienie:151021-przebudzenie-mojry|151021]]], [[[konspekty-zacmienie:151110-romeo-i-hektor|151110]]], [[[konspekty-zacmienie:151212-antyporadnik-wedkarza-arazille|151212]]]||
||Wiktor Sowiński||3||[[[konspekty-zacmienie:151001-plan-ujawnienia-z-hipernetu|151001]]], [[[konspekty-zacmienie:151007-nigdy-dosc-przyjaciol-sz-kur|151007]]], [[[konspekty-zacmienie:151021-przebudzenie-mojry|151021]]]||
||Alina Bednarz||2||[[[konspekty-zacmienie:151110-romeo-i-hektor|151110]]], [[[konspekty-zacmienie:151212-antyporadnik-wedkarza-arazille|151212]]]||
||Edward Sasanka||2||[[[konspekty-zacmienie:151007-nigdy-dosc-przyjaciol-sz-kur|151007]]], [[[konspekty-zacmienie:151021-przebudzenie-mojry|151021]]]||
||Emilia Kołatka||2||[[[konspekty-zacmienie:150728-sojusz-przeciwko-szlachcie|150728]]], [[[konspekty-zacmienie:151021-przebudzenie-mojry|151021]]]||
||Romeo Diakon||2||[[[konspekty-zacmienie:151110-romeo-i-hektor|151110]]], [[[konspekty-zacmienie:151212-antyporadnik-wedkarza-arazille|151212]]]||
||Szymon Skubny||2||[[[konspekty-zacmienie:151021-przebudzenie-mojry|151021]]], [[[konspekty-zacmienie:151110-romeo-i-hektor|151110]]]||
||Tatiana Zajcew||2||[[[konspekty-zacmienie:151021-przebudzenie-mojry|151021]]], [[[konspekty-zacmienie:151110-romeo-i-hektor|151110]]]||
||Anna Kajak||1||[[[konspekty-zacmienie:151001-plan-ujawnienia-z-hipernetu|151001]]]||
||Arazille||1||[[[konspekty-zacmienie:151212-antyporadnik-wedkarza-arazille|151212]]]||
||Aurel Czarko||1||[[[konspekty-zacmienie:151007-nigdy-dosc-przyjaciol-sz-kur|151007]]]||
||Borys Kumin||1||[[[konspekty-zacmienie:151021-przebudzenie-mojry|151021]]]||
||Czirna Zajcew||1||[[[konspekty-zacmienie:151212-antyporadnik-wedkarza-arazille|151212]]]||
||Erebos||1||[[[konspekty-zacmienie:151212-antyporadnik-wedkarza-arazille|151212]]]||
||Himechan||1||[[[konspekty-zacmienie:151212-antyporadnik-wedkarza-arazille|151212]]]||
||Jurij Zajcew||1||[[[konspekty-zacmienie:151212-antyporadnik-wedkarza-arazille|151212]]]||
||Leokadia Myszeczka||1||[[[konspekty-zacmienie:150728-sojusz-przeciwko-szlachcie|150728]]]||
||Marian Agrest||1||[[[konspekty-zacmienie:150728-sojusz-przeciwko-szlachcie|150728]]]||
||Netheria Diakon||1||[[[konspekty-zacmienie:151110-romeo-i-hektor|151110]]]||
||Oddział Zeta||1||[[[konspekty-zacmienie:151212-antyporadnik-wedkarza-arazille|151212]]]||
||Oktawian Maus||1||[[[konspekty-zacmienie:151007-nigdy-dosc-przyjaciol-sz-kur|151007]]]||
||Otton Blakenbauer||1||[[[konspekty-zacmienie:151212-antyporadnik-wedkarza-arazille|151212]]]||
||Ozydiusz Bankierz||1||[[[konspekty-zacmienie:150728-sojusz-przeciwko-szlachcie|150728]]]||
||Patrycja Krowiowska||1||[[[konspekty-zacmienie:151212-antyporadnik-wedkarza-arazille|151212]]]||
||Patryk Kloszard||1||[[[konspekty-zacmienie:151110-romeo-i-hektor|151110]]]||
||Rufus Czubek||1||[[[konspekty-zacmienie:151021-przebudzenie-mojry|151021]]]||
||Saith Kameleon||1||[[[konspekty-zacmienie:150728-sojusz-przeciwko-szlachcie|150728]]]||
||Wanda Ketran||1||[[[konspekty-zacmienie:151212-antyporadnik-wedkarza-arazille|151212]]]||