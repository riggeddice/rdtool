import unittest

from RdReportsReader.Resources.TestData.MockSourceResources import load_described_profile_resource


class TestResourceFileReader(unittest.TestCase):

    def test_profile_resource_throws_value_error_if_no_of_valid_category(self):

        # Given
        # When

        with self.assertRaises(Exception) as context:
            load_described_profile_resource("aslkjfqlikfjalfjalifjalif", "krystian korzunio", "1601")

        # Then

        self.assertTrue("There exists no directory:" in str(context.exception))

    def test_profile_resource_throws_value_error_if_no_valid_file(self):

        # Given
        # When

        with self.assertRaises(Exception) as context:
            load_described_profile_resource("single", "sdakjfsklfjslfjdslkjdfsfsdff")

        # Then

        self.assertTrue("There exists no file" in str(context.exception))

    def test_profile_resource_throws_value_error_if_can_have_more_than_one_file(self):

        # Given
        # When

        with self.assertRaises(Exception) as context:
            load_described_profile_resource("single", ".txt")

        # Then

        self.assertTrue("More than one file can be described" in str(context.exception))

    def test_profile_resource_reads_single_report(self):

        # Given
        # When

        actual_1 = load_described_profile_resource("single", "krystian", "korzunio", "1601")
        actual_2 = load_described_profile_resource("single", "inwazja-1601-krystian-korzunio.txt")

        # Then

        self.assertTrue(actual_1, "We got None, nothing got read")
        self.assertTrue(actual_2, "We got None, nothing got read")
        self.assertTrue(actual_1 == actual_2)
