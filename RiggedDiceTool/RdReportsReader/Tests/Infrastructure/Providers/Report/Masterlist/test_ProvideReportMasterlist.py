import unittest

from RdReportsReader.Resources.TestData.MockSourceResources import load_described_report_resource
from RdReportsReader.Src.Infrastructure.Read.Provide.Report import provide_from_report_domain
from RdReportsReader.Src.Infrastructure.Read.Source.Mock.MockAggregatedSource import MockAggregatedSource


class TestProvideReportMasterlist(unittest.TestCase):

    def test_reportMasterListProvider_worksWith_C01M06_Correct(self):

        # Given
        selected_source = MockAggregatedSource()
        selected_source.select_report_masterlist(load_described_report_resource("Masterlist", "C01M06_correct"))

        provide = provide_from_report_domain(selected_source)

        # When
        report_masterlist = provide.masterlist()

        # Then
        self.assertTrue(report_masterlist is not None, "ReportMasterlist returned as None on mock source")
        self.assertTrue(report_masterlist.metadata_count() == 6, "Wrong amount of reports in masterlist")
        self.assertTrue(report_masterlist.campaign_count() == 1, "Wrong amount of campaigns in masterlist")


