import unittest

from RdReportsReader.Resources.TestData.MockSourceResources import load_described_report_resource
from RdReportsReader.Src.Infrastructure.Read.Provide.Report import provide_from_report_domain
from RdReportsReader.Src.Infrastructure.Read.Source.Mock.MockAggregatedSource import MockAggregatedSource


class TestProvideReportMasterlist(unittest.TestCase):

    def test_reportCollectionProvider_worksWith_C01M06_CorrectComplete(self):

        # Given
        selected_source = MockAggregatedSource()
        selected_source.select_reports(load_described_report_resource("Collection", "C01M06_CorrectComplete"))

        provide = provide_from_report_domain(selected_source)

        # When
        report_collection = provide.collection()

        self.assertTrue(report_collection is not None, "Provide returned None instead of a ReportCollection.")
        self.assertTrue(report_collection.count() == 6, "Returned ReportCollection did not contain 6 missions as expected.")

    def test_reportCollectionProvider_worksWith_C01M02_MPW_Simplest_Locations(self):

        # Given
        selected_source = MockAggregatedSource()
        selected_source.select_reports(load_described_report_resource("Collection", "C01M02_MpwSimplestLocations"))

        provide = provide_from_report_domain(selected_source)

        # When
        report_collection = provide.collection()

        self.assertTrue(report_collection is not None, "Provide returned None instead of a ReportCollection.")
        self.assertTrue(report_collection.count() == 2, "Returned ReportCollection did not contain 6 missions as expected.")
