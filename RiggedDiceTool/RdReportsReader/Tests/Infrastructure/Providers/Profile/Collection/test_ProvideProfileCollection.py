import unittest

from RdReportsReader.Resources.TestData.MockSourceResources import load_described_profile_resource
from RdReportsReader.Src.Infrastructure.Read.Provide.Profile import provide_from_profile_domain
from RdReportsReader.Src.Infrastructure.Read.Source.Mock.MockAggregatedSource import MockAggregatedSource


class TestProvideProfileCollection(unittest.TestCase):

    def test_profileCollectionProvider_worksWith_P03_CorrectComplete(self):

        # Given

        selected_source = MockAggregatedSource()
        selected_source.select_profiles(load_described_profile_resource("Collection", "F03S04P08_SimpleComplete"))

        provide = provide_from_profile_domain(selected_source)

        # When

        profile_collection = provide.collection()

        # Then

        self.assertTrue(profile_collection is not None, "Provide returned None instead of a ProfileCollection.")
        self.assertTrue(profile_collection.count() == 8, "Returned ProfileCollection did not contain 8 profiles as expected.")
