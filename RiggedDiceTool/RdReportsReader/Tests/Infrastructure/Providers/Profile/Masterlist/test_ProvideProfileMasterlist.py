import unittest

from RdReportsReader.Resources.TestData.MockSourceResources import load_described_profile_resource
from RdReportsReader.Src.Infrastructure.Read.Provide.Profile import provide_from_profile_domain
from RdReportsReader.Src.Infrastructure.Read.Source.Mock.MockAggregatedSource import MockAggregatedSource


class TestProvideProfileMasterlist(unittest.TestCase):

    def test_reportMasterListProvider_worksWith_C01M06_Correct(self):

        # Given
        selected_source = MockAggregatedSource()
        selected_source.select_profile_masterlist(load_described_profile_resource("Masterlist", "F03S04P08_Simple"))

        provide = provide_from_profile_domain(selected_source)

        # When

        profile_masterlist = provide.masterlist()

        # Then

        self.assertTrue(profile_masterlist is not None, "ProfileMasterlist returned as nil on mock source")
        self.assertTrue(profile_masterlist.metadata_count() == 8, "Wrong amount of reports in profileMasterlist")
