import unittest

from RdReportsReader.Src.Domain.Report.Single.Complete.Structure.DramatisPersonae.Structure.DpRecord.CreateDpRecords import create_dp_records_from_dramatis_personae_section


class TestDpRecordsCreation(unittest.TestCase):

    def test_properly_formed_DramatisPersonae_record(self):

        # Given

        text = "+ Dramatis personae\n\n\n" + \
            "czł: Patrycja Krowiowska, która powiązała linie logistyczne dron ze Skrzydłorogu\n" + \
            "sda-dsadw czł: Patrycja Krowiowska, która powiązała linie logistyczne dron ze Skrzydłorogu\n" + \
            "- czł: [[[inwazja-karty-postaci-1609:patrycja-krowiowska|Patrycja Krowiowska]]]   " \
            ", która powiązała linie logistyczne dron ze Skrzydłorogu"

        # When

        result = create_dp_records_from_dramatis_personae_section(text)

        # Then

        self.assertTrue(len(result) == 3)
        self.assertTrue(result[0].name() == result[1].name() == result[2].name())
        self.assertTrue(result[0].actor_type() == result[1].actor_type() == result[2].actor_type())
        self.assertTrue(result[0].mission_deed() == result[1].mission_deed() == result[2].mission_deed())
