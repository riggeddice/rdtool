import unittest

from RdReportsReader.Src.Domain.Report.Single.Complete.Structure.DramatisPersonae.Structure.DpRecord.CreateSingleDpRecord import create_single_dp_record


class TestDpRecordCreation(unittest.TestCase):

    def test_with_one_delimiters_no_special_signs(self):

        # Given

        valid_text_1 = "czł: Patrycja Krowiowska, która powiązała linie logistyczne dron ze " \
                              "Skrzydłorogu i znalazła powiązanie ze Skubnym. Porwana w odwecie przez The Governess."
        valid_text_2 = "???: Patrycja Krowiowska, która powiązała linie logistyczne dron ze " \
                              "Skrzydłorogu i znalazła powiązanie ze Skubnym. Porwana w odwecie przez The Governess."
        valid_text_3 = "- czł: Patrycja Krowiowska, która powiązała linie logistyczne dron ze " \
                       "Skrzydłorogu i znalazła powiązanie ze Skubnym. Porwana w odwecie przez The Governess."

        # When

        result_1 = create_single_dp_record(valid_text_1)
        result_2 = create_single_dp_record(valid_text_2)
        result_3 = create_single_dp_record(valid_text_3)

        # Then

        self.assertTrue(result_1.actor_type() != result_2.actor_type())
        self.assertTrue(result_1.mission_deed() == result_2.mission_deed() == result_3.mission_deed())
        self.assertTrue(result_1.name() == result_2.name() == "Patrycja Krowiowska")
        self.assertTrue(result_1.actor_type() == result_3.actor_type() == "czł")
        self.assertTrue(result_2.actor_type() == "???")
        self.assertTrue("która powiązała linie logistyczne" in result_1.mission_deed())

    def test_with_different_delimiters_no_special_signs(self):

        # Given

        valid_text_1 = "czł: Patrycja Krowiowska jako ktośtam"
        valid_text_2 = "czł: Patrycja Krowiowska, ktośtam"

        # When

        result_1 = create_single_dp_record(valid_text_1)
        result_2 = create_single_dp_record(valid_text_2)

        # Then

        self.assertTrue(result_1.actor_type() == result_2.actor_type())
        self.assertTrue(result_1.mission_deed() == result_2.mission_deed())
        self.assertTrue(result_1.name() == result_2.name())
        self.assertTrue(result_1.actor_type() == "czł")
        self.assertTrue(result_1.name() == "Patrycja Krowiowska")
        self.assertTrue("ktośtam" in result_1.mission_deed())

    def test_with_twin_delimiter_no_special_signs(self):

        # Given

        valid_text_11 = "czł: Patrycja Krowiowska jako ktośtam jako łapczak"
        valid_text_12 = "czł: Patrycja Krowiowska jako ktośtam, łapczak"
        valid_text_22 = "czł: Patrycja Krowiowska, ktośtam, łapczak"
        valid_text_21 = "czł: Patrycja Krowiowska, ktośtam jako łapczak"

        # When

        result_11 = create_single_dp_record(valid_text_11)
        result_12 = create_single_dp_record(valid_text_12)
        result_22 = create_single_dp_record(valid_text_22)
        result_21 = create_single_dp_record(valid_text_21)

        # Then

        self.assertTrue(result_11.actor_type() == result_12.actor_type() == result_21.actor_type() == result_22.actor_type())
        self.assertTrue(result_11.name() == result_12.name() == result_21.name() == result_22.name())
        self.assertTrue(result_11.mission_deed() == result_21.mission_deed())
        self.assertTrue(result_12.mission_deed() == result_22.mission_deed())

    def test_with_special_symbols_greedy_regex(self):

        # Given

        valid_text_1 = "czł: Patrycja Krowiowska jako : ktośtam: jako łapczak"
        valid_text_2 = "czł: Patrycja Krowiowska,: ktośtam:, :łapczak"

        # When

        result_1 = create_single_dp_record(valid_text_1)
        result_2 = create_single_dp_record(valid_text_2)

        # Then

        self.assertTrue(result_1.actor_type() == result_2.actor_type() == "czł")
        self.assertTrue(result_1.name() == result_2.name() == "Patrycja Krowiowska")
        self.assertTrue(result_1.mission_deed() == ": ktośtam: jako łapczak")
        self.assertTrue(result_2.mission_deed() == ": ktośtam:, :łapczak")

    def test_with_well_formed_link_in_report(self):

        # Given

        valid_text = "- mag: [[[inwazja-karty-postaci-1601:siluria-diakon|Siluria Diakon]]]  jako mag KADEMu która podejmuje ogromne ryzyko by nikt nie zginął."

        # When

        result = create_single_dp_record(valid_text)

        # Then

        self.assertTrue(result.actor_type() == "mag")
        self.assertTrue(result.name() == "Siluria Diakon")
        self.assertTrue(result.mission_deed() == "mag KADEMu która podejmuje ogromne ryzyko by nikt nie zginął.")
