import unittest

from RdReportsReader.Src.Domain.Report.Metadata.Single.Create import create_single_report_metadata_from_raw_record


class TestReportMetadataToRdString(unittest.TestCase):

    def test_report_metadata_rdformat_from_zip_to_zip_no_extra_comments(self):

        # Given
        original_record = "* [[[inwazja-konspekty:120507-preludium-historia-swiata|001 - 120507 - Preludium: Historia świata]]]"
        metadata = create_single_report_metadata_from_raw_record(original_record)

        # When
        actual = metadata.to_rd_text()

        # Then
        self.assertTrue(actual == original_record, "RdRecord did not get formatted into the same string it was created from (no comments)")

    def test_report_metadata_rdformat_from_zip_to_zip_with_extra_comments(self):

        # Given
        original_record = "* [[[inwazja-konspekty:120507-preludium-historia-swiata|001 - 120507 - Preludium: Historia świata]]] some extra comments here "
        metadata = create_single_report_metadata_from_raw_record(original_record)

        # When
        actual = metadata.to_rd_text()

        # Then
        self.assertTrue(actual == original_record, "RdRecord did not get formatted into the same string it was created from (with comments)")
