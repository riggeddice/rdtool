import unittest

from RdReportsReader.Resources.TestData.MockSourceResources import load_described_report_resource
from RdReportsReader.Src.Infrastructure.Read.Provide.Report import provide_from_report_domain
from RdReportsReader.Src.Infrastructure.Read.Source.Mock.MockAggregatedSource import MockAggregatedSource


class TestReportCollectionGeneratesProfiles(unittest.TestCase):

    def test_proper_generation_C01M06Correct(self):

        # Given

        selected_source = MockAggregatedSource()
        selected_source.select_reports(load_described_report_resource("Collection", "C01M06_CorrectComplete"))

        report_collection = provide_from_report_domain(selected_source).collection()

        # When

        generated_profiles = report_collection.generate_profiles()

        # Then

        count = generated_profiles.count()

        self.assertTrue(generated_profiles is not None, "Nil while trying to generate profiles from ReportCollection")
        self.assertTrue(count == 12, "Expected 12 profiles to be generated, got: " + str(count))
