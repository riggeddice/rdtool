import unittest

from RdReportsReader.Resources.TestData.MockSourceResources import load_described_report_resource
from RdReportsReader.Src.Domain.Report.Masterlist.Complete.Structure.Prelude.Create import create_report_masterlist_prelude


class TestCreateReportMasterlistPrelude(unittest.TestCase):

    def test_with_NoHeader(self):

        # Given
        mlist_text = load_described_report_resource("Masterlist", "C01M06_correct")

        # When
        report_masterlist_header = create_report_masterlist_prelude(mlist_text)

        # Then
        self.assertTrue(report_masterlist_header is not None, "ReportMasterlistPrelude returned as None on mock source")

        retrieved = report_masterlist_header.to_rd_text()

        self.assertTrue(not retrieved, "ReportMasterlistPrelude does not contain no text")

    def test_with_Header(self):

        # Given
        mlist_text = load_described_report_resource("Masterlist", "C01M06_correct")

        header = "header"
        mlist_text = header + mlist_text

        # When
        report_masterlist_header = create_report_masterlist_prelude(mlist_text)

        # Then
        self.assertTrue(report_masterlist_header is not None, "ReportMasterlistPrelude returned as None on mock source")

        retrieved = report_masterlist_header.to_rd_text()

        self.assertTrue(header in retrieved, "ReportMasterlistPrelude does not contain added header")
