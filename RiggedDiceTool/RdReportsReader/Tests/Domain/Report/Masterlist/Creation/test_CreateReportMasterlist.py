import unittest

from RdReportsReader.Resources.TestData.MockSourceResources import load_described_report_resource
from RdReportsReader.Src.Domain.Report.Masterlist.Complete.Create import create_report_masterlist_from_text


class TestCreateReportMasterlist(unittest.TestCase):

    def test_with_C01_M06_Simple(self):

        # Given
        mlist_text = load_described_report_resource("Masterlist", "C01M06_correct")

        # When
        report_masterlist = create_report_masterlist_from_text(mlist_text)

        # Then
        self.assertTrue(report_masterlist is not None, "ReportMasterlist was not created at all")

        retrieved = report_masterlist.to_rd_text()

        self.assertTrue(retrieved in mlist_text, "ReportMasterlist does not match the initial records from which it was created (no nned for seqNo recalculation)")
