import unittest

from RdReportsReader.Resources.TestData.MockSourceResources import load_described_report_resource
from RdReportsReader.Src.Domain.Report.Masterlist.Complete.Structure.Tail.Create import create_report_masterlist_tail


class TestCreateReportMasterlistTail(unittest.TestCase):

    def test_with_Tail(self):

        # Given
        mlist_text = load_described_report_resource("Masterlist", "C01M06_correct")

        tail = "tail"
        mlist_text = mlist_text + tail

        # When
        report_masterlist_tail = create_report_masterlist_tail(mlist_text)

        # Then
        self.assertTrue(report_masterlist_tail is not None, "ReportMasterlistTail returned as None on mock source")

        retrieved = report_masterlist_tail.to_rd_text()

        self.assertTrue(tail in retrieved, "ReportMasterlistTail does not contain added tail")
