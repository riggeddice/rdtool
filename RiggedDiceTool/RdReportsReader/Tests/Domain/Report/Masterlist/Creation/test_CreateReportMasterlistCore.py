import unittest

from RdReportsReader.Resources.TestData.MockSourceResources import load_described_report_resource
from RdReportsReader.Src.Domain.Report.Masterlist.Complete.Structure.Core.Create import create_report_masterlist_core


class TestCreateReportMasterlistCore(unittest.TestCase):

    def test_with_C01_M06_Simple(self):

        # Given
        mlist_text = load_described_report_resource("Masterlist", "C01M06_correct")

        # When
        report_masterlist_core = create_report_masterlist_core(mlist_text)

        # Then
        self.assertTrue(report_masterlist_core is not None, "ReportMasterlistPrelude returned as None on mock source")

        retrieved = report_masterlist_core.to_rd_text()

        self.assertTrue(retrieved in mlist_text, "ReportMasterlistCore is not generated as subset of general masterlist.")
