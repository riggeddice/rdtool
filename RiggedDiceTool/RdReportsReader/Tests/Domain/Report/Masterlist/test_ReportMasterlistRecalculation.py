import unittest

from RdReportsReader.Resources.TestData.MockSourceResources import load_described_report_resource
from RdReportsReader.Src.Domain.Report.Masterlist.Complete.Create import create_report_masterlist_from_text


class TestReportMasterlistRecalculation(unittest.TestCase):

    def test_reportMasterlistRecalculateReportNumbersWorksForMock3MasterListImproperSeqNo(self):

        # Given
        mlist_text = load_described_report_resource("Masterlist", "C09M10_seqNoWrong")
        report_masterlist = create_report_masterlist_from_text(mlist_text)

        # When

        report_masterlist.correct_report_seq_numbers()

        # Then

        self.assertTrue(report_masterlist.report_number(1).sequence_number() == "001", "Recalculated sequenceNumber is wrong, expecting 1")
        self.assertTrue(report_masterlist.report_number(2).sequence_number() == "002", "Recalculated sequenceNumber is wrong, expecting 2")
        self.assertTrue(report_masterlist.report_number(3).sequence_number() == "003", "Recalculated sequenceNumber is wrong, expecting 3")

        text = report_masterlist.to_rd_text()

        amount_of_records = text.count("* [[[", 0, len(text))
        index_of_last = text.rfind("* [[[", 0, len(text))
        index_of_next_line = text.find("\n", index_of_last, len(text))

        interesting_line = text[index_of_last: index_of_next_line]
        delimiter_index = interesting_line.index("|")
        record_number_text = interesting_line[delimiter_index + 1: delimiter_index + 1 + 3]
        record_number = int(record_number_text)

        self.assertTrue(amount_of_records == record_number, "Recalculation did not properly set last record")
