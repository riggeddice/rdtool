import unittest

from RdReportsReader.Resources.TestData.MockSourceResources import load_described_report_resource
from RdReportsReader.Src.Domain.Location.Masterlist.Create import create_location_masterlist_from_report_collection
from RdReportsReader.Src.Infrastructure.Read.Provide.Report import provide_from_report_domain
from RdReportsReader.Src.Infrastructure.Read.Source.Mock.MockAggregatedSource import MockAggregatedSource


class TestLocationsMasterlistIsWellFormed(unittest.TestCase):

    def test_ignores_deeds__having_comma_delimiter(self):

        # Given

        selected_source = MockAggregatedSource()
        selected_source.select_reports(load_described_report_resource("Collection", "C01M03_MpwLocationsWithDeeds"))

        report_collection = provide_from_report_domain(selected_source).collection()
        generated_location_masterlist = create_location_masterlist_from_report_collection(report_collection)

        # When

        masterlist_text = generated_location_masterlist.to_rd_text()

        # Then

        slask_amount = masterlist_text.count(" # Śląsk")
        deed_text = "wykorzystywane jako dowodzenie"

        self.assertTrue(slask_amount == 2, "Śląsk record detected more than twice; probably Deed not extracted properly from LocationMasterlist.")
        self.assertTrue(deed_text not in masterlist_text, "Location deed detected in masterlist_text (should not be present, as it is a part of Single Location).")
