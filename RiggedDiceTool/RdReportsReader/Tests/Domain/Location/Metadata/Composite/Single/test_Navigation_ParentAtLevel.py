import unittest

from RdReportsReader.Src.Domain.Location.Metadata.SingleCompositeRecord.Create import create_single_location_metadata_composite_record_from_text_block


class TestNavigationParentAtLevel(unittest.TestCase):

    def test_returns_parent__flat_record_different_levels(self):

        # Given

        line_block = "# R0\n # R1\n  # R2\n   # R3\n    # R4\n     # R5\n      # R6".split("\n")
        composite_result = create_single_location_metadata_composite_record_from_text_block(line_block)

        node_r6 = composite_result.root().children()[0].children()[0].children()[0].children()[0].children()[0].children()[0].children()[0]

        expected_parent_at_depth_1 = composite_result.root().children()[0].children()[0]
        expected_parent_at_depth_1_name = expected_parent_at_depth_1.value().normalized_name()

        expected_parent_at_depth_4 = composite_result.root().children()[0].children()[0].children()[0].children()[0].children()[0]
        expected_parent_at_depth_4_name = expected_parent_at_depth_4.value().normalized_name()

        # When

        actual_parent_at_depth_1 = node_r6.parent_at_level_counted_from_root(1)
        actual_parent_at_depth_4 = node_r6.parent_at_level_counted_from_root(4)

        # Then

        actual_parent_at_depth_1_name = actual_parent_at_depth_1.value().normalized_name()
        actual_parent_at_depth_4_name = actual_parent_at_depth_4.value().normalized_name()

        self.assertTrue(actual_parent_at_depth_1_name == expected_parent_at_depth_1_name, "Names at depth 1 don't match")
        self.assertTrue(actual_parent_at_depth_4_name == expected_parent_at_depth_4_name, "Names at depth 3 don't match")
