import unittest

from RdReportsReader.Src.Domain.Location.Metadata.SingleCompositeRecord.Create import create_single_location_metadata_composite_record_from_text_block


class TestGetPathFromRoot(unittest.TestCase):

    def test_proper_file_path__for_deep_node(self):

        # Given

        line_block = "# Świat\n # Śląsk\n  # Kopalin\n   # DistrictA\n    # Prime\n    # Secundus\n   # DistrictB".split("\n")
        composite_result = create_single_location_metadata_composite_record_from_text_block(line_block)

        deep_node = composite_result.children()[0].children()[0].children()[0].children()[0].children()[0]
        expected_path = "Świat_Śląsk_Kopalin_Districta_Prime"

        # When

        actual_path = deep_node.path_from_root()

        # Then

        self.assertTrue(actual_path == expected_path, "Expected path from root != actual path from root; we got: " + actual_path)
