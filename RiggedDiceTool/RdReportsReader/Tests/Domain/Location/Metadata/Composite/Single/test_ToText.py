import unittest

from RdReportsReader.Src.Config.Providers.ProvideInternalTokens import ProvideInternalTokens
from RdReportsReader.Src.Domain.Location.Metadata.SingleCompositeRecord.Create import create_single_location_metadata_composite_record_from_text_block


class TestToText(unittest.TestCase):

    def test_no_set_depth_text_saves_what_is_loaded(self):

        # Given

        record_to_load = "# Świat\n # Primus\n  # Śląsk\n   # Powiat Kopaliński\n    # Kopalin\n     # Anything"
        composite_result = create_single_location_metadata_composite_record_from_text_block(record_to_load.split("\n"))

        # When

        actual = composite_result.to_rd_text()

        # Then

        self.assertTrue(record_to_load in actual)

    def test_set_depth_to_city_depth_text_saves_only_up_to_city(self):

        # Given

        up_to_city = "# Świat\n # Primus\n  # Śląsk\n   # Powiat Kopaliński\n    # Kopalin"
        past_city = "\n     # Anything"

        city_depth = ProvideInternalTokens().location_city_depth()

        record_to_load = up_to_city + past_city
        composite_result = create_single_location_metadata_composite_record_from_text_block(record_to_load.split("\n"))

        # When

        actual = composite_result.to_rd_text(city_depth)

        # Then

        self.assertTrue(record_to_load not in actual)
        self.assertTrue(up_to_city in actual)
