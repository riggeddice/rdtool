import unittest

from RdReportsReader.Src.Domain.Location.Metadata.CompositeRecordCollection.Action.Merge import merge_into_single_composite_record
from RdReportsReader.Src.Domain.Location.Metadata.RecordValueCollection.LocationRecordValueCollection import LocationRecordValueCollection
from RdReportsReader.Src.Domain.Location.Metadata.SingleCompositeRecord.Create import create_single_location_metadata_composite_record_from_text_block
from RdReportsReader.Src.Domain.Location.Metadata.SingleCompositeRecord.SingleLocationMetadataCompositeRecord import SingleLocationMetadataCompositeRecord


class TestGetFlatListFromLocationCompositeRecords(unittest.TestCase):

    def test_flat_list_has_proper_amount_of_composite_records_easy_line_block__composite_records(self):

        # Given

        line_block = "# Świat\n # Śląsk\n  # Kopalin\n   # DistrictA\n    # Prime\n    # Secundus\n   # DistrictB".split("\n")
        composite_result = create_single_location_metadata_composite_record_from_text_block(line_block)

        # When

        flat_list = composite_result.list_of_all_single_composite_records()

        # Then

        self.assertTrue(len(flat_list) == 7, "The amount of value records is not as expected; we received: " + str(len(flat_list)))
        self.assertTrue(type(flat_list[0]) is SingleLocationMetadataCompositeRecord, "We expected SingleLocationMetadataCompositeRecord and got something else")

    def test_flat_list_has_proper_amount_of_composite_records_inverted_line_block_centrum_centrum__composite_records(self):

        # Given

        line_block = "# Świat\n # Śląsk\n  # Kopalin\n   # Centrum\n    # Prime\n    # Obrzeza\n   # DistrictB\n  # Bazantow\n   # Centrum".split("\n")
        composite_result = create_single_location_metadata_composite_record_from_text_block(line_block)

        # When

        flat_list = composite_result.list_of_all_single_composite_records()

        # Then

        self.assertTrue(len(flat_list) == 9, "The amount of value records is not as expected; we received: " + str(len(flat_list)))
        self.assertTrue(type(flat_list[0]) is SingleLocationMetadataCompositeRecord, "We expected SingleLocationMetadataCompositeRecord and got something else")

    def test_flat_list_has_proper_amount_of_value_records_easy_line_block__value_collections(self):

        # Given

        line_block = "# Świat\n # Śląsk\n  # Kopalin\n   # DistrictA\n    # Prime\n    # Secundus\n   # DistrictB".split("\n")
        composite_result = create_single_location_metadata_composite_record_from_text_block(line_block)

        # When

        flat_list = composite_result.list_of_all_value_collections()

        # Then

        self.assertTrue(len(flat_list) == 7, "The amount of value records is not as expected; we received: " + str(len(flat_list)))
        self.assertTrue(type(flat_list[0]) is LocationRecordValueCollection, "We expected LocationRecordValueCollection and got something else")

    def test_flat_list_has_proper_amount_of_value_records_inverted_line_block_centrum_centrum__value_collections(self):

        # Given

        line_block = "# Świat\n # Śląsk\n  # Kopalin\n   # Centrum\n    # Prime\n    # Obrzeza\n   # DistrictB\n  # Bazantow\n   # Centrum".split("\n")
        composite_result = create_single_location_metadata_composite_record_from_text_block(line_block)

        # When

        flat_list = composite_result.list_of_all_value_collections()

        # Then

        self.assertTrue(len(flat_list) == 9, "The amount of value records is not as expected; we received: " + str(len(flat_list)))
        self.assertTrue(type(flat_list[0]) is LocationRecordValueCollection, "We expected LocationRecordValueCollection and got something else")

    def test_flat_list_has_proper_amount_of_composite_records_after_merge__composite_records(self):

        # Given

        line_block_1 = "# Świat, from 1\n # Śląsk, from 1\n  # Kopalin, from 1".split("\n")
        line_block_2 = "# Świat, from 2\n # Śląsk, from 2\n  # Kopalin, from 2\n   # DistrictA, from 2".split("\n")

        composite_1 = create_single_location_metadata_composite_record_from_text_block(line_block_1)
        composite_2 = create_single_location_metadata_composite_record_from_text_block(line_block_2)
        composites = [composite_1, composite_2]

        flat_list_1 = composite_1.list_of_all_single_composite_records()
        flat_list_2 = composite_2.list_of_all_single_composite_records()

        merged = merge_into_single_composite_record(composites)

        # When

        flat_list_merged = merged.list_of_all_single_composite_records()

        # Then

        self.assertTrue(len(flat_list_1) == 3, "The amount of records is not as expected; we received: " + str(len(flat_list_1)))
        self.assertTrue(len(flat_list_2) == 4, "The amount of records is not as expected; we received: " + str(len(flat_list_2)))
        self.assertTrue(len(flat_list_merged) == 4, "The amount of records is not as expected; we received: " + str(len(flat_list_merged)))

    def test_flat_list_has_proper_amount_of_value_records_after_merge__composite_records(self):

        # Given

        line_block_1 =  "# Świat, from 1\n # Śląsk, from 1\n  # Kopalin, from 1".split("\n")
        line_block_2 =  "# Świat, from 2\n # Śląsk, from 2\n  # Kopalin, from 2\n   # DistrictA, from 2".split("\n")

        composite_1 = create_single_location_metadata_composite_record_from_text_block(line_block_1)
        composite_2 = create_single_location_metadata_composite_record_from_text_block(line_block_2)
        composites = [composite_1, composite_2]

        flat_list_1 = composite_1.list_of_all_value_collections()
        flat_list_2 = composite_2.list_of_all_value_collections()

        merged = merge_into_single_composite_record(composites)

        # When

        flat_list_merged = merged.list_of_all_value_collections()

        # Then

        self.assertTrue(len(flat_list_1) == 3, "The amount of records is not as expected; we received: " + str(len(flat_list_1)))
        self.assertTrue(len(flat_list_2) == 4, "The amount of records is not as expected; we received: " + str(len(flat_list_2)))
        self.assertTrue(len(flat_list_merged) == 4, "The amount of records is not as expected; we received: " + str(len(flat_list_merged)))
