import unittest

from RdReportsReader.Src.Domain.Location.Metadata.CompositeRecordCollection.Action.Merge import merge_into_single_composite_record
from RdReportsReader.Src.Domain.Location.Metadata.SingleCompositeRecord.Create import create_single_location_metadata_composite_record_from_text_block


class TestMergeCompositeRecord__ValueCollection(unittest.TestCase):

    def test_if_properly_works_depth_3_flat_line_tree_2_identical_records(self):

        line_block_1 = "# Świat, from1\n # Śląsk, from1\n  # Kopalin, from1".split("\n")
        line_block_2 = "# Świat, from2\n # Śląsk, from2\n  # Kopalin, from2".split("\n")

        record_1 = create_single_location_metadata_composite_record_from_text_block(line_block_1)
        record_2 = create_single_location_metadata_composite_record_from_text_block(line_block_2)
        records = [record_1, record_2]

        merged_record = merge_into_single_composite_record(records)

        all_nodes = merged_record.list_of_all_single_composite_records()

        swiat_single_record_values = all_nodes[0].value().record_values()
        slask_single_record_values = all_nodes[1].value().record_values()
        kopalin_single_record_values = all_nodes[2].value().record_values()

        self.assertTrue(len(all_nodes) == 3, "We should receive 3 nodes and we got: " + str(len(all_nodes)))
        self.assertTrue(len(swiat_single_record_values) == 2, "We should receive 2 value records and we got: " + str(len(swiat_single_record_values)))
        self.assertTrue(len(slask_single_record_values) == 2, "We should receive 2 value records and we got: " + str(len(slask_single_record_values)))
        self.assertTrue(len(kopalin_single_record_values) == 2, "We should receive 2 value records and we got: " + str(len(kopalin_single_record_values)))
