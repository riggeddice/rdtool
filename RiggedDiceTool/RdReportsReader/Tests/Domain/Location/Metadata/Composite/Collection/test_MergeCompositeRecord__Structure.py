import unittest

from RdReportsReader.Src.Domain.Location.Metadata.CompositeRecordCollection.Action.Merge import merge_into_single_composite_record
from RdReportsReader.Src.Domain.Location.Metadata.SingleCompositeRecord.Create import create_single_location_metadata_composite_record_from_text_block


class TestMergeCompositeRecord__Structure(unittest.TestCase):

    def test_canMergeTwoSimpleMetadataCollections(self):

        # Given

        line_block_1 =  "# Świat\n # Śląsk\n  # Kopalin\n   # DistrictA\n    # Prime\n    # Secundus\n   # DistrictB".split("\n")
        line_block_2 = "# Świat\n # Śląsk\n  # Kopalin\n   # DistrictA\n    # Prime\n   # DistrictB\n    # Tertias".split("\n")

        composite_1 = create_single_location_metadata_composite_record_from_text_block(line_block_1)
        composite_2 = create_single_location_metadata_composite_record_from_text_block(line_block_2)

        composite_collection = [composite_1, composite_2]

        expected_result = "# Świat\n # Śląsk\n  # Kopalin\n   # DistrictA\n    # Prime\n    # Secundus\n   # DistrictB\n    # Tertias".split("\n")
        expected_composite = create_single_location_metadata_composite_record_from_text_block(expected_result)

        # When

        actual_composite = merge_into_single_composite_record(composite_collection)

        # Then

        root_contains_1_node = len(actual_composite.root().children()) == len(expected_composite.root().children())
        dis_a_contains_2_nodes = len(actual_composite.root().children()[0].children()[0].children()[0].children()[0].children()) == len(expected_composite.root().children()[0].children()[0].children()[0].children()[0].children())
        dis_b_contains_1_node = len(actual_composite.root().children()[0].children()[0].children()[0].children()[1].children()) == len(expected_composite.root().children()[0].children()[0].children()[0].children()[1].children())
        dis_a_contains_prime = actual_composite.root().children()[0].children()[0].children()[0].children()[0].children()[0].normalized_name() == expected_composite.root().children()[0].children()[0].children()[0].children()[0].children()[0].normalized_name()
        dis_a_contains_secondus = actual_composite.root().children()[0].children()[0].children()[0].children()[0].children()[1].normalized_name() == expected_composite.root().children()[0].children()[0].children()[0].children()[0].children()[1].normalized_name()
        dis_b_contains_tertias = actual_composite.root().children()[0].children()[0].children()[0].children()[1].children()[0].normalized_name() == expected_composite.root().children()[0].children()[0].children()[0].children()[1].children()[0].normalized_name()

        self.assertTrue(root_contains_1_node, "Composite root contains more than only the 'Kopalin' node; does not match reference collection")
        self.assertTrue(dis_a_contains_2_nodes, "DistrictA does not contain 2 nodes (expected: 'Prime, Secundus'; does not match reference collection")
        self.assertTrue(dis_b_contains_1_node, "DistrictB does not contain 1 node (expected: 'Tertias'; does not match reference collection")
        self.assertTrue(dis_a_contains_prime, "DistrictA does not contain 'Prime' node with appropriate name; does not match reference collection")
        self.assertTrue(dis_a_contains_secondus, "DistrictA does not contain 'Secundus' node with appropriate name; does not match reference collection")
        self.assertTrue(dis_b_contains_tertias, "DistrictB does not contain 'Tertias' node with appropriate name; does not match reference collection")

    def test_merge_properly_ignoring_case(self):

        # Given

        line_block_1 =  "# Świat\n # Śląsk\n  # Kopalin".split("\n")
        line_block_2 = "# Świat\n # śląsk\n  # Myślin".split("\n")

        composite_1 = create_single_location_metadata_composite_record_from_text_block(line_block_1)
        composite_2 = create_single_location_metadata_composite_record_from_text_block(line_block_2)

        composite_collection = [composite_1, composite_2]

        expected_result = "# Świat\n # Śląsk\n  # Kopalin\n  # Myślin".split("\n")
        expected_composite = create_single_location_metadata_composite_record_from_text_block(expected_result)

        # When

        actual_composite = merge_into_single_composite_record(composite_collection)

        # Then

        should_have_1_1_structure = len(actual_composite.root().children()[0].children()) == len(expected_composite.root().children()[0].children())
        should_have_1_1_2_structure = len(actual_composite.root().children()[0].children()[0].children()) == len(expected_composite.root().children()[0].children()[0].children())

        self.assertTrue(should_have_1_1_structure, "Probably merging is case-sensitive (while we expect ignoring case); not '1' at 'Śląsk' level: " +
                        actual_composite.root().children()[0].children()[0].normalized_name())
        self.assertTrue(should_have_1_1_2_structure, "Probably merging is case-sensitive (while we expect ignoring case); not '2' at 'Kopalin/Myślin' level: " +
                        actual_composite.root().children()[0].children()[0].children()[0].normalized_name())
