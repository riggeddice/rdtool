import unittest

from RdReportsReader.Src.Domain.Location.Metadata.CompositeRecordCollection.Action.Merge import merge_into_single_composite_record
from RdReportsReader.Src.Domain.Location.Metadata.SingleCompositeRecord.Create import create_single_location_metadata_composite_record_from_text_block


class TestLocationMetadataValueCollection__ToRdString(unittest.TestCase):

    def test_single_empty_record_for_masterlist(self):

        # Given

        line_block =  "# Świat\n # Śląsk\n  # Kopalin\n   # DistrictA\n    # Prime\n    # Secundus\n   # DistrictB".split("\n")
        composite_result = create_single_location_metadata_composite_record_from_text_block(line_block)
        value_collection = composite_result.root().children()[0].children()[0].children()[0].value()

        self.assertTrue(value_collection.normalized_name() == "Kopalin", "Got a wrong record because of a wrong amount of children()[0].")

        # When

        actual_text = value_collection.to_rd_text_for_masterlist(2)

        # Then

        expected_text = "  # Kopalin"

        self.assertTrue(actual_text == expected_text, "Actual result of a ValueCollection.to_rd_text_for_masterlist was: " + actual_text)

    def test_single_filled_record_for_masterlist(self):

        # Given

        line_block =  "# Świat\n # Śląsk\n  # Kopalin, not an empty string\n   # DistrictA\n    # Prime\n    # Secundus\n   # DistrictB".split("\n")
        composite_result = create_single_location_metadata_composite_record_from_text_block(line_block)
        value_collection = composite_result.root().children()[0].children()[0].children()[0].value()

        self.assertTrue(value_collection.normalized_name() == "Kopalin", "Got a wrong record because of a wrong amount of children()[0].")

        # When

        actual_text = value_collection.to_rd_text_for_masterlist(2)

        # Then

        expected_text = "  # Kopalin"

        self.assertTrue(actual_text == expected_text, "Actual result of a ValueCollection.to_rd_text_for_masterlist was: " + actual_text)

    def test_single_empty_record_for_descriptor(self):

        # Given

        line_block =  "# Świat\n # Śląsk\n  # Kopalin\n   # DistrictA\n    # Prime\n    # Secundus\n   # DistrictB".split("\n")
        composite_result = create_single_location_metadata_composite_record_from_text_block(line_block)
        value_collection = composite_result.root().children()[0].children()[0].children()[0].value()

        self.assertTrue(value_collection.normalized_name() == "Kopalin", "Got a wrong record because of a wrong amount of children()[0].")

        # When

        actual_text = value_collection.to_rd_text_for_descriptor()

        # Then

        expected_text = ""

        self.assertTrue(actual_text == expected_text, "Actual result of a ValueCollection.to_rd_text_for_descriptor was: " + actual_text)

    def test_single_filled_record_for_descriptor(self):

        # Given

        line_block =  "# Świat\n # Śląsk\n  # Kopalin, not an empty string\n   # DistrictA\n    # Prime\n    # Secundus\n   # DistrictB".split("\n")
        composite_result = create_single_location_metadata_composite_record_from_text_block(line_block)
        value_collection = composite_result.root().children()[0].children()[0].children()[0].value()

        self.assertTrue(value_collection.normalized_name() == "Kopalin", "Got a wrong record because of a wrong amount of children()[0].")

        # When

        actual_text = value_collection.to_rd_text_for_descriptor()

        # Then

        expected_text = "not an empty string"

        self.assertTrue(actual_text == expected_text, "Actual result of a ValueCollection.to_rd_text_for_descriptor was: " + actual_text)

    def test_three_records_for_masterlist(self):

        # Given

        line_block_1 =  "# Świat\n # Śląsk\n  # Kopalin, from record 1\n   # DistrictA".split("\n")
        line_block_2 =  "# Świat\n # Śląsk\n  # kopalin, from record 2\n   # DistrictA".split("\n")
        line_block_3 =  "# Świat\n # Śląsk\n  # Kopalin, from record 3\n   # DistrictA".split("\n")

        composite_result_1 = create_single_location_metadata_composite_record_from_text_block(line_block_1)
        composite_result_2 = create_single_location_metadata_composite_record_from_text_block(line_block_2)
        composite_result_3 = create_single_location_metadata_composite_record_from_text_block(line_block_3)
        composites = [composite_result_1, composite_result_2, composite_result_3]

        merged = merge_into_single_composite_record(composites)

        value_collection = merged.root().children()[0].children()[0].children()[0].value()

        self.assertTrue(value_collection.normalized_name() == "Kopalin", "Got a wrong record because of a wrong amount of children()[0].")

        # When

        actual_text = value_collection.to_rd_text_for_masterlist(2)

        # Then

        expected_text = "  # Kopalin"

        self.assertTrue(actual_text == expected_text, "Actual result of a ValueCollection.to_rd_text_for_masterlist was: " + actual_text)

    def test_three_records_for_descriptor(self):

        # Given

        line_block_1 =  "# Świat\n # Śląsk\n  # Kopalin, from record 1\n   # DistrictA".split("\n")
        line_block_2 =  "# Świat\n # Śląsk\n  # kopalin, from record 2\n   # DistrictA".split("\n")
        line_block_3 =  "# Świat\n # Śląsk\n  # Kopalin, from record 3\n   # DistrictA".split("\n")

        composite_result_1 = create_single_location_metadata_composite_record_from_text_block(line_block_1)
        composite_result_2 = create_single_location_metadata_composite_record_from_text_block(line_block_2)
        composite_result_3 = create_single_location_metadata_composite_record_from_text_block(line_block_3)
        composites = [composite_result_1, composite_result_2, composite_result_3]

        merged = merge_into_single_composite_record(composites)

        value_collection = merged.root().children()[0].children()[0].children()[0].value()

        self.assertTrue(value_collection.normalized_name() == "Kopalin", "Got a wrong record because of a wrong amount of children()[0].")

        # When

        actual_text = value_collection.to_rd_text_for_descriptor()

        # Then

        expected_text = "from record 1\nfrom record 2\nfrom record 3"

        self.assertTrue(actual_text == expected_text, "Actual result of a ValueCollection.to_rd_text_for_descriptor was: " + actual_text)
