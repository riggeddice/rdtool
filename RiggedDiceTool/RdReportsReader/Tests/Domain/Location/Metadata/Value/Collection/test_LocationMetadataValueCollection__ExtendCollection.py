import unittest

from RdReportsReader.Src.Domain.Location.Metadata.RecordValueCollection.Create import create_location_record_value_collection
from RdReportsReader.Src.Domain.Location.Metadata.SingleRecordValue.Create import create_location_metadata_record_value


class TestLocationMetadataValueCollection__ExtendCollection(unittest.TestCase):

    def test_basic_extending_mechanisms_work(self):

        # Given

        depth = "  # "
        city_name = "Kopalin"
        separator = ", "
        deed_1 = "from record 1-1"
        deed_2 = "from record 1-2"
        deed_3 = "from record 2-1"
        deed_4 = "from record 2-2"

        collection_1 = create_location_record_value_collection(depth + city_name + separator + deed_1)
        collection_1_2 = create_location_record_value_collection(depth + city_name + separator + deed_2)

        # When

        collection_1.extend_record_value_collection(collection_1_2)

        # Then

        collection_1_size = len(collection_1.record_values())
        self.assertTrue(collection_1_size == 2, "Wrong amount of records in collection after extending (1+1):" + str(collection_1_size))

        # Given

        collection_2 = create_location_record_value_collection(depth + city_name + separator + deed_3)
        value_2_2 = create_location_metadata_record_value(depth + city_name + separator + deed_4)

        # When

        collection_2.append_record_value(value_2_2)

        # Then

        collection_2_size = len(collection_2.record_values())
        self.assertTrue(collection_2_size == 2, "Wrong amount of records in collection after appending value (1+1):" + str(collection_2_size))

        # When

        collection_1.extend_record_value_collection(collection_2)

        # Then

        collection_1_final_size = len(collection_1.record_values())
        self.assertTrue(collection_1_final_size == 4, "Wrong amount of records in collection after extending (2+2):" + str(collection_1_final_size))

        actual_name = collection_1.normalized_name()
        deed_from_record_1 = collection_1.record_values()[0].deed()
        deed_from_record_2 = collection_1.record_values()[1].deed()
        deed_from_record_3 = collection_1.record_values()[2].deed()
        deed_from_record_4 = collection_1.record_values()[3].deed()

        self.assertTrue(city_name in actual_name, "Normalized name does not match city name: " + actual_name)
        self.assertTrue(deed_1 in deed_from_record_1, "First value record not in collection")
        self.assertTrue(deed_2 in deed_from_record_2, "Second value record not in collection")
        self.assertTrue(deed_3 in deed_from_record_3, "Third value record not in collection")
        self.assertTrue(deed_4 in deed_from_record_4, "Fourth value record not in collection")
