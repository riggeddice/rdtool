import unittest

from RdReportsReader.Resources.TestData.MockSourceResources import load_described_profile_resource
from RdReportsReader.Src.Domain.CharacterProfile.Masterlist.Structure.Prelude.Create import create_profile_masterlist_prelude


class TestCreateProfileMasterlistPrelude(unittest.TestCase):

    def test_with_no_header(self):

        # Given
        mlist_text = load_described_profile_resource("Masterlist", "F03S04P08_Simple")

        # When
        report_masterlist_prelude = create_profile_masterlist_prelude(mlist_text)

        # Then
        self.assertTrue(report_masterlist_prelude is not None, "ProfileMasterlistPrelude returned as None on mock source")

        retrieved = report_masterlist_prelude.to_rd_text()

        self.assertTrue(not retrieved, "Empty ProfileMasterlistPrelude contains text (while it shouldn't).")

    def test_with_header(self):

        # Given
        mlist_text = load_described_profile_resource("Masterlist", "F03S04P08_Simple")

        header = "Header:"
        mlist_text_with_header = header + mlist_text

        # When
        report_masterlist_prelude = create_profile_masterlist_prelude(mlist_text_with_header)

        # Then
        self.assertTrue(report_masterlist_prelude is not None, "ProfileMasterlistPrelude returned as None on mock source")

        retrieved = report_masterlist_prelude.to_rd_text()

        self.assertTrue(retrieved is not None, "ProfileMasterlistPrelude does not contain any text.")
        self.assertTrue(header in retrieved, "ProfileMasterlistPrelude does not contain header.")
