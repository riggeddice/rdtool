import unittest

from RdReportsReader.Resources.TestData.MockSourceResources import load_described_profile_resource
from RdReportsReader.Src.Domain.CharacterProfile.Masterlist.Structure.Tail.Create import create_profile_masterlist_tail


class TestCreateProfileMasterlistTail(unittest.TestCase):

    def test_with_tail(self):

        # Given
        mlist_text = load_described_profile_resource("Masterlist", "F03S04P08_Simple")

        tail = ", Tail"
        mlist_text_with_tail = mlist_text + tail

        # When
        report_masterlist_tail = create_profile_masterlist_tail(mlist_text_with_tail)

        # Then
        self.assertTrue(report_masterlist_tail is not None, "ProfileMasterlistTail returned as None on mock source")

        retrieved = report_masterlist_tail.to_rd_text()

        self.assertTrue(retrieved is not None, "ProfileMasterlistTail does not contain any text.")
        self.assertTrue(tail in retrieved, "ProfileMasterlistTail does not contain tail.")
