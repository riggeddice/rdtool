import unittest

from RdReportsReader.Resources.TestData.MockSourceResources import load_described_profile_resource
from RdReportsReader.Src.Domain.CharacterProfile.Masterlist.Create import create_profile_masterlist_from_text


class TestCreateProfileMasterlist(unittest.TestCase):

    def test_with__F03S04P08_Simple(self):

        # Given
        mlist_text = load_described_profile_resource("Masterlist", "F03S04P08_Simple")

        # When
        report_masterlist = create_profile_masterlist_from_text(mlist_text)

        # Then
        self.assertTrue(report_masterlist is not None, "ProfileMasterlistCore returned as None on mock source")
