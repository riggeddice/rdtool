import unittest

from RdReportsReader.Resources.TestData.MockSourceResources import load_described_profile_resource
from RdReportsReader.Src.Domain.CharacterProfile.Masterlist.Structure.Core.Create import create_profile_masterlist_core


class TestCreateProfileMasterlistCore(unittest.TestCase):

    def test_with__F03S04P08_Simple(self):

        # Given
        mlist_text = load_described_profile_resource("Masterlist", "F03S04P08_Simple")

        # When
        profile_masterlist_core = create_profile_masterlist_core(mlist_text)

        # Then
        self.assertTrue(profile_masterlist_core is not None, "ProfileMasterlistCore returned as None on mock source")

        metadata_count = profile_masterlist_core.metadata_count()

        self.assertTrue(metadata_count == 8, "ProfileMasterlistCore does not contain 8 profiles (as expected with the list)")
