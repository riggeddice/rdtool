import unittest

from RdReportsReader.Resources.TestData.MockSourceResources import load_described_report_resource
from RdReportsReader.Resources.TestData.MockSourceResources import load_described_profile_resource
from RdReportsReader.Src.Domain.CharacterProfile.Complete.Collection.Create import create_complete_profile_collection
from RdReportsReader.Src.Infrastructure.Read.Provide.Profile import provide_from_profile_domain
from RdReportsReader.Src.Infrastructure.Read.Provide.Report import provide_from_report_domain
from RdReportsReader.Src.Infrastructure.Read.Source.Mock.MockAggregatedSource import MockAggregatedSource


class TestSingleProfile_ToRdText(unittest.TestCase):

    def test_andrea_has_name_and_it_does_not_become_a_non_name_section(self):

        # Given

        selected_source = MockAggregatedSource()
        selected_source.select_reports(load_described_report_resource("Collection", "C01M06_CorrectComplete"))
        selected_source.select_profiles(load_described_profile_resource("Collection", "F03S04P08_SimpleComplete"))

        reports = provide_from_report_domain(selected_source).collection()

        profile_provider = provide_from_profile_domain(selected_source)
        read_profiles = profile_provider.collection()
        profile_masterlist = profile_provider.masterlist()

        generated_profiles = reports.generate_profiles()

        complete_profile_collection = create_complete_profile_collection(read_profiles, generated_profiles, profile_masterlist)

        andrea_profile = complete_profile_collection.profile_having_name("Andrea Wilgacz")

        potential_anomaly_1 = "+ +++"
        potential_anomaly_2 = "+++ +"
        expected_proper_name = "+ Andrea Wilgacz"

        # When

        profile_text = andrea_profile.to_rd_text()

        # Then

        self.assertTrue(potential_anomaly_1 not in profile_text, "Anomaly detected: '+ +++' before name")
        self.assertTrue(potential_anomaly_2 not in profile_text, "Anomaly detected: '+++ +' before name")
        self.assertTrue(expected_proper_name in profile_text, "Name not present according to wikidot rules in profile")
