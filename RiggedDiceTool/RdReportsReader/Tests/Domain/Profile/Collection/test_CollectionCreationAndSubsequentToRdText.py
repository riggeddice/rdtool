import unittest

from RdReportsReader.Resources.TestData.MockSourceResources import load_described_profile_resource
from RdReportsReader.Resources.TestData.MockSourceResources import load_described_report_resource
from RdReportsReader.Src.Domain.CharacterProfile.Complete.Collection.Create import create_complete_profile_collection
from RdReportsReader.Src.Infrastructure.Read.Provide.Profile import provide_from_profile_domain
from RdReportsReader.Src.Infrastructure.Read.Provide.Report import provide_from_report_domain
from RdReportsReader.Src.Infrastructure.Read.Source.Mock.MockAggregatedSource import MockAggregatedSource


class TestCollectionCreationAndSubsequentToRdText(unittest.TestCase):

    def test_mergeReadAndGenerated_onMocks(self):

        # Given

        selected_source = MockAggregatedSource()
        selected_source.select_reports(load_described_report_resource("Collection", "C01M06_CorrectComplete"))
        selected_source.select_profiles(load_described_profile_resource("Collection", "F03S04P08_SimpleComplete"))

        reports = provide_from_report_domain(selected_source).collection()

        profile_provider = provide_from_profile_domain(selected_source)
        read_profiles = profile_provider.collection()
        profile_masterlist = profile_provider.masterlist()

        generated_profiles = reports.generate_profiles()

        # When

        complete_profile_collection = create_complete_profile_collection(read_profiles, generated_profiles, profile_masterlist)

        # Then

        self.assertTrue(complete_profile_collection is not None, "CompleteProfileCollection does not exist at all (null)")
        self.assertTrue(complete_profile_collection.count() == 15, "Expected CompleteProfileCollection to have 15 elements, it has: " + str(complete_profile_collection.count()))

    def test_afterMergeTextValueForSingleProfileContainsMechanicsHistoryName_onMocks(self):

        # Given

        selected_source = MockAggregatedSource()
        selected_source.select_reports(load_described_report_resource("Collection", "C01M06_CorrectComplete"))
        selected_source.select_profiles(load_described_profile_resource("Collection", "F03S04P08_SimpleComplete"))

        name_part_contains = "Andrea"
        history_part_contains = "+++ Relacje z postaciami:"
        mechanics_part_contains = "Jaka jest:"

        reports = provide_from_report_domain(selected_source).collection()

        profile_provider = provide_from_profile_domain(selected_source)
        read_profiles = profile_provider.collection()
        profile_masterlist = profile_provider.masterlist()

        generated_profiles = reports.generate_profiles()
        complete_profile_collection = create_complete_profile_collection(read_profiles, generated_profiles, profile_masterlist)
        single_profile = complete_profile_collection.profile_at_index(0)

        # When

        text_value = single_profile.to_rd_text()

        # Then

        contains_name = name_part_contains in text_value
        contains_history = history_part_contains in text_value
        contains_mechanics = mechanics_part_contains in text_value

        self.assertTrue(contains_name, "The first profile does not contain the name (expected: 'Andromeda')")
        self.assertTrue(contains_history, "The first profile does not contain the history (expected: '+++ Relacje')")
        self.assertTrue(contains_mechanics, "The first profile does not contain the mechanics (expected: 'Jaka jest')")

    def test_afterMergeWorksWithoutHistory_testDracena_onMocks(self):

        # Given

        selected_source = MockAggregatedSource()
        selected_source.select_reports(load_described_report_resource("Collection", "C01M06_CorrectComplete"))
        selected_source.select_profiles(load_described_profile_resource("Collection", "F03S04P08_SimpleComplete"))

        name_part_contains = "Andrea Wilgacz"
        mechanics_part_contains = "Jaka jest"

        reports = provide_from_report_domain(selected_source).collection()

        profile_provider = provide_from_profile_domain(selected_source)
        read_profiles = profile_provider.collection()
        profile_masterlist = profile_provider.masterlist()

        generated_profiles = reports.generate_profiles()
        complete_profile_collection = create_complete_profile_collection(read_profiles, generated_profiles, profile_masterlist)
        single_profile = complete_profile_collection.profile_having_name("Andrea Wilgacz")

        # When

        text_value = single_profile.to_rd_text()

        # Then

        contains_name = name_part_contains in text_value
        contains_mechanics = mechanics_part_contains in text_value

        self.assertTrue(contains_name, "Andrea's profile does not contain the name (expected: 'Andrea')")
        self.assertTrue(contains_mechanics, "Andrea's profile does not contain the mechanics (expected: 'Jaka jest')")

    def test_afterMergeWorksWithoutMechanics_testGermont_onMocks(self):

        # Given

        selected_source = MockAggregatedSource()
        selected_source.select_reports(load_described_report_resource("Collection", "C01M06_CorrectComplete"))
        selected_source.select_profiles(load_described_profile_resource("Collection", "F03S04P08_SimpleComplete"))

        name_part_contains = "Artur"
        history_part_contains = "+++ Relacje z postaciami:"

        reports = provide_from_report_domain(selected_source).collection()

        profile_provider = provide_from_profile_domain(selected_source)
        read_profiles = profile_provider.collection()
        profile_masterlist = profile_provider.masterlist()

        generated_profiles = reports.generate_profiles()
        complete_profile_collection = create_complete_profile_collection(read_profiles, generated_profiles, profile_masterlist)
        single_profile = complete_profile_collection.profile_having_name("Artur Szmelc")

        # When

        text_value = single_profile.to_rd_text()

        # Then

        contains_name = name_part_contains in text_value
        contains_history = history_part_contains in text_value

        self.assertTrue(contains_name, "Germont's profile does not contain the name (expected: 'Dariusz')")
        self.assertTrue(contains_history, "Germont's profile does not contain the history (expected: '+++ Relacje')")
