import re
import unittest

from RdReportsReader.Resources.TestData.MockSourceResources import load_described_profile_resource
from RdReportsReader.Resources.TestData.MockSourceResources import load_described_report_resource
from RdReportsReader.Src.Domain.CharacterProfile.Complete.Collection.Create import create_complete_profile_collection
from RdReportsReader.Src.Infrastructure.Read.Provide.Profile import provide_from_profile_domain
from RdReportsReader.Src.Infrastructure.Read.Provide.Report import provide_from_report_domain
from RdReportsReader.Src.Infrastructure.Read.Source.Mock.MockAggregatedSource import MockAggregatedSource


class TestCollectionAtaRelationMaps(unittest.TestCase):

    def test_ata_relation_map__should_not_have_profile_masterlist_link__if_character_is_not_in_profile_masterlist(self):

        # Given

        selected_source = MockAggregatedSource()
        selected_source.select_reports(load_described_report_resource("Collection", "C01M06_CorrectComplete"))
        selected_source.select_profiles(load_described_profile_resource("Collection", "F03S04P08_SimpleComplete"))

        reports = provide_from_report_domain(selected_source).collection()

        profile_provider = provide_from_profile_domain(selected_source)
        read_profiles = profile_provider.collection()
        profile_masterlist = profile_provider.masterlist()

        generated_profiles = reports.generate_profiles()

        complete_profile_collection = create_complete_profile_collection(read_profiles, generated_profiles, profile_masterlist)
        single_profile = complete_profile_collection.profile_having_name("Andromeda (Kasia Nowak)")

        # When

        profile_text = single_profile.to_rd_text()

        # Then

        matches = re.findall('.*August Bankierz.*', profile_text)

        self.assertTrue(len(matches) == 1, "A number different to 1 record of August Bankierz has been found in AtaRelationMap; expected exactly 1, got: " + str(len(matches)))

        character_from_report = matches[0]
        char_lines = character_from_report.split("||")

        name_line = ""
        for single_line in char_lines:
            if "August Bankierz" in single_line:
                name_line = single_line
                break

        contains_link = "[[[" in name_line or "]]]" in name_line
        self.assertTrue(contains_link is False, "AtaRelationMap contains link to ProfileMasterlist where the Character does not have its own profile.")

    def test_ata_relation_map__should_have_profile_masterlist_link__if_character_is_in_profile_masterlist(self):

        # Given

        selected_source = MockAggregatedSource()
        selected_source.select_reports(load_described_report_resource("Collection", "C01M06_CorrectComplete"))
        selected_source.select_profiles(load_described_profile_resource("Collection", "F03S04P08_SimpleComplete"))

        reports = provide_from_report_domain(selected_source).collection()

        profile_provider = provide_from_profile_domain(selected_source)
        read_profiles = profile_provider.collection()
        profile_masterlist = profile_provider.masterlist()

        generated_profiles = reports.generate_profiles()

        complete_profile_collection = create_complete_profile_collection(read_profiles, generated_profiles, profile_masterlist)
        single_profile = complete_profile_collection.profile_having_name("Andromeda (Kasia Nowak)")

        # When

        profile_text = single_profile.to_rd_text()

        # Then

        matches = re.findall('.*Samira Diakon.*', profile_text)

        self.assertTrue(len(matches) == 1, "A number different to 1 record of Samira Diakon has been found in AtaRelationMap; expected exactly 1, got: " + str(len(matches)))

        character_from_report = matches[0]
        char_lines = character_from_report.split("||")

        name_line = ""
        for single_line in char_lines:
            if "Samira Diakon" in single_line:
                name_line = single_line
                break

        contains_link = "[[[" in name_line or "]]]" in name_line
        self.assertTrue(contains_link is True, "AtaRelationMap does not contain link to ProfileMasterlist where the Character has its own profile.")
