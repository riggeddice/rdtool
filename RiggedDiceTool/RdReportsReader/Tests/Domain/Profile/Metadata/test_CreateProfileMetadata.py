import unittest

from RdReportsReader.Src.Domain.CharacterProfile.Metadata.Single.Create import create_single_profile_metadata_from_raw_record


class TestCreateProfileMetadata(unittest.TestCase):

    def test_profile_metadata_rdformat_created_properly_no_comments_record(self):

        # Given
        original_record = "czł: [[[inwazja-karty-postaci-1601:krystian-korzunio|Krystian Korzunio]]]"

        # When
        metadata = create_single_profile_metadata_from_raw_record(original_record)

        # Then
        self.assertTrue(metadata is not None, "ProfileMetadata has not been created at all.")
        self.assertTrue(metadata.name() == "Krystian Korzunio", "ProfileMetadata did not parse name properly")
        self.assertTrue(metadata.ziplink() == "inwazja-karty-postaci-1601_krystian-korzunio", "ProfileMetadata did not parse weblink into ziplink properly")
        self.assertTrue(metadata.to_rd_text() == "[[[inwazja-karty-postaci-1601:krystian-korzunio|Krystian Korzunio]]]", "ProfileMetadata did not change into RdText properly")
