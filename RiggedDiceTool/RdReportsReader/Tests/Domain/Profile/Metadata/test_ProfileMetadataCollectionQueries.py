import unittest

from RdReportsReader.Src.Domain.CharacterProfile.Metadata.Collection.Create import create_profile_metadata_collection


class TestProfileMetadataCollectionQueries(unittest.TestCase):

    def test_profile_metadata_collection_query__contains_name(self):

        # Given
        text_block = """mag: [[[inwazja-karty-postaci-1411:eryk-plomien|Eryk Płomień]]] (Piter)
mag: [[[inwazja-karty-postaci-1411:pawel-sepiak|Paweł Sępiak]]] (Bebuk)
mag: [[[inwazja-karty-postaci-1604:siluria-diakon|Siluria Diakon]]] (Kić) - Siluria Tyrania (Sabina)

mag: [[[inwazja-karty-postaci-1411:aleksandra-trawens|Aleksandra Trawens]]]"""

        metadata = create_profile_metadata_collection(text_block)

        # When

        result = metadata.contains_profile_name("Paweł Sępiak")

        # Then
        self.assertTrue(result is True, "ProfileMetadataCollection 'contains name' query did not recognize Paweł Sępiak.")

    def test_profile_metadata_collection_query__record_having_name(self):

        # Given
        text_block = """mag: [[[inwazja-karty-postaci-1411:eryk-plomien|Eryk Płomień]]] (Piter)
mag: [[[inwazja-karty-postaci-1411:pawel-sepiak|Paweł Sępiak]]] (Bebuk)
mag: [[[inwazja-karty-postaci-1604:siluria-diakon|Siluria Diakon]]] (Kić) - Siluria Tyrania (Sabina)

mag: [[[inwazja-karty-postaci-1411:aleksandra-trawens|Aleksandra Trawens]]]"""

        metadata = create_profile_metadata_collection(text_block)

        # When

        result = metadata.retrieve_single_metadata_having_name("Paweł Sępiak")

        # Then
        self.assertTrue(result.name() == "Paweł Sępiak", "ProfileMetadataCollection 'retrieve record create_campaign_summary name' query did not retrieve Paweł Sępiak.")

