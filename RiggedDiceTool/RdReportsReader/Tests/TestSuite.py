#!/usr/bin/python3.5
# -*- coding: utf-8 -*-

import unittest


class TestCases:

    all = unittest.TestLoader().discover("RdReportsReader.Tests", pattern="test_*.py")
    app_logic = unittest.TestLoader().discover("RdReportsReader.Tests.AppLogic", pattern="test_*.py")
    business = unittest.TestLoader().discover("RdReportsReader.Tests.Business", pattern="test_*.py")
    domain = unittest.TestLoader().discover("RdReportsReader.Tests.Domain", pattern="test_*.py")
    infrastructure = unittest.TestLoader().discover("RdReportsReader.Tests.Infrastructure", pattern="test_*.py")


unittest.TextTestRunner(verbosity=2).run(TestCases.all)
