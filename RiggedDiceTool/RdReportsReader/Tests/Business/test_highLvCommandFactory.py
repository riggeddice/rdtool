import unittest

from RdReportsReader.Src.Business.Selection import selected_business_actions
from RdReportsReader.Src.Business.Selection import BusinessActionConfig


class TestHighLvCommandFactory(unittest.TestCase):

    def test_ifNothingSelectedWorks(self):
        with self.assertRaises(KeyError):

            # Given
            selected = BusinessActionConfig(form_character_profiles_from_rd=False,
                                            correct_report_masterlist_seq_no=False,
                                            generate_location_masterlist=False, generate_campaign_summaries=False)

            # When & Then
            selected_business_actions(selected)

    def test_highLevelOperation(self):

        # Given

        config = BusinessActionConfig(generate_campaign_summaries=False, generate_location_masterlist=False)

        # When

        commands = selected_business_actions(config)

        # Then
        commands_count = len(commands)
        self.assertTrue(commands_count == 2, "The factory did not return two objects but: " + str(len(commands)))
