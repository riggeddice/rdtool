import unittest
from unittest.mock import MagicMock

from RdReportsReader.Resources.TestData.MockSourceResources import load_described_report_resource
from RdReportsReader.Src.Domain.Location.Description.Collection.Create import create_location_descriptions_from_report_collection
from RdReportsReader.Src.Infrastructure.Read.Provide.Report import provide_from_report_domain
from RdReportsReader.Src.Infrastructure.Read.Source.Mock.MockAggregatedSource import MockAggregatedSource
from RdReportsReader.Src.Infrastructure.Write.Destination.File.FileAggregatedDestination import FileAggregatedDestination
from RdReportsReader.Src.Infrastructure.Write.Persist.Location import persist_location_collection


class TestGenerateLocationDescriptions(unittest.TestCase):

    @unittest.skip("No idea what exactly should be returned yet")
    def test_generate_location_masterlist__on_mocks_including_save(self):

        # Given

        selected_source = MockAggregatedSource()
        selected_source.select_reports(load_described_report_resource("Collection", "C01M03_MPW_locations_deeds"))

        report_collection = provide_from_report_domain(selected_source).collection()

        generated_location_description_collection = create_location_descriptions_from_report_collection(report_collection)

        destination_to_test = FileAggregatedDestination()
        destination_to_test.persist_single_location_description = MagicMock(return_value=None)

        # When

        persist_location_collection(generated_location_description_collection, destination_to_test)

        # Then

        actual_data_to_be_persisted = destination_to_test.persist_single_location_description.call_args_list

        self.assertTrue(False)
