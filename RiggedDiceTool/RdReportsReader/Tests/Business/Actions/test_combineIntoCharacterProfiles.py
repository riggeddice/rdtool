import unittest
from unittest.mock import MagicMock

from RdReportsReader.Resources.TestData.MockSourceResources import load_described_profile_resource
from RdReportsReader.Resources.TestData.MockSourceResources import load_described_report_resource
from RdReportsReader.Src.Domain.CharacterProfile.Complete.Collection.Create import create_complete_profile_collection
from RdReportsReader.Src.Infrastructure.Read.Provide.Profile import provide_from_profile_domain
from RdReportsReader.Src.Infrastructure.Read.Provide.Report import provide_from_report_domain
from RdReportsReader.Src.Infrastructure.Read.Source.Mock.MockAggregatedSource import MockAggregatedSource
from RdReportsReader.Src.Infrastructure.Write.Destination.File.FileAggregatedDestination import FileAggregatedDestination
from RdReportsReader.Src.Infrastructure.Write.Persist.Profile import persist_profile_collection


class TestCombineIntoCharacterProfiles(unittest.TestCase):

    def test_combineIntoProfiles_everythingOnMocksIncludingSave(self):

        # Given

        selected_source = MockAggregatedSource()
        selected_source.select_reports(load_described_report_resource("Collection", "C01M06_CorrectComplete"))
        selected_source.select_profiles(load_described_profile_resource("Collection", "F03S04P08_SimpleComplete"))

        reports = provide_from_report_domain(selected_source).collection()

        profile_provider = provide_from_profile_domain(selected_source)
        read_profiles = profile_provider.collection()
        profile_masterlist = profile_provider.masterlist()

        generated_profiles = reports.generate_profiles()

        complete_profile_collection = create_complete_profile_collection(read_profiles, generated_profiles, profile_masterlist)

        destination_to_test = FileAggregatedDestination()
        destination_to_test.persist_single_profile = MagicMock(return_value=None)

        # When

        persist_profile_collection(complete_profile_collection, destination_to_test)

        # Then

        self.assertTrue(destination_to_test.persist_single_profile.call_count == 15, "15 profiles were not 'written' to MagicMock.")

        call_list = destination_to_test.persist_single_profile.call_args_list

        andrea_call = call_list[0][0]
        diana_call = call_list[5][0]

        self.assertTrue(andrea_call[0] == "Andrea Wilgacz" and "+++ Jakie ma zasoby" in andrea_call[1], "Andrea record malformed")
        self.assertTrue(diana_call[0] == "Diana Weiner" and "+++ Jaka jest" in diana_call[1], "Diana record malformed")
