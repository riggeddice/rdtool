import unittest
from unittest.mock import MagicMock

from RdReportsReader.Resources.TestData.MockSourceResources import load_described_report_resource
from RdReportsReader.Src.Config.Providers.ProvideInternalTokens import ProvideInternalTokens
from RdReportsReader.Src.Domain.Location.Masterlist.Create import create_location_masterlist_from_report_collection
from RdReportsReader.Src.Infrastructure.Read.Provide.Report import provide_from_report_domain
from RdReportsReader.Src.Infrastructure.Read.Source.Mock.MockAggregatedSource import MockAggregatedSource
from RdReportsReader.Src.Infrastructure.Write.Destination.File.FileAggregatedDestination import FileAggregatedDestination
from RdReportsReader.Src.Infrastructure.Write.Persist.Location import persist_location_masterlist


class TestGenerateLocationMasterlist(unittest.TestCase):

    def test_generate_location_masterlist__on_mocks_including_save(self):

        # Given

        selected_source = MockAggregatedSource()
        selected_source.select_reports(load_described_report_resource("Collection", "C01M02_MPWSimplestLocations"))

        report_collection = provide_from_report_domain(selected_source).collection()

        generated_location_masterlist = create_location_masterlist_from_report_collection(report_collection)

        destination_to_test = FileAggregatedDestination()
        destination_to_test.persist_location_masterlist = MagicMock(return_value=None)

        # When

        persist_location_masterlist(generated_location_masterlist, destination_to_test)

        # Then

        persisted_location_masterlist = destination_to_test.persist_location_masterlist.call_args_list
        persisted_masterlist_text = persisted_location_masterlist[0][0][0]

        expected_header = ProvideInternalTokens().top_level_header() + " " + ProvideInternalTokens().location_header() + ":"

        self.assertTrue(expected_header in persisted_masterlist_text, "Lokalizacje header not in persisted_masterlist_text")
        self.assertTrue(" # Śląsk" in persisted_masterlist_text, "Śląsk on deoth == 1 not in persisted_masterlist_text")
        self.assertTrue("    # Wielkie Muzeum Śląskie" in persisted_masterlist_text, "Museum uniquely from 'Irytka' \
            on not in persisted_masterlist_text")
        self.assertTrue("Sala audiencyjna Lorda Terminusa" in persisted_masterlist_text,
                        "Audience Hall uniquely from 'Millennium' on depth == 5 not in persisted_masterlist_text")
        self.assertTrue(persisted_masterlist_text.count("Sala audiencyjna Lorda Terminusa") == 1, "non-unique \
object at high depth appears more than once in persisted_masterlist_text")
