import unittest

from RdReportsReader.Src.AppLogic.Structure.Composite.CompositeNode import CompositeNode


class TestCompositeNodeNavigation(unittest.TestCase):

    def test_adding_children_works_widely(self):

        wide_node = CompositeNode(1)
        nodes_to_add = [CompositeNode(2), CompositeNode(3), CompositeNode(4)]

        for node in nodes_to_add:
            wide_node.add_child(node)

        self.assertTrue(wide_node.has_children())
        self.assertTrue(len(wide_node.children()) == len(nodes_to_add))

    def test_adding_children_works_deeply(self):

        deep_node = CompositeNode(1)
        nodes_to_add = [CompositeNode(2), CompositeNode(3), CompositeNode(4)]

        last_node = deep_node
        for node in nodes_to_add:
            last_node.add_child(node)
            last_node = node

        self.assertTrue(deep_node.has_children())
        self.assertTrue(len(deep_node.children()) == 1)
        self.assertTrue(deep_node.children()[0].children()[0].children()[0] is not None)

    def test_removing_children_works(self):

        wide_node = CompositeNode(1)
        removable_node = CompositeNode(10)
        nodes_to_add = [CompositeNode(2), removable_node, CompositeNode(4), CompositeNode(5)]

        for node in nodes_to_add:
            wide_node.add_child(node)

        wide_node.remove_child(removable_node)

        self.assertTrue(len(wide_node.children()) == 3)
        self.assertTrue(removable_node not in wide_node.children())

    def test_get_root_works(self):

        deep_node = CompositeNode(0)
        third_node = CompositeNode(3)
        last_node = CompositeNode(5)

        nodes_to_add = [CompositeNode(1), CompositeNode(2), third_node, CompositeNode(4), last_node]

        last_node = deep_node
        for node in nodes_to_add:
            last_node.add_child(node)
            last_node = node

        root_from_3 = third_node.root()
        root_from_last = last_node.root()

        self.assertTrue(root_from_3 is root_from_last)
        self.assertTrue(root_from_3 is deep_node)
