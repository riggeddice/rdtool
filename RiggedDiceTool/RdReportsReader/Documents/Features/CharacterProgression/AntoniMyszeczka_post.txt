+ Antoni Myszeczka


+++ Na misjach:

+++++ Postać wystąpiła na: 2 misjach.
+++++ Postać wystąpiła w: 2 kampaniach: Druga Inwazja, Blakenbauerowie x "Skorpion"

||~ Data ||~ Numer ||~ Misja ||~ W kampanii ||
|| 141006 || 117 || [[[inwazja-konspekty:141006-klinika-slonecznik|Klinika "Słonecznik" (WD, EM)]]]||Blakenbauerowie x "Skorpion"||
|| 140109 || 125 || [[[inwazja-konspekty:140109-uczniowie-moriatha|Uczniowie Moriatha(AW, WZ)]]]||Druga Inwazja||

+++ Progresja postaci:

||~ Misja o tytule ||~ Progresja postaci || 
|| [[[inwazja-konspekty:141006-klinika-slonecznik|Klinika "Słonecznik" (WD, EM)]]]  || ma dostęp do wielkiej hodowli Dromopod Iserat     ||
|| [[[inwazja-konspekty:141006-klinika-slonecznik|Klinika "Słonecznik" (WD, EM)]]]  || ma stały dochód +1 Water (szacun, sława i chwała) ||

+++ Dokonania:

|| [[[inwazja-konspekty:141006-klinika-slonecznik|Klinika "Słonecznik" (WD, EM)]]] || posiadacz dorodnej hodowli Dromopod Iserat ||
|| [[[inwazja-konspekty:140109-uczniowie-moriatha|Uczniowie Moriatha(AW, WZ)]]] || mag, któremu zniknęły Dromopod Iserat w pewnej skandalicznej akcji. ||

+++ Relacje z postaciami:

||~ Z kim ||~ Intensywność ||~ Na misjach ||
||[[[inwazja-karty-postaci-1411:anabela-diakon|Anabela Diakon]]]||1||[[[inwazja-konspekty:141006-klinika-slonecznik|141006]]]||
||[[[inwazja-karty-postaci-1604:andrea-wilgacz|Andrea Wilgacz]]]||1||[[[inwazja-konspekty:140109-uczniowie-moriatha|140109]]]||
||[[[inwazja-karty-postaci-1411:cezary-piryt|Cezary Piryt]]]||1||[[[inwazja-konspekty:140109-uczniowie-moriatha|140109]]]||
||[[[inwazja-karty-postaci-1411:dalia-weiner|Dalia Weiner]]]||1||[[[inwazja-konspekty:141006-klinika-slonecznik|141006]]]||
||[[[inwazja-karty-postaci-1411:edwin-blakenbauer|Edwin Blakenbauer]]]||1||[[[inwazja-konspekty:141006-klinika-slonecznik|141006]]]||
||[[[inwazja-karty-postaci-1411:esme-myszeczka|Esme Myszeczka]]]||1||[[[inwazja-konspekty:141006-klinika-slonecznik|141006]]]||
||[[[inwazja-karty-postaci-1411:estera-piryt|Estera Piryt]]]||1||[[[inwazja-konspekty:141006-klinika-slonecznik|141006]]]||
||Henryk Waciak||1||[[[inwazja-konspekty:141006-klinika-slonecznik|141006]]]||
||Lenart Stosal||1||[[[inwazja-konspekty:140109-uczniowie-moriatha|140109]]]||
||[[[inwazja-karty-postaci-1411:marian-agrest|Marian Agrest]]]||1||[[[inwazja-konspekty:140109-uczniowie-moriatha|140109]]]||
||[[[inwazja-karty-postaci-1411:mojra|Mojra]]]||1||[[[inwazja-konspekty:140109-uczniowie-moriatha|140109]]]||
||Onufry Maus||1||[[[inwazja-konspekty:141006-klinika-slonecznik|141006]]]||
||[[[inwazja-karty-postaci-1411:rebeka-piryt|Rebeka Piryt]]]||1||[[[inwazja-konspekty:140109-uczniowie-moriatha|140109]]]||
||[[[inwazja-karty-postaci-1411:sebastian-tecznia|Sebastian Tecznia]]]||1||[[[inwazja-konspekty:140109-uczniowie-moriatha|140109]]]||
||[[[inwazja-karty-postaci-1604:tadeusz-baran|Tadeusz Baran]]]||1||[[[inwazja-konspekty:141006-klinika-slonecznik|141006]]]||
||[[[inwazja-karty-postaci-1411:waclaw-zajcew|Wacław Zajcew]]]||1||[[[inwazja-konspekty:140109-uczniowie-moriatha|140109]]]||
||[[[inwazja-karty-postaci-1411:waldemar-zupaczka|Waldemar Zupaczka]]]||1||[[[inwazja-konspekty:140109-uczniowie-moriatha|140109]]]||
||[[[inwazja-karty-postaci-1411:wiktor-diakon|Wiktor Diakon]]]||1||[[[inwazja-konspekty:141006-klinika-slonecznik|141006]]]||