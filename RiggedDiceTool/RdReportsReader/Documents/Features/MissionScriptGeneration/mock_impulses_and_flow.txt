﻿==========================================
You have four “impulses” having only things relevant to you: name and actions.
===

+++ Nazwa:
Impulse 1

+++ Akcje:
# a 1 1
# a 1 2 <Fire, Water>
# a 1 3


+++ Nazwa:
Impulse 2

+++ Akcje:
# a 2 1 <wydarzenie>
# a 2 2 <osoba>
# a 2 3 <Water, Fire, Void>


+++ Nazwa:
Impulse 3

+++ Akcje:
# a 3 1
# a 3 2


+++ Nazwa:
Impulse 4

+++ Akcje:
# a 4 1
# a 4 2


=========================================
=========================================


Let us create an Actor out of those impulses. Every Actor has three impulses; in the mock file Actor_1 was assigned randomly Impulse1, Impulse2, Impulse3
===

Actor_1
    Impulse 1
    Impulse 2
    Impulse 3
    
Every Actor has one Supply and one Demand. Generate them for Actor (Supply cannot be equal to Demand):

Actor_1
    Impulse 1
    Impulse 2
    Impulse 3
    
    Supply: Fire
    Demand: Water
    
=========================================
=========================================

Let us make 3 moves with an Actor:

Move 1:

Stage I: 
We need to select which Impulse is used. Let’s assume we have randomly generated Impulse 2.

Stage II: 
We need to select the move from the selected Impulse. That Impulse has three possible moves and let’s assume we selected move #3

Stage III: 
We need to unpack the wildcards. 
We have the Elements (Resources) wildcard: <Water, Fire, Void>. We select (randomly) the Water element, so the move:

# a 2 3 <Water, Fire, Void>

Becomes

# a 2 3 Water

Which means our result looks like this:

Actor_1 : a 2 3 Water


Move 2:

Stage I: 
Impulse 2

Stage II: 
move #1

Stage III: 
the wildcard here is special; it is one of the keywords:

<wydarzenie>
<problem>
<czegoś> 
<coś>

if one of those keywords appear, we append “(powiązanie: <Earth, Fire, Air, Water, Void>)” directly after the keyword.

So:

# a 2 1 <wydarzenie>

Becomes:

# a 2 1 <wydarzenie> (powiązanie: <Earth, Fire, Air, Water, Void>)

and finally (randomly generated one of the Elements):

# a 2 1 <wydarzenie> (powiązanie: Air)

And thus, after two moves we have:

Actor_1 : a 2 3 Water
Actor_1 : a 2 1 <wydarzenie> (powiązanie: Air)


Move 3:
Stage I: 
impulse 2

Stage II: 
move #2

Stage III: 
there is no wildcard here, nor there are elements. Therefore we append to the end of the line (zmiana surowców: <Earth, Fire, Air, Water, Void>).

So:

# a 2 2 <osoba>

becomes:

# a 2 2 <osoba> (zmiana surowców: <Earth, Fire, Air, Water, Void>)

and in the end:

# a 2 2 <osoba> (zmiana surowców: <Void>)

=========================================