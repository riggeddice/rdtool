Look at the attached file "MissionScriptGeneration_specification" where you have a spec of the stuff to be done and overall explanation.

Look at the attached file mock_impulses_and_flow.txt. You have four impulses having only things relevant to you: name and actions.
You also have an example of a single generation of moves related to a single Actor (made from 3 Impulses).

Look at the attached file "inwazja-szablony-impulsy_narcyz.txt". This is an example of a file you will have to deal with; a properly formed Impulse. 
Note the part you are to parse to get Moves from the Impulse.

Look at the attached file "mission_script_gen_config.properties" where you have an example of a config file to steer the mission generation
(in code something like this is solved by ProvideInternalTokens for inspiration)

Ask for a RD backup file to get the most recent one.

Impulses come from a proper Masterlist in backup file.

Actual state of the system:
- nothing exists of what is to be written.

Expected state of the system:
- there exists a test proving it works.
- there exists a proper ProvideFrom_XXX_Domain and Persist_XXX
- there exists a proper High Level Command which governs the mission generation
- there exists a configuration file to set: 
    the amount of Actors
    probabilities of Demand and Supply change
    amount of Phases
    amount of Moves to be done for a Phase by a Character
    weights of particular Element presence (mostly used for "how much less Void appears than others")
- configuration file is parsed and is properly used by a Mission Generator
- mission is generated like expected, in the mock file
- mission is saved to a _generatedMission.txt file (do stuff to the rd_tool_config.json file and a proper ProvideXXX to make sure things are saved where we want to)
    
===
Keywords:

<wydarzenie>
<problem>
<czegoś>
<coś>

===
Appended to keyword directly after a keyword:
(powiązanie: <Earth, Fire, Air, Water, Void>)

===
Appended if no Element nor Keyword present:
(zmiana surowców: <Earth, Fire, Air, Water, Void>)
