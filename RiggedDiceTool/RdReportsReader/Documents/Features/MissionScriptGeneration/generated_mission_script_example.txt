﻿+ Skrypt:

++++ Actors: 
* NDH (Narcyz, Dyskretny, Hulaka). Supply: Fire, Demand: Water.
* INH (Idealista, Narcyz, Hulaka). Supply: Air, Demand: Water.
* VDD (Vendetta, Deliryczny, Dyskretny). Supply: Void, Demand: Earth.
* VDD_2 (Vendetta, Dług Honorowy, Deliryczny) Supply: Water, Demand: Air.

++++ Faza 1:

* INH: Skupienie całej uwagi na sobie podczas <wydarzenie> (powiązanie: Fire), przez co doszło do <wydarzenie> (powiązanie: Water).
* VDD_2: Redukowanie Earth po stronie <osoba> lub <grupa>
* NDH: Włamanie się <miejsce> elektronicznie lub fizycznie bez pozostawienia śladów celem <wydarzenie> (powiązanie: Air) wpływając na Earth.
* INH: Przekonanie <osoba> odnośnie <czegoś> (powiązanie: Earth) z absolutną pewnością siebie.
* VDD: Zastawienie pułapki przy użyciu Water na <osoba> by doprowadzić do <wydarzenie> (powiązanie: Water).

++++ Faza 2:
... 