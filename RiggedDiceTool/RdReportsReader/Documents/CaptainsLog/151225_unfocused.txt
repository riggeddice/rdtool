The refactoring of the application is going in good direction. An important observation has been made – it is not enough
to simply create reports and to generate the reports; as much as report has been modified and adapted to the new circumstances,
profile will have to be modified and adapted as well.

That would have to exist a new type of profile, ReadProfile. This one combined with generated profile will lead to
complete profile – the one which exists in the document describing the data, that is RD dictionary.
This also modifies high level algorithm – up to this point I thought that existing profile would combine with generated
profile and that existing profile would become a prototype of complete profile. However, I should have read profiles
from one side and generated profiles from the other side and here I should use a…
something like CompleteProfileCollectionFactory to combine read profiles with generated profiles;
or existing profiles with generated profiles, depends how you call them, so I will have a collection of complete profiles at last.

Updating the high level algorithm, reading the profile and forming a proper ReadProfile should now become the highest priority.

Also, I have stumbled upon the first issue with my current approach to structuring the application. Namely, namespace problems. Consider those below:

require_relative '../../Src/DomModel/Report/Single/Structure/DramatisPersonae/View/ActorToActorRelationMapCollection/Command/AtarmCollectionSubsetExtractor'
require_relative '../../Src/DomModel/Report/Single/Structure/DramatisPersonae/View/ActorToRecordsCollection/Command/AtrCollectionSubsetExtractor'

When you look at the paths which lead to the files, they are actually quite understandable. However, both could be simply named “SubsetExtractor”
and in my opinion this would be a far superior name. The problem is, I would not actually be able to distinguish between both of them because of
lack of name spacing and very similar code structure. The fact that I could actually reuse one class in both of those places more or less doesn’t help.

Which leads me to another observation – reusability is a lie. I can have testability and reusability, but then I will have
exactly the same problem which is now in the infrastructure level. The problem is, it is too generalized; I have too little code to do too much.
When I am going to work on archetypes and motives – and especially when I am going to parse mechanics in any way – the infrastructure level
will have to be completely refactored. This has happened in the past before, when I have actually used metadata to denote
both master list component for a character (now: profile) while they have actually diverged with time.
This is why I can use subset extractor in both Atarm and Atr, but to do so would be a dead end;
the structure of those is actually too unstable at this point and I cannot assume it will not change.
So reusability is something which can exist – but only for high-level components, modules. Commands (once implemented as “helpers”)
are quite connected to the objects which called them and I am not sure I should work on their reusability.

One other design choice which is worth mentioning here – DramatisPersonaeRecord. Namely, the constructor of the record requires three parameters:
initialize(actor_type, name, mission_deed).
On the other hand, the record is heavily reliant on the parent, that is DramatisPersonae. Thus I have created a method
set_parent_dramatis_personae(dramatisPersonae)
which is called much later than the constructor is. I have actually introduced a risk of object mutability,
but I have made it much easier to work with this object and made testing slightly easier. This choice will be evaluated in the future,
and the refactoring may appear here again so the object returns to being immutable at the cost of either creating the record
using old record data (copy constructor) or passing the parent down to the child, that is, record.

All in all, an interesting area I need to look at is the tests. Right now I am making very high-level tests which do not
really test a lot (well, they tell me if a whole area is correct) but I do not have many tests in the algorithm level,
even if I could easily write them. I do admit that this approach to writing code I have right now does not require that much tests,
and I do know that you actually should not test façades (while most of my code is façades), but it still is slightly unsettling.
Well, as long as it works for me, it is good enough.

Tests will require very heavy refactoring. They do not have any apparent structure and they are not really connected to new mental model at all.
It is quite possible that the tests will simply get added while old tests will wither and die. Then get deleted. Time will tell;
for now I’m focused on the profile and on rebuilding the overall complete profile generation.