# README #

### Business goal of the application ###

Paper and pencil RPG assistant tool connected with riggeddice website on wikidot. Niche and pointless for anyone outside our small group. Mostly public for educational use; being used to test different coding styles, code structure and basically having fun.

### Intended license 

GPLv3

### How do I get set up? ###

* Download. Run Python 3. Developed in PyCharm.
* Configuration: rd_tool_config.json for filepaths.
* Dependencies: nothing except standard Python libraries from what I remember.
* Database configuration: none at this moment
* How to run tests: Tests are located in rdtool/RiggedDiceTool/RdReportsReader/Tests
* Deployment instructions: none at this moment

### Contribution guidelines ###

No reason why you would have any advantage in contributing, but if you want to, be my guest.

* Writing tests: put tests in Tests. The idea 'how to' is not stabilized in the application yet.
* Code review: snake_case for methods, one class per file, more 'enterprise-y' than 'hacker-y' approach.
* Other guidelines: none at this moment.

### Who do I talk to? ###

* lukasz.januszek@gmail.com